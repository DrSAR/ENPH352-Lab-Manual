\chapter{Acoustic Impedance}
\renewcommand\chapname{acousticimp}
\chapternumberreset

\section{Purpose}

To gain facility with the impedance tube, the standard method of measuring the acoustic properties of surfaces.
 
\section{Introduction}

See Rossing and Fletcher sections 6.2 and 8.3.

Acoustic impedance is the ratio of acoustic pressure to acoustic velocity or volume flow - which of these you choose depends on the application. There is a straightforward analogy here between acoustic and electrical impedance: pressure/voltage, velocity or flow/current. The expression of Newton's Second Law in the context of an acoustic wave relates pressure $p$ to velocity $u$:

\begin{equation}
\rho\frac{\partial u}{\partial t} = -\nabla p 
\end{equation}

In the case of a plane wave with the following forms, the impedance $z$ has a simple form:

\begin{equation}
p = p_0 e^{j(kx-\omega t)}; \ \ \ 
u = u_0 e^{j(kx-\omega t)}; \ \ \ 
z = \frac{p}{u} = \rho c
\end{equation}

Note that the field of acoustics, by convention, uses the engineer's notation for the square root of minus one
(j). For a plane wave in a free space pressure and velocity are in phase and their ratio is the real product of the density $\rho$ and the speed of sound $c = \omega/k$. For spherical waves and waves in a tube with terminations, the impedance is complex and varies in space and time.

Consider a rigid hollow tube with a sound source at one end, a termination of some sort at the other, with the acoustic pressure measured by microphones at two points along its length (M1 and M2).

\begin{figure}[h]
\begin{center}
\includegraphics[angle =-90,width=\textwidth]{figs/imp_tube.eps}
\caption{General arrangement of simple acoustic impedance tube.}
\label{fig:imp_tube}
\end{center}
\end{figure}

The inner radius of the tube is $a$, the length $L$, and the two microphones are mounted at distances $l_1$ and $l_2$ from a speaker sealed in one end. At the other end is a sample of unknown acoustical impedance $Z_L$. In this case it makes sense to talk about impedance in terms of pressure divided by volume flow rather than velocity. Thus the characteristic impedance of the tube is given by:

\begin{equation}
 Z_0=\frac{ρc}{S}, \ \ \  S = \pi a^2.
\end{equation}

If the speaker is delivering an acoustic signal of frequency $f = \omega/2\pi$ (whose wavelength is much longer than the radius of the tube) we can assume the pressure is a result of forward and backward-going plane waves.

\begin{equation}
p(x,t)= [Ae^{-jkx}+Be^{jkx}]e^{j\omega t}
\end{equation}

Then the acoustic flow $U$ in the tube can be written in terms $Z_0$:

\begin{equation}
U(x,t)=(Ae^{-jkx}-Be^{jkx})/Z_0
\end{equation}

With a bit of algebra, and the assumption that the speaker end acts like a closed pipe we can find the reflection coefficient 
$r=B/A$ in terms of the ratio of the two microphone pressure signals, $H_{12}= p_2/p_1$, and two lengths $\Delta L= L-l_1$  and 
$s= l_2-l_1$.

\begin{equation}
r = \frac{H_{12} - e^{-jks}}{e^{jks} - H_{12}}e^{2jk\Delta L}
\end{equation}

\begin{equation}
Z_L = Z_0 \frac{1+r}{1-r}
\end{equation}

To test the assumption about the speaker (which is actually delivering the acoustic signal through many small holes in a thick piece of brass), we can take data with a brass plug in the load end, and with the load end open.
The brass plug is easy to model: 

\begin{equation}
Z_L \gg Z_0, \ \ \    r \approx 1
\end{equation}

The open end is less straightforward - $p$ is small at this end but not zero. The standing wave does not stop here, it radiates to the outside world. We need an expression for the {\it radiation impedance} of the unflanged end of a pipe (why do you think a flanged pipe would be any different? See Rossing and Fletcher):

\begin{equation}
Z_L/Z_0 \approx 0.25(ka)^2 + 0.61jka  \ \ \ \ \ \ (ka\ll 1)
\end{equation}

Otherwise, one can put in a material sample (architectural material, say) and measure its acoustic absorbing and reflecting qualities. Note there will be a change in $\Delta L$ when you insert a sample.

\section{Equipment and Tasks}

\begin{itemize}
\item Brass impedance tube with four electret microphones, a speaker and brass terminator
\item Function generator
\item Measurement Computing ADC
\item DAQ computer
\item Basic DAQ code BareBonesDaq.m
\end{itemize}

Use DAQ code to take sweep data with the microphones with the following end conditions:

\begin{itemize}
\item Brass termination
\item Open end (“unflanged”)
\item Various pieces of foam
\end{itemize}

The analysis above is predicated on two microphones being identical. You have four microphones that are not in reality identical; this makes a nice challenge and an opportunity for creativity.  You will need to calibrate the microphones against each other, both in amplitude and phase (only relative amplitude and relative phase matter, as you don't know what is coming out of the speaker). Then make plots of $H_{12}$, $Z_L/Z_0$ and $r$ (magnitude and phase, comparing data and theory for the impedances you know (brass termination, unflanged open pipe) for pairs of microphones. 

When you feel confident you understand the system, measure materials whose impedance you don't know.


\section*{Bibliography}

 T. D. Rossing and N. Fletcher, {\bf ``Principles of Vibration and Sound"}, (Springer) 1994.


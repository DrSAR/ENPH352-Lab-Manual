\section{Appendix:  Quantum Mechanics of NMR}\label{sec:nmr-quantum}

In NMR, the large DC magnetic field $\vec{B_o}$ = $B_o \hat{z}$ defines the 
system's $\hat{z}$-direction.  Any particles which have spin angular momentum 
(e.g. protons, neutrons, or electrons), and aren't already paired in full 
orbitals/states, will align parallel or anti-parallel to the field, so as to 
reduce the system's energy.  In proton NMR, we're interested, not surprisingly, 
in protons.  These particles are spin $\frac{1}{2}$, meaning that their allowed 
spin states are $m_I = \pm\frac{1}{2}$.  

The portion of the Hamiltonian due to the magnetic field is 
\begin{equation}
H = \frac{e}{2m}g\vec{B_o}\cdot\vec{I} = 
\frac{eg}{2m}B_o\left(\pm\frac{1}{2}\hbar\right).
\end{equation} 

For free protons, g = 5.586, and is usually called the Land\'e g-factor.  
$\vec{I}$ is the nuclear spin.  

For anything interesting to happen, we must add a small, time-varying magnetic 
field $\vec{B_1} = B_1\hat{x}\cos{\omega t} + B_1\hat{y}\sin{\omega t}$.  In 
order to deal with this field in a comparatively straightforward manner,  I 
must now introduce some notation:  

I will be using spinors, because life becomes much simpler when the problem can
be turned into 2$\times$2 matrices.  The first time you see spinors, they 
usually look like a bizarre way to represent spin, but once you wrap your mind 
around them, they can be {\slshape extremely} handy.  Spin up ($\hat 
z$-direction)%
~$\equiv~%
\left(\!\begin{smallmatrix}
1\\ 0
\end{smallmatrix}\!\right)$, spin down~$\equiv~%
\left(\!\begin{smallmatrix}
0\\ 1
\end{smallmatrix}\!\right)$.  
These are identical to the $\uparrow$ and $\downarrow$ you know and love from 
first year chem.  {\slshape All} possible spin states can be constructed from 
these two spinors.  For example, it can be shown that the state where spin is 
up in the $\hat x$ direction corresponds to 
$\frac{1}{\sqrt{2}}\left(\!\begin{smallmatrix}
1\\ 1
\end{smallmatrix}\!\right)~=~%
\frac{1}{\sqrt{2}}\left(\uparrow~+~\downarrow\right)$.  In quantum mechanics, 
you can know the angular momentum around only one axis at a time.  If you know 
the spin about the $x$-axis, the particle is in some superposition of spin-up 
and spin-down states in the $\hat z$ direction.

Using spinors, the Hamiltonian with both magnetic fields is 
\begin{equation}
H = \frac{eg\hbar}{4m} \begin{pmatrix}
B_o & B_1e^{j\omega t}\\ 
B_1e^{j\omega t} & -B_o
\end{pmatrix}
\end{equation}

The wavefunction 
$\psi$~=~$\left(\!\begin{smallmatrix}
a\\ b
\end{smallmatrix}\!\right)$, 
where $a$ and $b$ are time-varying and $a^2+b^2=1$.  When $B_1$ is first 
turned on, it finds the spin in its equilibrium state 
$\left(\!\begin{smallmatrix}
1\\ 0
\end{smallmatrix}\!\right)$, 
so $a(0)$ = 1 and $b(0)$ = 0.  In order to minimize mess, I must now introduce 
a couple more $\omega$'s.  As above, $\omega$ is the angular frequency of the 
applied field, but we now have $\omega_o$, the resonant (Larmor) frequency 
from field $B_o$, and $\omega_1$, which is the $B_1$ equivalent.  I.e. 
\begin{equation}
\frac{\omega_o}{2} \equiv 
\frac{eg}{4m}B_o\ \ \ \ \ \ \text{and}\ \ \ \ \ \ 
\frac{\omega_1}{2} \equiv \frac{eg}{4m}B_1
\end{equation}

If we apply the Schr\"odinger equation ($j\hbar\partial_t\psi = H\psi$) to 
$\left(\!\begin{smallmatrix}
a\\ b
\end{smallmatrix}\!\right)$, we get
\begin{equation}
j\hbar\partial_t \begin{pmatrix}
a\\ b
\end{pmatrix} = 
H \begin{pmatrix} a\\ b \end{pmatrix} = \frac{\hbar}{2}
\begin{pmatrix}
\omega_o & \omega_1e^{j\omega t}\\
\omega_1e^{j\omega t} & -\omega_o
\end{pmatrix} 
\begin{pmatrix}
a\\ b
\end{pmatrix}
\end{equation}
This gives 
\begin{equation}
j\partial_ta=\frac{\omega_o}{2}a+\frac{\omega_1}{2}be^{j\omega t}\ 
\ \ \ \ \ \text{and}\ \ \ \ \ \ 
j\partial_tb=-\frac{\omega_o}{2}b+\frac{\omega_1}{2}ae^{j\omega t}
\end{equation}

It is now convenient to define two new ``constants", $A \equiv 
ae^{j\frac{\omega_o}{2}t}$ and $B \equiv be^{-j\frac{\omega_o}{2}t}$.  Now, 
\begin{equation}\label{eqn:nmr-idta}
j\partial_tA=\frac{\omega_1}{2}Be^{j(\omega_o-\omega)t}\ 
\ \ \ \ \ \text{and}\ \ \ \ \ \ 
j\partial_tB=\frac{\omega_1}{2}Ae^{-j(\omega_o-\omega)t}
\end{equation}
This can be solved by $A = A(0)e^{j\lambda t}$ and $B = B(0)e^{j\lambda 
t}e^{-j(\omega_o-\omega)t}$, where the roots of $\lambda$ are 
\begin{equation}
\lambda_\pm = \frac{\omega_o-\omega\pm\sqrt{(\omega_o-\omega)^2 + 
\omega_1^2}}{2}
\end{equation}
The general solution for $A$ is then
\begin{equation}
A = A_+e^{j\lambda_+t} + A_-e^{j\lambda_-t}
\end{equation}
Substituting this into the first part of equation \ref{eqn:nmr-idta} gives
\begin{equation}
B = 
-\frac{2}{\omega_1}e^{-j(\omega_o-\omega)t}\left[\lambda_+A_+e^{j\lambda_+t} 
+ \lambda_-A_-e^{j\lambda_-t}\right]
\end{equation}

The initial conditions $A(0) =a(0)= 1$ and $B(0)=b(0) = 0$ imply that $A_+ + 
A_- = 1$ and $\lambda_+A_+ + \lambda_-A_- = 0$, or
\begin{equation}
A_+ = \frac{\lambda_-}{\lambda_--\lambda_+}\ 
\ \ \ \ \ \text{and}\ \ \ \ \ \ 
A_- = -\frac{\lambda_+}{\lambda_--\lambda_+}
\end{equation}

We can now obtain a result for the probability that the spin has flipped to 
down (or how much ``down" there is in the superposition of states):  
\begin{align}
P_{a\to b}(t) = |b|^2 = |B|^2 &= 
\frac{4}{\omega_1^2}\left|\frac{\lambda_+\lambda_-}{\lambda_--\lambda_+}e^{j\lambda_+t} 
- \frac{\lambda_+\lambda_-}{\lambda_--\lambda_+}e^{j\lambda_-t}\right|^2 
\\
&= \frac{\frac{4}{\omega_1^2}\left[\frac{(\omega_o-\omega)^2}{4} - 
\frac{(\omega_o-\omega)^2 + \omega_1^2}{4}\right]^2}{\left[- 
\sqrt{(\omega_o-\omega)^2 + \omega_1^2}\right]^2}\left|%
e^{j\frac{\omega_o-\omega}{2}t}\left(e^{j\frac{\sqrt{(\omega_o - \omega)^2 + 
\omega_1^2}}{2}t} - e^{-j\frac{\sqrt{(\omega_o - \omega)^2 +
\omega_1^2}}{2}t}\right)\right|^2
\notag \\
&= \frac{\frac{\omega_1^2}{4}}{(\omega_o-\omega)^2 + \omega_1^2}\left|%
e^{j\frac{\omega_o-\omega}{2}t}2j\right|^2\sin^2\frac{\sqrt{(\omega_o - 
\omega)^2 + \omega_1^2}}{2}t
\notag \\
&= \frac{\omega_1^2}{(\omega_o - \omega)^2 + 
\omega_1^2}\sin^2\frac{\sqrt{(\omega_o - \omega)^2 + \omega_1^2}}{2}t
\label{eqn:nmr-flipprob}
\end{align}

At this point, it should suddenly become clear why the spectrometer needs to be 
tuned to resonance ($\omega = \omega_o$).  The simplification is dramatic:  
\begin{equation}
\label{eqn:nmr-final}
P_{a\to b}(t) = \sin^2\frac{\omega_1}{2}t
\end{equation}

It should also be clear now what $\frac{\pi}{2}$ and $\pi$ pulses correspond 
to.  It should be noted that, while this was derived for a single particle, the
same result works for the system as a whole, with the energies multiplied by 
($N_1 - N_2$), where $N_1$ and $N_2$ are the numbers of particles in the lower 
and upper energy states respectively.  

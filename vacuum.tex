\chapter{Vacuum Systems}
\renewcommand\chapname{Vacuum}
\chapternumberreset
\setcounter{secnumdepth}{3}
\section{Purpose}

\begin{enumerate}
\item To become familiar with the use and calibration of vacuum systems 
and mass flow controllers.  

\item To study the flow properties of several gases in a vacuum system.  
\end{enumerate}

\section{Introduction}

Vacuum chambers are used in a variety of scientific and industrial 
applications to provide a controlled atmosphere at reduced pressures. In many
cases it desirable to remove as much of the ambient gas as possible. 
Examples where this is important include the preparation and study of 
high-purity materials that are sensitive to corrosion, impurities or 
oxidation, electron microscopes where gas molecules scatter the electron beam 
used to image the specimens, and industrial vacuum coating systems for a 
combination of the above reasons. In other applications, one wants to replace 
the ambient atmosphere with a controlled mixture of ``process gases'' whose 
chemical properties can result in thin-film growth or oxidation (e.g. oxygen 
for SiO$_\text{2}$ gate dielectrics on Si), or highly selective etching of 
substrates placed in the chamber.  These are but a few of the many 
applications of vacuum and gas-flow-control technologies.  

Regardless of whether one wishes to work in a reduced atmosphere or none at 
all, vacuum pumps are required to maintain the desired pressure and gauges are 
required to observe it.  There are many different types of pumps, pressure 
transducers and flow controllers available, each with a unique set of 
attributes which make it useful for specific applications.  For example, 
rotary pumps are inexpensive, robust, and relatively simple to use, but they 
only work at relatively low vacuum (high pressure). 
Diffusion pumps are relatively inexpensive, simple, and capable of achieving 
higher vacuums, but they require oil for their operation and therefore are 
potentially disastrous sources of contamination, particularly in the event of 
failure or incorrect usage -- diffusion pump oil has a well-earned reputation 
for being nearly impossible to clean off.  
%Capacitance manometers are 
%robust and accurate, but again they only work at relatively low vacuum. 

The detailed mechanisms involved in the operations of vacuum system components 
are beyond the scope of this manual, but some generic issues associated with 
them will be covered.  These include:  accurate measurement of total and 
partial pressures of constituent gases, and the relationship between pumping 
speed, chamber volume, pressure and flow rate.  

\section{Theory}\label{vac-theory}

The analysis of pumping systems is similar to electrical circuit analysis with 
pressure playing the role of electrical potential (voltage). The role of 
current is played by the system's {\sl throughput}, $Q$, defined as the 
quantity of gas (the volume of gas at a known pressure) that passes through a 
plane per unit time, 
\begin{equation}
Q = \frac{d(PV)}{dt}\text{.}
\end{equation}
Notice that $Q$ has units of work (Watts). This not the kinetic or
potential energy contained in the gas molecules, but rather the work
required to transport the molecules across the plane. Note that the
number of molecules of gas per unit time that a given value of $Q$
describes depends on temperature.  The {\sl conductance} $C$ of an
object, such as a piece of tubing or an orifice, is defined in terms
of the throughput associated with a pressure drop across that object
assuming it is connected to two large reservoirs on either side:
\begin{equation}
C = \frac{Q}{P_2-P_1}
\end{equation}
$C$ has units of volume per unit time.  The conductance of an object is in 
general a nonlinear function of the pressure, but it can frequently be 
approximated as being independent of pressure.

\begin{figure}[htb]
\begin{center}
\psfrag{MFC}{MFC}
\psfrag{Po}{$P_\circ$}
\psfrag{Pch}{$P_{Ch}$}
\psfrag{Pump}{To Pump}
\psfrag{Qin}{$Q_{in}$}
\psfrag{Qout}{$Q_{out}$}
\psfrag{Flow}{Flow Control}
\includegraphics[width=0.65\textwidth]{figs/vac-simple.eps}
\caption{Schematic diagram of a simple vacuum system.\label{fig:vac-simple}}
\end{center}
\end{figure}

At a fixed temperature and for a single-component gas, the throughput $Q$ 
across any plane in a connected system is simply proportional to the rate of 
particle flow; hence one can apply particle conservation principles to derive 
the dynamical equations of motion.  Consider the simple system shown in Figure 
\ref{fig:vac-simple}, consisting of a chamber connected to a pump and a mass 
flow controller (MFC). If the volume of the chamber is $V_{Ch}$, then it 
follows that
\begin{align}
-V_{Ch}\frac{dP_{Ch}}{dt} &= Q_{out} - Q_{in}\\
Q_{in} &= S_{MFC}P_\circ\\
Q_{out} &= S_{Pump}P_{Ch}
\end{align}
where $S_{MFC}$ is the {\sl volumetric flow} of the Mass Flow
Controller (MFC) at temperature $T_\circ$ and pressure $P_\circ$, and
$S_{Pump}$ is the {\sl pumping speed} at $T_\circ$ and $P_{Ch}$,
assuming the temperature to be constant.  Both $S_{Pump}$ and
$S_{MFC}$ are volume flow rates; to determine the actual amount of gas
(eg molecules per time) the density of the gas must also be known.
In steady state operation the chamber pressure is a constant,
so one obtains the relation
\begin{equation}
S_{Pump} = S_{MFC}\frac{P_\circ}{P_{Ch}}.
\end{equation}
This provides one method of estimating the speed of the pump if one has a 
calibrated MFC and accurate pressure sensors.  
%Usually, MFCs are calibrated at 
%standard temperature $T_s$ (273.15K) and standard pressure $P_s$
%(760~torr).  
In practice, gas flow rates are more often measured in
terms of sccm or slm (Standard Cubic Cm per Minute or Standard Litres
per Minute). These measurements specify mass flow, and are the
quantities that the mass flow controllers control. They describe the volume
that would be occupied by the gas flow in one minute, if that gas had
been at standard temperature ($T_s$ =273.15K) and pressure ($P_s$ =
760 Torr).
%The unit of standard volumetric flow is the sccm,
%or Standard Cubic Centimetre per Minute. 
The volumetric flow can be found from standard flow using
\begin{equation}
S_{MFC} = S_{SMFC}\frac{P_s}{T_s}\frac{T_\circ}{P_\circ}
\end{equation}
Here, $S_{SMFC}$ is the volumetric flow of the MFC under standard conditions.

Suppose the pumping speed is a constant and the flow rate $S_{MFC}$ is changed 
instantaneously to $S'_{MFC}$ from a steady state ($Q_{in}=Q_{out}$).  
$P_{Ch}$ responds (in this idealized model) exponentially with time constant 
$\frac{V_{Ch}}{S_{Pump}}$. From the above relations, the following 
differential equation is obtained:
\begin{equation}
\frac{V_{Ch}}{S_{Pump}}\frac{dP_{Ch}}{dt} = \frac{S'_{MFC}}{S_{Pump}}P_\circ 
- P_{Ch}
\end{equation}
For $t>t_\circ$, the solution is
\begin{equation}
P_{Ch}(t) = \frac{S'_{MFC}}{S_{Pump}}P_\circ + 
\left[\frac{S'_{MFC}}{S_{Pump}}P_\circ - 
P_{Ch}(t_\circ)\right]e^{-\frac{S_{Pump}}{V_{Ch}}(t-t_\circ)}
\end{equation}
where $t_\circ$ is the time when the flow rate changes from $S_{MFC}$ to 
$S'_{MFC}$ and the $P_{Ch}(t_\circ)$ is the pressure at $t_\circ$. This 
provides a method of estimating $V_{Ch}$ if the pumping speed is known.

\section{The Vacuum System}

The vacuum system used in this lab is shown schematically in figure 
\ref{fig:vac-system}, with the components identified in figure 
\ref{fig:vac-photo}. There are three vacuum chambers involved in this system: 
a Process Chamber, a 1.91~L Calibration Chamber and a Residual Gas Analyzer 
(RGA) Chamber. 

\begin{figure}[htb]
\begin{center}
\includegraphics*[angle=-90,width=\textwidth]{figs/vac-system.eps}
\caption{Schematic diagram of the vacuum system.\label{fig:vac-system}}
\end{center}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics*[angle =-90,width=\textwidth]{figs/vac-photo.eps}
\caption{The vacuum system.  {\bf A} is the turbo pump controller, {\bf B} the 
PENNING 8 gauge, {\bf C} the 4-channel controller and readout, as well as its 
outputs, {\bf D} is the rotary pump (vent valve is hidden behind {\bf B} and 
{\bf C}), and {\bf E} is the Calibration Chamber.  The Process Chamber, Green 
and Yellow Valves, Main Valve, RGA, and mass flow controllers are also marked.  
The needle valve to the RGA Chamber is behind the RGA.\label{fig:vac-photo}}
\end{center}
\end{figure}

The Process Chamber is on the top of the right-hand rack; it consists of an 
anodized aluminum housing (gold coloured) sealed to a base plate using an 
O-ring. The Calibration Chamber is connected to the Process Chamber through 
two valves which we'll refer to as the yellow and green valves, according to 
the colour of their knobs. To evacuate the Process Chamber and the Calibration 
Chamber, a rotary pump is connected to the Process Chamber via the Main Valve. 
The pressures of the Process and Calibration Chambers are measured by the 
capacitance heads. The sensitivities of the heads are 2 to 2000~mtorr for the 
Process Chamber and 1 to 1000~torr for the Calibration Chamber respectively. 
Two process gases (N$_\text{2}$ and He) can be fed into the system by 
appropriate controlling of the shut-off valves, bypass valves, MFC's, and 
green and yellow valves.

The RGA Chamber is in the left-hand rack and connects to the Process Chamber 
via a needle valve and an air-actuated valve. The RGA Chamber is quite small, 
just large enough to house the Residual Gas Analyzer. Since the filaments of 
the RGA must work at pressures lower than 1$\times$10$^\text{-5}$~torr, a cold 
cathode head is installed on the RGA Chamber to monitor the pressure. To 
evacuate the RGA Chamber, a turbo pump, backed with a diaphragm pump, is 
mounted directly to the chamber. The rotary pump alone cannot reach pressures 
in the 10$^\text{-5}$~torr range, but a turbo pump would deform from heating 
and possibly explode if asked to operate in atmospheric pressure. The purpose 
of the micrometer-actuated needle valve is to set the pressure difference 
between the Process Chamber and the RGA Chamber if the gases in the Process 
Chamber must be analyzed while its pressure is greater than the maximum 
working pressure of the RGA.

In the panel on the left-hand rack, the RGA Controller (VACSCAN), the 
4-channel controller and readout, and the cold cathode head indicator (PENNING 
8) are mounted from top to bottom. The VACSCAN measures gases' partial 
pressures in the RGA Chamber by ionizing them with a hot filament and passing 
them through a mass spectrometer, which measures intensity versus 
charge-to-mass ratio. This instrument is controlled by computer via an RS-232 
(standard serial) connection. The 4-channel controller and readout both 
controls and displays the flow rates of the MFCs (Channel 1 for N$_\text{2}$ 
and Channel 2 for He, both in sccm's). It also displays the pressures in the 
Calibration Chamber (Channel 3, in torr) and Process Chamber (Channel 4, in 
mtorr) - even though the display is labelled ``Flow". The PENNING 8 indicates the pressure in the RGA Chamber; at the 
bottom, there are two LEDs for monitoring the RGA's protective circuit. The 
green one lights when the pressure in the RGA Chamber too high, and the RGA is 
locked. The red LED indicates that the pressure in the RGA Chamber 
is lower than 1$\times$10$^\text{-5}$~torr and the RGA is ready.  The
protective circuit is also interlocked to the pressure in the Process
Chamber, which must be below 1000 mtorr for the protective circuit
(and RGA) to function.

\section{Method}
\subsection{Task 1:  Residual Gas Analysis}
\subsubsection{Gas Analysis for the RGA Chamber}

Since only the gases already present in the RGA Chamber are to be analyzed 
here, the air valve should be turned off, using the switch on the panel on the 
left-hand rack.

\begin{enumerate}

\item Turn on the cooling water for the turbo pump, and turn on the Main Power 
switch, above the air valve switch, to start the diaphragm pump. Find the 
turbo pump controller (TCP 015), and press the lowest black button on. Set the 
Select Switch on the Penning 8 gauge to range ``1'' and turn the power on for the PENNING 8. Monitor 
the pressure in the RGA Chamber and record approximately how long it takes to 
fall below 6$\times$10$^\text{-6}$~torr. Note that the unit of pressure on the 
PENNING 8 is {\bf not} torr, but mbar (760~torr = 1~atm = 101.325~kPa = 
1.01325~bar). Turn on the VACSCAN to warm it up, then open {\sl Vacuum Scan 
and Bar Chart.vi} on the computer's desktop and read the brief description of 
the software provided there while waiting for the pressure to fall.

\item When the pressure in the RGA Chamber falls below 
1$\times$10$^\text{-5}$~torr, you may analyze the gases in the RGA chamber, 
assuming the red LED is on. Open the appropriate part of the LabView code. Maintain the pressure below 8$\times$10$^\text{-6}$~torr before taking data, as the pressure in 
the chamber can sometimes fluctuate. Run the LabView VI and record the partial 
pressures of the five most abundant gas species as detected by the RGA. The 
filament is usually turned off automatically when data collection is 
completed. If you stop the VI while it is taking data, however, the filament 
will remain on -- to avoid burning out the filament, please turn it off 
manually. We recommend you let the VI run its course.

\item Compare the composition of the gas in the chamber with that of air.
\end{enumerate}

\subsubsection{Gas Analysis for the Process Chamber\label{sssec:RGA-PC}}

Now, the air-actuated valve must be opened, to allow the gases in the Process 
Chamber to reach the RGA.  

\begin{enumerate}
\item Please check that all valves in the right-hand rack are closed. The Main 
Valve is operated by the black knob above the rotary pump. Find the toggle 
switch on the motor and switch on the rotary pump if you have not already done 
so. After a few minutes, slowly open the Main Valve to let the rotary pump 
pump on the Process Chamber.

\item Switch on the power to the 4-channel controller and readout and select 
to channel 4. Monitor and control the pressure of the chamber until it is 
about 500~mtorr. How can you control the pressure in the Process Chamber? If 
the pressure in the Process Chamber is much lower than 500~mtorr, you need to 
turn off the rotary pump and vent the Process Chamber -- how do you do this?

\item The air-actuated valve is pneumatically controlled with a working 
pressure of 60~PSI -- check that the regulator on the bottle of compressed air 
is set correctly. As explained above, a needle valve makes it possible to 
analyze gases at with Process Chamber pressures greater than 1$\times$10$^\text{-5}$~torr.  Before 
turning the air valve's switch on, make sure the RGA filament is off.  When 
you switch the air valve on, monitor the pressure of the RGA Chamber using the 
PENNING 8 gauge;  if the pressure in the chamber increases, gas is flowing 
from the Process Chamber to the RGA Chamber. If the pressure is too high for 
your measurement, adjust the needle valve {\it slightly} to reduce the gas flow into 
the chamber.

\item Analyze the gases in the RGA Chamber as before, and compare the results 
with your previous measurements. After finishing this task, switch off the 
air-actuated valve.
\end{enumerate}

It is important that you understand that the results you just obtained are the 
partial pressures of the various gas constituents in the RGA Chamber, which 
are proportional to, but much lower than, the partial pressures of the same 
species in the Process Chamber.

\subsection{Task 2: Mass Flow Controllers}

An MFC accurately measures and controls the mass flow rate of a gas. Here you 
will learn how to control the external gas flow (N$_\text{2}$ and He) from the 
bottles to the Process Chamber.

\begin{enumerate}
\item Check that all valves in the right-hand rack are closed except the main 
valve. Set the regulators on the bottles of N$_\text{2}$ and He to about 
15~PSI.  Slowly turn on the shut-off valve for N$_\text{2}$. All the flow should pass through the MFC (the bypass valve is kept closed).

\item The MFC can be controlled either manually or automatically. For manual 
control, set the toggle switches for channel 1 to {\bf FLOW} and {\bf ON} and 
monitor channel 4 -- the Process Chamber. The pressure will increase after you 
turn on the Green Valve. Channel 1 now indicates the flow rate of 
N$_\text{2}$, which is about 20~sccm (the flow rate may not be displayed if 
the value is greater than 20~sccm). Make fine adjustments with the Main Valve 
until the system is balanced and the pressure in the chamber is about 
500~mtorr.

\item Turn the toggle switch {\bf ON}, repeat steps 3 and 4 of 
\ref{sssec:RGA-PC} to analyze the gases in the Process chamber, and compare 
the result with previous ones. Turn the toggle switch back {\bf OFF}.

\item Pump the Process Chamber back down to its minimum pressure. Repeat the process for He, using channel 2.  

\end{enumerate}

\subsection{Task 3: Measurement of Pumping Speed}
\subsubsection{Preparation}

\begin{enumerate}
\item This task uses only the Process Chamber and Calibration Chamber, so you 
should turn the RGA filament and air valve off if you haven't already done so. 
Switch off the VACSCAN because it may impact the data.


\item Open {\sl Gas Flow and Pumping Speed.vi} on the desktop computer, and 
read the instructions contained therein.  In the description there,
'Alternative mode' indicates alternating - the mass flow is
periodically turned alternated between two values.


\item In the ``Setting and Testing'' page of the VI, choose Testing
  Mode and vary all the setting parameters except the Measuring
  Chamber. Run the VI to see how the MFC drive voltage changes with
  different settings. If there are any problems, check that two cables
  have been connected from the DAC0 and DAC1 Analog Output to channels
  1 and 2 of the Set Point Input on the 4-channel readout as well as
  the Analog Inputs ACH4 and ACH5.  Check that four cables have been
  connected from ACH0, ACH1, ACH2 and ACH3 to the Transducer Scaled
  Outputs of channels 1 to 4 respectively.  These cables are all
  connected on the rear side of the lower BNC breakout panel for the
  4-channel readout.

\item To obtain the relation between the pressure in the Process
  Chamber and the flow rate, check that the shut-off valves, Green
  Valve and Main Valve in the right-hand rack are wide open -- why
  must these valves be open? Set the toggle switches for channels 1
  and 2 to EXT and ON; in the VI, set the Mode to Measuring and the
  Measuring Chamber to Process. Select proper settings, run the VI,
  and monitor the flow rate and total pressure with time. Vary the
  voltage applied to each channel (always keep it $<$~5~V) to get a
  rough idea of how the pressure changes with flow rate. You have the
  option of storing the data in a spreadsheet format on the hard disk
  when the VI terminates.
\end{enumerate}

\subsubsection{Calibration of the N$_\text{2}$ MFC}\label{sssec:vac-calib}

\begin{enumerate}
\item Ensure that there is no gas flow into the Process Chamber. Turn the 
Green and Yellow Valves wide open, and monitor the pressure in the Calibration 
Chamber from the Capacitance Head. Close the Green Valve when the pressure in 
the Calibration Chamber has stabilized. Make sure that channel 1's shut-off 
valve is open and that its toggle switch is set to EXT;  set the Mode to 
Measuring and the Measuring Chamber to Calibration. Set the Flowing Gas to 
Nitrogen and the Driving Voltage (Max) for N$_\text{2}$ to be 5~V. Choose the 
period to be 30 seconds (Direct Flow Pattern) and the Number of Cycles to be 
1. Turn channel 1's other toggle switch ON, and check that there is still no 
flow through the MFC -- how can you check?

\item Run the VI; the N$_\text{2}$ gas flows through the MFC directly to the 
Calibration Chamber, whose pressure increases. Save your results to calibrate 
the maximum N$_\text{2}$ flow rate for the MFC (at 5~V). Compare your results 
with the standard flow rate using the equation in the Theory section 
(\ref{vac-theory}), and obtain the MFC working temperature T$_\circ$. Do you 
need to calibrate the flow rate for the MFC with different driving voltages? 
Why or why not?
\end{enumerate}

\subsubsection{Measurement of the N$_\text{2}$ Pumping Speed}

Close the Yellow Valve and open the Green Valve again. Vary the driving 
voltage to the N$_\text{2}$ MFC to obtain its flow rate ($S_{MFC}$) versus 
Process Chamber pressure ($P_{Ch}$), from which you can calculate the pumping 
speed of N$_\text{2}$ for different driving voltages. The flow rate of the MFC 
($S_{MFC}$) is obtained from the value of $S_{SMFC}$, T$_\circ$, and $P_\circ$ 
from step 1 in \ref{sssec:vac-calib}. Graph the pumping speed versus Process 
Chamber pressure -- is the pumping speed a constant? Before starting the VI, 
ensure that you have pumped down to base pressure and that the period is long 
enough to establish a steady state before the MFC is turned off.

\subsubsection{Volume of the Process Chamber}

\begin{enumerate}
\item Using the maximum flow rates and the known calibration volume, carry out 
a procedure similar to \ref{sssec:vac-calib} above to determine the effective 
volume of the Process Chamber. Measure the external dimensions of the Process 
Chamber and estimate its volume -- how can you explain the difference between 
these two values?

\item Select Alternative Flow Pattern. Adjust the period to be a few time 
constants (about 30 seconds), and select a proper value for the Number of 
Cycles. Since the pumping speed varies with pressure, a small driving voltage 
change should be selected (e.g. 5~V for Max and 4.5~V for Min) where the 
pumping speed can be roughly considered to be constant. For N$_\text{2}$ flow, 
record the transient pressure data, and use it to obtain an 
independent value for the Process Chamber volume. Compare the results with 
those obtained above and give a detailed explanation.
\end{enumerate}

\section{Shutdown Procedures}

The shutdown procedure is very important for this lab. If you aren't familiar 
with the procedure, you can usually follow this principle: the earlier you 
turned something on, the later you turn it off.

\begin{enumerate}
\item Turn the toggle switches OFF for all channels, and wait until the 
Process Chamber reaches its base pressure, then close all the valves in the 
right-hand rack.

\item Turn off the RGA filament and the air-actuated valve if you haven't 
already done so, then switch off the VACSCAN, the PENNING 8 and the turbo 
pump.

\item Turn off the rotary pump and the 4-channel controller and readout, then 
finally the Main Power.

\item Open the vent valve (a small toggle valve at the inlet to the rotary 
pump) and close it -- this vents the rotary pump. Shut off the valves on the 
gas bottles, as well as the cooling water. 

\item Check the system and confirm that you have performed all the above 
steps.
\end{enumerate}

\section{Reference}

O'Hanlon, J.F., \underline{A Users Guide to Vacuum Technology}, John Wiley \& 
Sons, New York (1989).

\setcounter{secnumdepth}{1}

%% To Do:
%%
%% Q=SP <==> V=IR could be fleshed in/reworded a bit (Throughput = Gas Load?),
%% Figure vac-system:  Turbo Pump vent valve has to be below turbo pump, or
%%      turbo would be destroyed.  
%% Experiment could stand to be reworded, shortened.  It uses far too many 
%%      words.  
%% Tongkai capitalized all references to valves, etc.  I only capitalized a 
%%      few.  I hope I stayed self-consistent.  I thought it was silly to 
%%      capitalize everything, but some standards (or abbreviations) should be 
%%      introduced.  
%% Extra references needed -- is there a copy of Varian's Vacuum Technology 
%%      Seminar booklet in the lab?  

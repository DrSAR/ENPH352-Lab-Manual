\chapter{Optical Pumping of Rubidium}
\renewcommand\chapname{Rb}
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\ba}{\begin{align}}
\newcommand{\ea}{\end{align}}
\newcommand{\faom}{f_{\mathrm{aom}}}
\newcommand{\frep}{f_{\mathrm{rep}}}
\newcommand{\fceo}{f_{\mathrm{ceo}}}
\newcommand{\fhet}{f_{\mathrm{het}}}
\newcommand{\fbeat}{f_{\mathrm{beat}}}
\newcommand{\vcw}{\nu_{\mathrm{CW}}}
\newcommand{\bvec}[1]{\mathbf{#1}}
\chapternumberreset

\section{Purpose}
\label{sec:purpose}
\begin{enumerate}
	\item To become familiar with the occurrence and characteristics of optical pumping
	\item To examine some atomic spectra, and understand atomic structure
\end{enumerate}
%
\section{Introduction}
\label{sec:intro}

Optical pumping is a method of using photons to place or move atoms into a particular spin state. Sometimes this is referred to as spin polarizing a gas, as very efficient optical pumping can create a polarized sample, where all atoms are in the same spin state. This is useful, for example, in magnetic-resonance imaging, performing spectroscopy on a gas, or if one wanted to control the types of interactions that take place in an atomic ensemble. The techniques of modern optical pumping were introduced in the early 1950s, with a Nobel prize awarded in 1966 for the technique, and optical pumping is still widely used today in many modern physics labs. 

\section{Theory}
\label{sec:theory}

The atom used in this experiment is Rubidium (an alkali metals) that has 37 protons and electrons. Like the other alkali metals, Rb has only a single valence electron, which lies in an $s$-orbital. Rb has two naturally occurring isotopes: $^{85}$Rb and $^{87}$Rb. While only $^{85}$Rb is stable, the half-life of $^{87}$Rb is over 49 billion years (for reference, the universe is estimated to be about 13.7 billion years old, as of the writing of this lab manual). You will investigate both isotopes in this lab. \\

In order to understand and investigate optical pumping, there are three main concepts that must be understood: 1) atomic structure, which will tell you which levels are involved in the optical pumping process, 2) photon absorption, which describes how photons interact with the Rb atoms, and 3) the polarization of light. This lab manual outlines each of these three topics, but it is strongly recommended that you look at some (or all) of the sources in the references section, and to read through the manual that comes with the apparatus.

\subsection{Atomic Structure}
\label{subsec:theory}

Like many physical models, atomic structure starts with a fairly simple picture of the energy levels within an atom, and then makes small corrections to the model to account for additional terms or perturbations to the system. The key is that each successive correction or perturbation is on an energy scale that is much smaller than the energy scale of the unperturbed system.
\paragraph{Electronic Structure} The starting point of atomic structure is the electronic configuration, where electrons fill orbital shells until all 37 electrons are placed:
%
\be
1s^22s^22p^63s^23p^63d^{10}4s^24p^65s^{1}5p^{0}
\ee
%
where the number represents $n$, the principal quantum number, and the letter represents $\ell$, the electron orbital angular momentum. Each value of $\ell$ is represented by a number. That is, $l=0$ is a $s$-orbital, $l=1$ is a $p$-orbital, $2$ is $d$, $3$ is $f$, etc. You can remember this using the mnemonic \textit{\textbf{s}mart \textbf{p}eople \textbf{d}on't \textbf{f}ail \textbf{g}eometry}. The $5p$ orbital represents the first available excited state for the electron. Since all the electrons in the inner shells are paired, Rb (and all the alkalis) can be modeled as though it was a simple hydrogenic system with a single valence electron.
%
\paragraph{Fine Structure} One consideration that has not been included in the electronic structure model is that the electron itself has a spin, $S = 1/2$, and an associated magnetic dipole moment. This spin is important because it couples with the orbital angular momemtum, $L$, and perturbs the energy of the system. Physically, in the frame of the electron, the positively charged nucleus appears to be moving, which induces a magnetic field. In turn, the electron spin interacts with the field, producing a (small) shift in energy. This additional term in the Hamiltonian has the form $\bvec{L}\cdot\bvec{S}$. We can define a new quantum number $\bvec{J} = \bvec{L} + \bvec{S}$, where J is the total angular momentum, and can take on values ranging from $L+S$ down to $|L-S|$ in integer steps, according to the rules of addition of angular momentum. Each state can be labelled by a new term symbol, $^{2S+1}L_{J}$ where $S$ is the electron spin, $L$ is the electron orbital and $J$ is the total angular momentum. Since the ground state of Rb is a $5s$ state, $L=0$ and therefore J only takes on one value, $J=1/2$. However, the first excited state is a $5p$ state, so $L=1$ and J can take on two values, either $J=1/2$ or $J=3/2$, see Figure \ref{fig:87Rb_structure}. The transition from $^2S_{1/2} \rightarrow~^2P_{1/2}$ is called the D$_1$ transition, and from $^2S_{1/2} \rightarrow~^2P_{3/2}$ is called the D$_2$ transition.Although it is a little confusing, the $S$ used in the term symbols which label the fine structure states correspond to the letter label for $L$ (s,p,d,f etc.), and is distinct from the $S$ in the $2S+1$ term, which represents the electron spin. Note that the splitting between the two excited states is about $7~$THz (we are giving the energy in frequency units, $E=h\nu$), which is small compared the energy difference between the $5s$ and $5p$ states, which is on the order of $380~$THz.
%
\paragraph{Hyperfine Structure} The nucleus has a spin as well, which is labeled with the quantum number $I$. Similar to fine structure, this nuclear spin couples with the total angular momentum, $J$, which introduces a new term in the Hamiltonian that looks like $\bvec{J}\cdot\bvec{I}$. Likewise, we can define another new quantum number, $\bvec{F} = \bvec{J} + \bvec{I}$, where F can take on values ranging from $J+I$ down to $|J-I|$ in integer steps. \\

Up until this point, there hasn't been any distinction between the two different isotopes of Rb. Aside from a different number of neutrons, they also differ in the value of their nuclear spin. This means that each isotope has a different hyperfine structure. In the ground state, the splitting between hyperfine levels is on the order of a few GHz, and in the excited state the splitting is on the order of 100's of MHz, which is much smaller than the THz scale of fine structure splitting. You should work out the different hyperfine levels of Rb for yourself. As a check, Dan Steck has compiled an excellent reference document (see Section \ref{sec:ref}). In addition, the atomic structure for $^{87}$Rb is shown in Figure \ref{fig:87Rb_structure}.
%
\paragraph{Zeeman Effect} In the absence of any external fields (for example, a magnetic field), each of the hyperfine levels is actually $2F+1$ degenerate levels, which are labeled by $M_F$, the projection of $F$ along some quantization axis, where $-F \le M_F \ge F$ in integer steps. An external magnetic field breaks this degeneracy as the magnetic dipole moment of the atom interacts with the magnetic field. In the case of a small magnetic field (if you are wondering what small means in this context - and you should be - keep reading... but you should do that anyway), the energy of each $M_F$ level relative to energy in the absence of the magnetic field is,
%
\be
\label{eq:small_z}
E_{\mathrm{Z}} = g_F\mu_B BM_F
\ee
%
where $\mu_B$ is the so-called Bohr magneton and,
%
\be
g_F = g_J\frac{F(F+1)+J(J+1)-I(I+1)}{2F(F+1)}
\ee
%
where,
%
\be
\label{eq:gf}
g_J = 1+\frac{J(J+1)+S(S+1)-L(L+1)}{2J(J+1)}
\ee
%
Notice that for small (there's that word again) $B$ the levels fan out with an equal spacing between adjacent $M_F$ levels. Since the energy spacing increases with increasing magnetic field strength, you could conceivably make the Zeeman splitting as large as the hyperfine splitting (or bigger) if you applied a large enough field. This would break the assumption that each correction is small compared to the unperturbed system. To be clear: Eq. \ref{eq:small_z} is only valid when the magnetic field is small enough such that the Zeeman splitting is much less than the hyperfine splitting. You should work out approximately what field you would need to apply to break this condition, and keep this value in mind when you do this experiment. If you enter a regime where the magnetic field is not small (now you know what this means) then the energy difference between adjacent Zeeman levels will not be equal. You can investigate this effect in this lab. 
%
\begin{figure}
	\centering
		\includegraphics[width=0.9\textwidth,clip]{figs/87Rb_structure.eps}
	\caption{Atomic structure of $^{87}$Rb ($I = 3/2$, showing electronic, fine and hyperfine structure. Also shown on the right is the small field Zeeman splitting as a function of magnetic field. Note that this sketch is not to scale.}
	\label{fig:87Rb_structure}
\end{figure}
%
\subsection{Photon Absorption}
%
If a photon incident on a Rb atom has the correct energy, such that the photon energy, $h\nu$, matches the energy difference between two atomic energy levels (and the lower energy level actually contains an electron), the atom can absorb the photon, and the electron is excited to the higher lying level. However, there are particular rules which govern which transitions are allowed to occur. The most common type of transition is an \text{electric dipole transition}, which has the selection rules that,
%
\be
\Delta F = 0, \pm 1 \quad \mathrm{and}\quad \Delta M_F = 0, \pm 1 \ee
%
with the exception that an electron in a $F=0$ state cannot undergo a transition to another $F=0$ state. The same selection rules apply to transitions between states with different $L$ or different $J$ quantum numbers. \\

In this lab, you might be able to imagine many different possible transitions that could occur, each a different energy scale. However, we only need to consider two. First, you will use optical light (in the near IR at 795~nm) to drive transitions on the D$_1$ line. Secondly, you will use an RF field with a frequency on the order of kHz or MHz. Since the splitting between different hyperfine levels (in the ground state) is on the order of several GHz, this radiation will only let you drive transitions where $\Delta F=0$. That is, only between different Zeeman ($M_F$) levels, within the same hyperfine manifold. Whether the photon (either the optical or RF) drives a transition where $\Delta M_F = 0,+1,-1$ depends on the polarization of light.
%
\subsection{Polarization}
%
Consider an electric field vector (ie. light) travelling along the z-axis (in the positive direction). This electric field vector can have components along the x and y-axis, with a relative phase difference between the two components. There are three types of polarization we need to consider, and each type of polarization can drive transitions with specific $\Delta M_F$ values.
\begin{itemize}
	\item \textbf{Linear Polarization} If the light is linearly polarized, the relative phase difference between the two components is zero. Light that is linear ($\pi$ polarized) drives transitions where $\Delta M_F= 0$.
	\item \textbf{Right-Circular Polarization} If the light is RCP, the two components are equal in magnitude, with a relative phase of $\pi/2$, where the component along the x-axis lags behind the component along the y-axis. If you were to look at the electric field vector from the positive z-axis, it would trace out a helix in the clockwise direction (hence, right). RCP ($\sigma^+$ polarized) light will drive transitions where $\Delta M_F = 1$.
	\item \textbf{Left-Circular Polarization} If the light is LCP, the two components are equal in magnitude, with a relative phase of $\pi/2$, where the component along the y-axis lags behind the component along the x-axis. If you were to look at the electric field vector from the positive z-axis, it would trace out helix in the counterclockwise direction (hence, left). LCP ($\sigma^-$ polarized) light will drive transitions where $\Delta M_F = -1$.
\end{itemize}
In the system in this experiment, the z-axis is defined by the quantization axis of the experiment. In this case, this is the direction of magnetic field, which is oriented along the direction of propagation of the light. \\

For optical pumping to work efficiently (see Section \ref{subsec:opt}), the light needs to be either RCP or LCP. The light emitted from the Rb bulb used in this experiment does not have a well defined polarization. To polarize it, you will first pass it through a linear polarizer. Next, you pass it through a \textit{quarter wave plate} (QWP). A QWP has two orthogonal axes, and is designed such that the index of refraction along one axis (the slow axis) will slow down the component of the electric field along that axis such that it exits the wave plate a quarter wave ($\pi/2$) out of phase relative to the electric field that transmitted along the other axis  (the fast axis). This is exactly the phase shift we need to make our light LCP or RCP. \\

In this experiment, the optical light will either drive transitions with $\Delta M_F = +1$ or $\Delta M_F = -1$. It happens that the RF radiation is along an axis orthogonal to the magnetic field so it will always drive transitions with $\Delta M_F = \pm1$. Of course, since the energy in the RF field is only large enough to drive transitions between different Zeeman levels, where $\Delta F = 0$, a transition with $\Delta M_F = 0$ wouldn't be very interesting, since the initial and final would be the same!

\subsection{Optical Pumping}
\label{subsec:opt}
In this lab, we optically pump the Rb gas using the (optical) light driving the D$_1$ transition. That is, the light drives transitions between hyperfine levels in the $^2S_{1/2}$ manifold to another hyperfine level in the $^2P_{1/2}$ manifold (obeying, of course, the selection rules outlined above). The light will either drive transitions with $\Delta M_F = +1$ or $-1$ (depending on whether the light is RCP or LCP). However, when the electron decays from the excited state, the transition can have any of $\Delta M_F = 0,\pm1$. This means that, \textit{on average}, each decay event will have $\Delta M_F = 0$, while each absorption even will have (let's assume RCP light) $\Delta M_F=+1$. This will cause the electrons to be pumped to the ground state with the highest value of $M_F$ (sometimes referred to as a stretched state), see Figure \ref{fig:PUMP}. \\
%
\begin{figure}
	\centering
		\includegraphics[width=0.6\textwidth,clip]{figs/opt_pump_cartoon.eps}
	\caption{Cartoon diagram of optical pumping. In this example, both the ground (lower) and excited (upper) levels are $F=1$ states. Solid lines represent absorption events with RCP light, where $\Delta M_F=1$ and dashed lines represent the \textit{average} decay event, where $\Delta M_F=0$. Multiple absorption and decay events will lead the electrons to be pumped into the ground state with the largest $M_F$ value. Notice that this final state is dark. That is, an electron cannot absorb any of the incident light because there is no excited state with $M_F = 2$.}
	\label{fig:PUMP}
\end{figure}
%
%
In a perfect experiment, all the atoms would end up in this final stretched state. In reality (and this lab) there are many other processes that act to undo the optical pumping (for example, collisions between Rb atoms can exchange angular momentum). Instead of putting all the atoms in the stretched state, the optical pumping light establishes a new equilibrium in the system where there are more atoms in the stretched state then there would be in the absence of the light. \\

The state that the electrons are optically pumped into should (ideally) be a dark state. That is, a state where the atom is no longer able to absorb a photon, even though the incident light has the right energy. If not, atoms in this state will continue to scatter light and possibly leave the state. How is it possible for a state to be dark? Selection rules! Go back to your sketch of the hyperfine levels in the ground and excited state for either isotope of Rb (and/or look at Figure \ref{fig:87Rb_structure}) and see if you can find which state could be dark. Remember the selection rules that $\Delta F = 0, \pm 1$ and $\Delta M_F = +1$ or $-1$ depending on the polarization of the light. \\

In fact, since the state is dark, it creates the perfect signal to observe whether or not the sample is optically pumped. If more atoms are in the dark state, the more light is transmitted (i.e. not absorbed) in the cell. If fewer atoms are in the dark state, the less light is transmitted. Therefore, by measuring the intensity of the light after the cell, you can infer how well pumped the sample is. \\

Finally, the RF field drives transitions between different $M_F$ states within the same hypefine manifold (that is, within states with the same $F$). This field \textbf{can} drive transitions out of (and into) the dark state, which acts to reduce the number of atoms in the optically pumped dark state. In this experiment, you will set a particular frequency for the RF field, and vary the magnetic field strength inside the cell. At just the right magnetic field strength, the energy difference between Zeeman levels will equal the energy of the RF photons (see Eq. \ref{eq:small_z}) and you will observe a loss of atoms from the dark state via a decrease in the optical light intensity transmitted through the cell.

\section{Procedure}
\label{sec:procedure}
The details on how to run the experiment are well laid out in the manual for the apparatus that is found in the lab room. You should read the description of the experiment and apparatus in the manual carefully.

\subsection{Task 1 - Measurement of the Nuclear Spins}
Your goal is the measure the value of the nuclear spin ($I$) for both $^{85}$Rb and $^{87}$Rb. Recall, from Eq. \ref{eq:gf} that the value of the nuclear spins influences the splitting of the Zeeman levels (as a function of magnetic field). You will measure the RF transition frequencies of each isotope as a function of magnetic field, from which you can infer $g_F$ and $I$. This procedure roughly follows section \textbf{4B. Low Field Resonances} in the experiment manual.
%
\begin{enumerate}
	\item Turn on the appartus and set the cell temperature to around 50$^\circ$C. Although this needs to be the first thing you do when you want to run the experiment, it should also be the first thing you do when you get to the lab, because the Rb bulb and cell take a few minutes to warm up. 
	\item Install the optics in the experiment (see fig. 4A.1 of the experiment manual). You will need to install one plano-convex lens before the cell to collimate light from the Rb lamp and a second lens after the cell to focus the light onto a photodector. You will also need to use a linear polarizer and a quarter wave plate to set the correct polarization of light before the cell. There is a second linear polarizer available that you can use before the detector to test how well you have set the circular polarization. You will also need to insert and interference filter before the cell, which ensures that only light that drives the $D_1$ transition enters the cell.
	\item For this task, you will not need to use the Horizontal Field coils, so you should disconnect them so no current can run through them. You will only need to use the Horizontal Magnetic Field Sweep coils and Vertical Field coils. The ``monitor"
	% not the "recorder" output
output for the sweep field should be connected to CH1 of the oscilloscope, and the detector amplitude output should be connected to CH2. It's best if you trigger on CH1 (the magnetic field sweep) for this experiment. 
	\item In order to determine the current running through the sweep coils, there is a monitor output on the front panel of the apparatus. Since the current passes through a one ohm resistor, the voltage measured across the output is equal to the current in the coils. The coils are arranged in a Helmholtz configuration, and the field at the atoms is (approximately) given by:
\be
B (\mathrm{Gauss}) = 8.991\times10^{-3} \left(\frac{IN}{R}\right)
\ee
where $I$ is the current, $N$ is the number of turns of wire that make up the coils, and $R$ is the mean radius. You can find the coil specifications in the experiment manual. 
	\item There is some background magnetic field present in the experiment. You need to minimize and measure the size of this field before you can take useful data. With no RF field applied from the function generator, vary the sweep horizontal magnetic field. (NB The switch sticks; you may have to toggle it to make sure it sweeps). You should see a dip occur in the light intensity, which occurs because of the Zeeman levels are degenerate (which acts to scramble the Zeeman states). Adjust the vertical field coils to minimize the width of this dip, and write down the current in the horizontal sweep field at the location of the dip. You will need this later to properly calibrate the magnetic field. 
	\item Apply an RF signal (say 150~kHz) to the RF coils, and set its amplitude to whatever value you want (just don't choose the wrong value). Slowly sweep the horizontal magnetic field using the sweep coils searching for dips in the light intensity, which represent Zeeman resonances. Measure the current at which each resonance occurs. Once you have found a transition, adjust the RF amplitude to a value that optimizes the signal. For one choice of RF frequency, you should see two dips. Why? It's also possible you might see some other much weaker dips. If so, turn down the RF amplitude (and think about why you saw these extra dips). If you can't see any extra dips, try turning up the RF amplitude until you do (and think about why you saw these extra dips).
	\item Measure the transition frequencies of each isotope as a function of the sweep coil current. You should take at least four or five points for each isotope. 
	\item Plot the transition frequency as a function of magnetic field. You will need to convert your recorded sweep coil current for each frequency to a magnetic field strength (be sure to account for the residual field you measured previously). Fit your data to extract $g_F$ and $I$. What is the relationship between the transition frequency and the magnetic field? Is this what you expect? Why or why not?
	\item Would this experiment have worked if you used a optical filter that passed light that drove the $D_2$ transition instead of the $D_1$ transition? Why or why not?
\end{enumerate}
%
\subsection{Task 2 - Choose Your Own Adventure}
There are many other experiments that are possible on this apparatus. Take a look through Chapter 4 of the TeachSpin manual for the experiment, and do one of the other experiments. Some options include:
\begin{enumerate}
	\item{Rb Absorption as a function of cell temperature}
	\item{Quadratic Zeeman effect}
	\item{Transient / Dynamical effects}
\end{enumerate}
If you aren't sure which one sounds interesting, ask the instructor or TA and we can help you pick.
	


\section{References}
\label{sec:ref}

\begin{itemize}
	\item [1] Eric D. Black, \textit{Physics 77 Lab Manual: Optical Pumping}, available online \\
\verb! http://www.pma.caltech.edu/~ph77/labs/optical-pumping.pdf!
	\item [2] Daniel A. Steck, \textit{Rubidium 85 D Line Data}, available online at \verb! http://steck.us/alkalidata! (revision 2.1.5, 19 September 2012)
	\item [3] Daniel A. Steck, \textit{Rubidium 87 D Line Data}, available online at \verb! http://steck.us/alkalidata! (revision 2.1.5, 19 September 2012)
	\item [4] Daniel A. Steck, \textit{Classical and Modern Optics}, available online at 
\verb!http://steck.us/teaching! (revision 1.5.0, 13 June 2013)
	\item [5] David J. Griffiths, \textit{Introduction to Quantum Mechanics}, 2nd ed. Pearson Prentice Hall,
2005.
	\item [6] Teachspin, \textit{Optical Pumping of Rubidium OP1-A: Guide to the Experiment}, 2002. 
\end{itemize}

%\section{Theory}

%\section{Procedure}

%\subsection{Task 1: Measurement of Noise Voltage}

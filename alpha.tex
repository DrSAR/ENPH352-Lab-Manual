\chapter{Alpha Particle Range in Air}
\renewcommand\chapname{Alpha}
\chapternumberreset
\section{Purpose}

\begin{enumerate}
\item To study the energy loss of charged particles in matter.
%and to measure the energy loss as a function of particle energy.
\end{enumerate}

\section{Theory}

As charged particles traverse matter, they lose energy by ionizing atoms 
along their path.  As the amount of energy lost per collision is 
approximately constant, each particle will have a path length or range in 
a given stopping material dependent on its incident energy.  Thus for any 
particular type of particle the range is a definite function of the energy 
as shown in Figure \ref{fig:alpha_ugly}.  The mean range of particles is 
defined as shown in Figure \ref{fig:alpha_range}, where $n_o$ is the 
number of particles reaching a detector with no stopping material present 
and $n$ is the number reaching the detector with stopping material of 
$x$~cm between the source and the detector.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=.7\textwidth]{figs/alpha-f1.eps}
\caption{\label{fig:alpha_ugly}Range-energy relationship for $\alpha$ 
particles.}
\end{center}
\end{figure}

The mean range of alpha particles can also be found by plotting the 
differential range or the difference in the number of alpha particles detected 
for each increment of absorber as shown in Figure \ref{fig:alpha_peak}.  The 
distribution of ranges about the mean range is Gaussian i.e.
\begin{equation}
\frac{\Delta n_i}{n_0} = \frac{\Delta x_i}{\alpha\sqrt{\pi}} e^{-\frac{(x_i-R)^2}{\alpha^2}}
\end{equation}
where $\Delta n_i/n_o$ is the fraction of particles having range between 
$x_i$ and $x_i + \Delta x_i$.  The range straggling parameter, $\alpha$, 
is the half width of the range distribution at $1/e$ of maximum as shown 
in Figure \ref{fig:alpha_peak}.  The best measurements have shown that 
$\alpha = 0.15 R$

\begin{figure}[htb]
\begin{center}
\psfrag{x}{$x$}
\psfrag{y}{$\frac{n}{n_o}$}
\psfrag{1}{1}
\psfrag{5}{$\frac{\text{1}}{\text{2}}$}
\psfrag{Rr}{$R$}
\psfrag{Rn}{$R_n$}
\includegraphics{figs/alpha-f3.eps}
\caption{\label{fig:alpha_range}The extrapolated number-distance range $R_n$ 
exceeds the mean range R by 0.886$\alpha$, where $\alpha$ is the 
range-straggling parameter.}
\end{center}
\end{figure}

The distribution is broadened by alpha particles scattering off the inside 
of the chamber, the finite energy resolution of the detector, and the 
thickness of the source itself.  If the width of the differential range 
curve is broad or asymmetric, the energy might be best determined using 
the extrapolated range.

The rate of energy loss of alpha particles is given in Melissinos (page 
157) as 
\begin{equation}
-\frac{dE}{dx} = \left(\frac{4\pi Z^2 e^4}{m_e v^2}\right) 
n_e\ln{\frac{m_e v^2}{I}}
\label{energy_loss}
\end{equation}
where $I$ is some average ionization potential of the material, $Z$ is the 
atomic number of the incoming particle, $v$ is its (non-relativistic) 
velocity, and $n_e$ is the electron density of the scattering material.  

\begin{figure}[htb]
\begin{center}
\psfrag{ecks}{$~~~~~x$}
\psfrag{why}{$-\frac{1}{n_o}\frac{dn}{dx}$}
\psfrag{sigma}{$\alpha$}
\psfrag{R}{$R$}
\psfrag{max}{$\frac{1}{\alpha\sqrt\pi}$}
\psfrag{mid}{\!$\frac{1}{e}\cdot\frac{1}{\alpha\sqrt\pi}$}
\includegraphics{figs/alpha-f2.eps}
\caption{\label{fig:alpha_peak}Schematic ``number-range curve,'' illustrating 
a symmetric distribution of ranges about the mean range R.  The ``range 
straggling parameter'' $\alpha$ is $\sqrt{2}$ times the standard deviation 
of the range distribution.}
\end{center}
\end{figure}

While the concepts explored in this experiment have plenty of applications in 
nuclear physics and nuclear medicine, there are less obvious applications.  In 
particular, semiconductors are often doped in a similar way, although the 
dopants are normally neutral atoms and are slowed by collisions with the 
lattice.  

\subsection{Chamber Air Pressure Measurement}

A solid state pressure transducer is used to convert pressure into voltage 
readings.  The transducer uses strain gauges in a bridge configuration to 
measure the displacement produced in a solid plate by having an absolute 
vacuum (static) applied to one side and the working pressure applied to the 
other.

The transducer is calibrated at $P$ = 1~atm and vacuum.  Measure the 
atmospheric pressure when you do the lab using the Fortin barometer on the 
end of the bookshelves near the lab door.  The other calibration point is 
obtained by pumping the chamber to $P \approx$ 0~mmHg using the vacuum 
pump.  A linear interpolation is used to determine the chamber pressure 
from the transducer response.  %Refer to the graph in the bench file.

In this experiment, the range will be varied by increasing the air pressure in 
the vacuum chamber.  Adjust the source-detector distance to $d_0 \approx 
4.5$~cm, so that the effective distance, $d$, between the source and detector 
will be approximately given by
\begin{equation}
d = \frac{P}{P_{atm}} d_0
\end{equation}

\section{Procedure}

{\bfseries BEFORE STARTING:}  Read the precautions on using the surface 
barrier detector located at the lab bench. Do not touch either the detector or the radioactive sample 
($\sim$1$\mu$Ci $^\text{241}$AmO$_\text{2}$).  

\begin{figure}[htb]
\begin{center}
\psfrag{scope}{Oscilloscope}
\psfrag{amp}{Amplifier}
\psfrag{preamp}{Pre-amp}
\psfrag{detector}{Detector}
\psfrag{Source}{Source}
\psfrag{chamber}{Chamber}
\psfrag{Ph}{Pulse Height}
\psfrag{analyser}{Analyzer}
\psfrag{Bias}{Bias}
\psfrag{Voltage}{Voltage}
\psfrag{Power}{8V Power}
\psfrag{Supply}{Supply}
\psfrag{DVM}{DVM}
\psfrag{Black}{Black}
\psfrag{Red}{Red}
\psfrag{White}{White}
\psfrag{rotary}{Rotary pump}
\psfrag{thermocouple}{Thermocouple}
\psfrag{gauge}{Gauge}
\psfrag{Solid}{Solid}
\psfrag{State}{State}
\psfrag{Transducer}{Transducer}
\includegraphics{figs/alpha-setup.eps}
\caption{\label{fig:alpha_setup}Schematic diagram of the apparatus.}
\end{center}
\end{figure}

\begin{enumerate}
\item Connect the apparatus as shown in Figure \ref{fig:alpha_setup}, and 
turn on the equipment. Open the UC530 application. Ensure that the bias voltage (``high voltage") for the detector is set to zero. Check that when you turn it on it will be negative (hardware switch in one setup, software in the other).

\item Calibrate the pressure transducer as outlined above, and verify the 
separation between source and detector is $\approx 4.5$~cm, recording the 
actual value.  Evacuate the chamber when finished.

\item START THE NEGATIVE BIAS VOLTAGE AT 10-20 VOLTS AND TURN UP SLOWLY, MAKING 
SURE NOT TO EXCEED THE MAXIMUM ALLOWED FOR THE DETECTOR, THE BIAS
POWER SUPPLY GOES UP TO 1000V!
\begin{center}
\begin{tabular}{l|l}
Detector serial number & Max voltage\\ \hline
16-625J & -100 volts\\
16-721J & -75 volts\\
16-469A & -100 volts\\
\end{tabular}
\end{center}

\item Adjust the bias voltage for best signal to noise and adjust the 
amplifier gain. On one set-up with the older data acquisition system,
adjust the gain so that the pulses are approximately 8 volts on the
oscilloscope. With the newer data acquisition system you should set
the gain so that the highest energy pulses are on scale.

\item Observe the pulse height spectrum using the pulse height analyzer, 
perhaps re-adjusting the amplifier gain so that the energy spectrum is at 
the right of the display. Note: to clear the spectrum in the older setup, use control-F2.

\item Close the valve to the pump and observe the change in pulse height 
as air is admitted to the chamber.

\item Now re-evacuate the chamber and record the count rate and the 
centroid on the pulse height analyzer.  You will want to set your 
Region-Of-Interest (ROI) to be just above the noise in the first several 
channels.  Observe the rate and centroid channel at several different 
pressures. The points near the end of the range (near atmospheric 
pressure) are the most important, so get plenty of data in this range.  
How would you calculate the error on the number of counts?  

\item Convert the transducer voltage readings into pressure values using 
your calibration.

\item Convert the channel of the centroid into energy assuming a linear 
calibration.  Take the centroid channel at vacuum to equal the full source 
energy and assume channel 0 is at 0 MeV. 

\item Plot the centroid of the energy as a function of effective distance 
in air and fit a curve to the data (the curve you fit should be based on Eq.~\ref{energy_loss}).

\item Plot the derivative of the curve, again as a function of effective 
distance, and display together with the curve from above on the same axis.  
This gives you the energy loss per unit length and is called the Bragg 
Curve.  Is it what you expected?

\item Plot the count rate as a function of effective distance.  Also plot 
the derivative of the curve as a function of effective distance and again 
display the curves together. 

\item Determine the range and range straggling parameter.
\end{enumerate}

When you are finished with the equipment, please turn the bias power
supply back down to 0 V, and vent the pump when you turn it off: fully
open the two air admittance valves and then open the pump valve and
then shut off the pump.
\section{References}

\begin{itemize}
\item Evans, {\bf The Atomic Nucleus}, Pages 650 - 667.
\item Melissinos, {\bf Experiments in Modern Physics}, sections 5.2.2, 
5.2.3, and 5.5.3 (first edition).
\end{itemize}
%
%\section{Appendix - Useful Mathmatica Commands}
%
%Mathmatica is installed on the physics server and is invoked by typing 
%{\tt math} at the prompt.  Before you can graph in Mathmatica you must 
%first set the graphic display.  To do this you must type setenv DISPLAY 
%$\langle$address \#$\rangle$ before you enter Mathmatica, where 
%$\langle$address \#$\rangle$ is the ethernet address of the machine you 
%are using.
%
%In Mathmatica you enter data in the following format \{ \{\#, \#\}, \{\#, 
%\#\}, \{\#, \#\}, .... \} followed by the enter key.  To assign the data 
%to a variable just type $\langle$variable name$\rangle$ = \%  The \% 
%symbol is just a wildcard which represents the most recent output.
%
%Some other commands you may find useful are:
%\begin{enumerate}
%\item {\tt ListPlot}: Plots a list of data points.
%
%\item {\tt Fit}: Finds the least squares fit to a list of data.
%
%\item {\tt Table}: Generates a list of values of a given expression for a 
%range of values.
%
%\item {\tt Plot}: Plots a function over a given range.
%
%\item {\tt D}: Gives the partial derivative of a function.
%
%\item {\tt Show}: Displays graphics and can show several plots combined.
%
%\item {\tt Display}: Writes graphics to the specified output channel.
%\end{enumerate}
%
%Once you have exited Mathmatica, your graphics files can be exported in a 
%garbled PostScript format.  You will need to convert them into proper 
%PostScript before using them.  At the physics prompt type {\tt psfix 
%{\slshape filename} $\rangle$ {\slshape new-filename}} where filename is 
%the name of your file.  

\chapter{Photoelectric Effect}
\renewcommand\chapname{Photo-e$^-$}
\chapternumberreset
\section{Purpose}

\begin{enumerate}
\item To perform a classic quantum mechanics experiment.  

\item To study properties of some metals.

\item To determine a fundamental constant.
\end{enumerate}

\section{Theory}
In the late 19$^\text{th}$ century, physics was able to satisfactorily 
explain almost everything.  One of the few remaining unexplained phenomena 
was the photoelectric effect, discovered by Heinrich Hertz in 1887, who 
noticed by accident that light shining on a metal produced a current.  
Further experiments on the phenomenon led to J.J.\ Thompson's discovery of 
the electron in 1897.  In 1905, Einstein published a revolutionary paper 
explaining blackbody radiation by quantizing light with $E\propto\nu$, and 
predicting the photoelectric effect, including several unobserved 
properties of it.  He was later awarded the Nobel prize for this paper.  
Milliken didn't believe Einstein, but ended up confirming his predictions 
by 1913.  

Unlike the classical description of light, which identifies energy with 
amplitude, the particle description of light links energy with frequency:  
\begin{equation}
E=h\nu =\hbar\omega
\end{equation}
where $h$ and $\hbar$ are two different forms of Planck's constant.  When 
such a photon strikes a metal, it can be absorbed and excite a band 
electron.  If its energy was greater than the ionization potential of the 
metal (work function), the electron can be freed entirely, provided it was 
in a sufficiently high-energy state to start with.  

%%% Noise starts again %%%

\section{Procedure}
The electronic system used in this experiment consists of a pre-amplifier 
(small ``brass" box) with a gain of $\approx$200, connected to a main unit 
containing a power amplifier with adjustable gain, a filter used to set the 
pass band, followed by an RMS to DC converter, whose conversion gain can be 
set indirectly via the {\bf INPUT RANGE CONTROL} lever.

The unit should be turned on and given at least 5-10 minutes to warm up before
taking any data.  While the amplifier is warming up you should familiarize 
yourself with the settings and be sure you can answer the following questions 
before proceeding to the next step.  The accuracy of this experiment depends 
on your knowledge of the amplifier and converter system and on your 
understanding of what you are trying to measure.

Connect the signal generator through an attenuator to the amplifier input and 
view the {\bf AC MONITOR} on the oscilloscope.  Set the attenuator to reduce 
the source voltage to a level enough above the noise level to be 
distinguishable from it, yet low enough that you don't overload the amplifier 
--- typical settings are between 60 and 110~dB, depending on what you choose 
as an input voltage.  Adjust the attenuator and try varying the frequency and 
amplitude of the signal generator.  \underline{TRY NOT TO OVERLOAD THE 
AMPLIFIER}.

What happens to the voltage response at high and low frequencies?\\
Why do you think it does this?\\
How linear is this amplifier?\\
What does it do with signals that are too large?

Try changing the settings and observe the results. What happens to the output 
when you change the bandwidth, gain and range? 

When selecting the settings to be used for the actual experiment, you should 
recognize that the same settings are used for the whole lab.  Therefore it is 
crucial that you choose appropriate settings.  Spend some time testing the 
different settings keeping these requirements in mind:
\begin{itemize}
\item The signal must be amplified enough so that you can measure the noise 
voltages of the resistors
\item You must be able to measure and integrate the complete gain curve (zero 
to infinity) using the available apparatus
\item High frequencies should be avoided, since the load begins to have 
significant reactive components.
\end{itemize}

\subsection{Task 1: Measurement of Noise Voltage}
$\star\star\star${\bf Warning}:  Connecting the oscilloscope changes the data 
--- only connect the scope for troubleshooting, never when taking data.

\begin{enumerate}
\item Connect the various resistances (ranging up to 10~k$\mathsf\Omega$) 
across the pre-amplifier input, and measure the noise level with the DMM 
connected across the {\bf DC OUTPUT} of the amplifier.  

\item Plot your $V_{DC}$ vs.\ $R$ data to get a relationship you can use when 
calculating $k_B$.  Why did the voltmeter not read zero when $R$ was zero?  
\end{enumerate}

\begin{figure}[htb]
\begin{center}
\psfrag{Task1}{\Large\sf Task 1}
\psfrag{Task2}{\Large\sf Task 2}
\psfrag{Preamp}{\Large\sf Pre-amp}
\psfrag{Attenuator}{\Large\sf Attenuator}
\psfrag{Function}{\Large\sf Function}
\psfrag{Generator}{\Large\sf Generator}
\psfrag{Oscilloscope}{\Large\sf Oscilloscope}
\psfrag{DVM}{\Large\sf DVM}
\psfrag{input}{\large\sf input}
\psfrag{DCmonitor}{\large\sf DC monitor}
\psfrag{ACmonitor}{\large\sf AC monitor}
\psfrag{Amplifier}{\Large\sf Amplifier}
\includegraphics[width=0.8\textwidth]{figs/noisefig.eps}
\caption[]{A block diagram of the thermal noise apparatus.}
\label{fig:noiseblock}
\end{center}  
\end{figure}

\subsection{Task 2: Measurement of $G(f)$}
$\star\star\star${\bf Warning}: The DC output will likely change when the {\bf
AC MONITOR} or signal generator is connected to the oscilloscope.  If this 
happens, take data with the {\bf AC MONITOR} disconnected and, to avoid 
clipping, just watch that the current doesn't exceed 80~$\mu$A on the 
analog dial meter.

\begin{enumerate}
\item To avoid reflections, BNC lines carrying variable-frequency AC require 
50~{$\sf\Omega$} terminations, which are built into many (but not all) 
devices' BNC connections.  You will need to make sure your lines are properly 
terminated.  You can assume that the input impedance of the amplifier system 
is \underline{very} high.

\item Making sure not to overload the amplifier or clip the signal, determine 
a value for $G(f)$ over the entire range of frequencies.  Take enough data to 
resolve the shape of $G(f)$ so you can accurately determine $\gamma$.

\item Do some more-rigorous measurements of $G(f)$ at a few representative 
frequencies by fixing the frequency and attenuation and then plotting 
$V^2_{out}$ vs $V^2_{in}$.  Why is this method more rigorous?

\item Determine $\gamma$, and therefore a value for Boltzmann's constant, 
using the results from Task 1.

\item (Optional) As a good exercise you should also construct a Bode plot to 
see how closely the -3~dB drop from the peak gain relates to the frequency 
cutoff points.
\end{enumerate}

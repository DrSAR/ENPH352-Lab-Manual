\hyphenation{cry-o-gen-ics}
\newcommand{\tc}{{\slshape T}$_\text{c}$}
\chapter{Lock-in Detection:  High-{\slshape T}$_\text{c}$ Superconductivity}
%\typeout{Hyperref really dislikes "Tc" in the chapter heading.}
\renewcommand\chapname{HTSC}
\chapternumberreset

\section{Purpose}
%\begin{enumerate}
%\item 
To introduce you to lock-in amplifiers and some topics of current 
interest to researchers in the department.

%\item To examine some properties of a high-temperature superconductor.
%\end{enumerate}

\section{Introduction}
Superconductivity was first observed in 1911, by Dutch physicist Heike 
Kammerlingh-Onnes.  When he cooled mercury in liquid helium, its DC 
resistivity abruptly vanished around 4K.  It wasn't until the 1950's that a 
satisfactory explanation was found --- two electrons couple to a phonon 
(quantized lattice vibration), lowering their energy and changing even their 
most basic properties.  By the 1970's, superconductors were fully understood, 
metallic compounds had been found with superconducting transition temperatures 
(\tc ) as high as 23K, and theorists had proven that it would be impossible 
for anything to have a \tc\ much higher than 25K.  

In late 1986, Swiss physicists J.\ Georg Bednorz and K.\ Alex M\"uller 
discovered superconductivity at nearly 40K in 
La$_{\text{2-}x}$Ba$_x$CuO$_\text{4}$, an otherwise poorly-conducting ceramic.  
This startling discovery sparked an intense effort to find and study more such 
compounds.  In January 1987, the first compound was found with a \tc\ above 
the temperature of liquid nitrogen (77K) --- 
YBa$_\text{2}$Cu$_\text{3}$O$_{\text{7}-x}$ (YBCO, \tc = 93.7K for $x = 
0.08$).  Today, many such compounds are known, the highest recorded 
ambient-pressure \tc\ is 138K, and there is no accepted explanation for why 
they superconduct.  While these temperatures are still quite cold by most 
people's standards, they're extremely high for low-temperature physics, and 
are readily attainable.  

Low-temperature (conventional) superconductors are in common use for 
high-field magnets (as used in MRI machines), but the liquid helium required 
to keep them cold makes the cost prohibitive for most other uses.  Liquid 
nitrogen is far cheaper than helium, so high-\tc\ superconductors should 
eventually see far more common use.  An application of particular current 
interest is in cellphone base stations and satellites, where the absence of 
electrical resistance can make bandwidth constraints less of a problem.  A 
variety of superconductor-based quantum computing schemes have also been 
suggested, and are being investigated.  

\section{Theory}
\begin{figure}[htb]
\begin{center}
\psfrag{z}{$z$}
\psfrag{Vacuum}{Vacuum}
\psfrag{Superconductor}{Superconductor}
\psfrag{Eqn1}{$\vec B = \mu_o\vec H_{ext}$}
\psfrag{Eqn2}{$\vec B = \mu_o\vec H_{ext} e^{-z/\lambda_L}$}
\psfrag{Hext}{$H_{ext}$}
\psfrag{lambda}{$\lambda_L$}
\includegraphics{figs/htsc-decay.eps}
\caption{Magnetic field decaying into a superconductor.\label{fig:htsc-decay}}
\end{center}
\end{figure}

Aside from having zero DC resistance, superconductors are also perfect 
diamagnets.  This well-understood property, known as the Meissner Effect, is 
commonly used for spectacular demonstrations --- a magnet placed atop a piece 
of YBCO will spontaneously levitate when the YBCO goes superconducting.  The 
ground state for this system is to have currents on the surface of the 
superconductor, cancelling out the magnetic field.  The currents, and thus the 
fields, in fact decay exponentially into the sample (Figure 
\ref{fig:htsc-decay}), with a characteristic length $\lambda_L$, the magnetic 
(or London) penetration depth.  In the high-\tc\ superconductors, $\lambda_L$ 
depends on the temperature and what direction the currents are travelling, and 
ranges from $\sim$0.1 to 10$\mu$m.  Figures \ref{fig:htsc-field}a and b show 
our superconducting crystal in an applied magnetic field above and below \tc.  

\begin{figure}[htb]
\begin{center}
\psfrag{a}{\bfseries a)}
\psfrag{b}{\bfseries b)}
\psfrag{c}{\bfseries c)}
\psfrag{Hext}{\textcolor{red}{$\vec H_{ext}$}}
\psfrag{Area A}{\textcolor[rgb]{0,0,0.566}{Area $A$}}
\psfrag{L}{$L$}
\psfrag{ell}{$a$}
\psfrag{tee}{$t$}
\includegraphics[width=0.6\textwidth]{figs/htsc-field.eps}
\caption{\label{fig:htsc-field}Perfect diamagnetism in a superconductor.  
{\bfseries a)}~For {\slshape T} $>$ \tc, YBCO is slightly metallic, and does 
not significantly alter the magnetic field.  {\bfseries b)}~For {\slshape T} 
$<$ \tc, the superconductor expels the applied field, and the pickup coil sees 
a reduced field.  {\bfseries c)}~In our apparatus, the sample is in the upper 
of two counterwound coils connected in series.  In a uniform field (above 
\tc), there is no net field, but below \tc\ the two coils see different 
fields, so there is a net field detected in the circuit.}
\end{center}
\end{figure}

From Faraday's Law, the voltage induced in a wire loop in a time-varying 
magnetic field is
\begin{equation*}
V_{loop} = - \frac{d\Phi}{dt}
\end{equation*}
where the magnetic flux $\Phi = \iint \vec B\cdot d\vec a$.  In our setup, an 
external AC magnetic field 
\begin{equation}
\vec B_{ext} = B_{ext}\sin(\omega t)\hat x =\mu_oH_{ext}\sin(\omega t)\hat x
\end{equation}
is applied, and the voltage in a pickup coil (of length $L$, cross-sectional 
area $A$, and $N$ turns per unit length) would be 
\begin{align}
V_{coil} &= -NLA\mu_oH_{ext}\omega\cos(\omega t), \label{eqn:htsc-vcoil} \\
\intertext{or}
\left|V_{coil}\right| &\propto LAH_{ext}.
\end{align}
It is customary to use $\vec H$, the field in matter, instead of $\vec B$, 
because $\vec H$ is the field we can directly control.  Amp\`ere's Law in 
matter reads 
\begin{align}
\vec\nabla\times\left(\frac{1}{\mu_o}\vec B - \vec M\right) &= \vec J_f \notag \\
\intertext{or}
\vec\nabla\times\vec H &= \vec J_f
\end{align}
where $\vec J_f$ is the free current and $\vec M$, the ``magnetization'', is 
the magnetic dipole moment per unit volume.  

However, we're not interested in measuring the applied field;  we're looking 
for small changes to it due to the presence of a superconducting sample.  The 
apparatus used has two identical coils a short distance apart, wound in 
opposite directions and connected in series.  In a uniform AC field, they will 
contribute equal voltages, and these will cancel, leaving no signal.  However, 
if one half contains a piece of YBCO (Figure \ref{fig:htsc-field}c), that 
half will give a different voltage 
\begin{align}
\left|V_{coil}'\right| &\propto \left[LA - \ell_x\ell_yt\left(1 -
     \frac{2\lambda_L}{t}\right)\right]\\
&= C\left(LAH_{ext} - |\vec m|\right)
\end{align}
where $C$ is a calibration constant and $\vec m$ is the sample's total 
magnetization.  The voltage detected will be
\begin{equation}
\left|V_{coil} - V_{coil}'\right| = C|\vec m| = C'\left(1 - 
\frac{2\lambda_L}{t}\right).
\label{eqn:htsc-detected}
\end{equation}

A measurement of the sample's magnetization, then, can be used to determine 
the London penetration depth.  

\section{The Apparatus}

Figure \ref{fig:htsc-probe} shows the key components of the apparatus.

\begin{figure}[htb]
\begin{center}
\psfrag{He gas}{He gas}
\psfrag{10Vpp}{10V$_\text{pp}$}
\psfrag{1kHz}{10kHz}
\psfrag{to pump}{to pump}
\psfrag{Lock-in}{Lock-in}
\psfrag{GPIB}{GPIB}
\psfrag{PC}{PC}
\psfrag{DMM}{DMM}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{Pre-amp}{Pre-amp}
\psfrag{4w-ohm}{4-wire $\sf\Omega$}
\psfrag{LN2}{Liquid Nitrogen}
\psfrag{Field coil}{Field Coil}
\includegraphics[width=\textwidth]{figs/htsc-probe2.eps}
\caption{\label{fig:htsc-probe}Schematic diagram of the apparatus.  There are 
four subsystems --- the vacuum/cryogenics plumbing and the magnetic detection, 
thermometry and heating circuitry.  The computer monitors the temperature and 
detected signal, and runs current through the 50$\sf\Omega$ resistor to heat 
the probe.}
\end{center}
\end{figure}

\subsection{Plumbing and Temperature Control}

The probe is cooled to 77.4K in liquid nitrogen, which is held in a dewar 
flask.  A quartz test tube separates the liquid nitrogen from the probe, 
which allows the probe to reach temperatures other than 77K and prevents 
damage from thermal shock.  Beryllium-copper springs make thermal contact 
between the copper coil assembly and the test tube, to keep the coils 
cold.  The sample and the thermometer and heater chips are mounted on a 
sapphire (Al$_\text{2}$O$_\text{3}$) plate, which is a very good thermal 
conductor.  This is thermally insulated from the rest of the probe with 
teflon/quartz.  Figure \ref{fig:htsc-probephoto} shows a photo of this 
part of the probe.  

\begin{figure}
\begin{center}
\includegraphics*[width=.7\textwidth]{figs/htsc-probephoto.eps}
\caption{\label{fig:htsc-probephoto}Photograph of the susceptometer.}
\end{center}
\end{figure}

To cool the probe, the quartz tube is filled with helium gas, which is 
then pumped out for thermal isolation.  How does this helium business 
work, and why do we use helium for this purpose?  To warm the sapphire 
plate, a current is applied to the chip resistor;  the temperature reached 
will depend on the current applied.  

\subsection{Thermometry}

The sapphire plate also holds a silicon diode which acts as a thermometer.  
Diodes thermometers are usually used by passing a constant current
through them and measuring the voltage.  We will use the four-wire
resistance function of the multimeter to do this. (see Figure 
\ref{fig:htsc-diode}).  

\begin{figure}[htb]
\begin{center}
\psfrag{eh}{\bfseries a)}
\psfrag{bee}{\bfseries b)}
\psfrag{DMM}{DMM}
\psfrag{a}{$\alpha$}
\psfrag{b}{$\beta$}
\psfrag{V}{\Large V}
\psfrag{I+}{I$_+$}
\psfrag{V+}{V$_+$}
\psfrag{I-}{I$_-$}
\psfrag{V-}{V$_-$}
\psfrag{Thermal Gradient}{Thermal Gradient}
\psfrag{Troom}{{\slshape T}$_{room}$}
\psfrag{TN2}{{\slshape T}$_{N_2}$}
\includegraphics[width=0.7\textwidth]{figs/htsc-diode.eps}
\caption{\label{fig:htsc-diode}{\bfseries a)} Two-wire and {\bfseries b)} 
four-wire measurements on a diode thermometer.  The DMM's current 
and voltage connections are marked.}
\end{center}
\end{figure}

In a four-wire resistance measurement, a constant current is supplied to
the load through two leads, and the voltage across it is measured using
the remaining two leads.  The resistance is then obtained through Ohm's
law.  Four-wire measurements are far superior to two-wire for small
resistances, because the latter measures the resistance of the load
{\slshape and} the wires.  In a four-wire measurement, there is negligible
current in the voltage leads, so only the load is measured.  In the case
of the diode thermometer shown in figure \ref{fig:htsc-diode}, the thermal
gradient we apply would make it virtually impossible to account for the
resistance of the leads, making 4-wire measurements crucial.  The diode is
also extremely non-Ohmic, so a constant bias current is required.

The current through a diode varies as
\begin{equation}
I = I_o\left(e^{eV/nk_BT}-1\right)
\end{equation}
where $I_o(T)$ is the reverse-biased saturation current, V is the
diode voltage, and $T$ is the temperature, and $n$ is diode-dependent
factor known as the 'nonideality factor'. Usually $1< n < \sim 2.5$.
$I_o$ is also a function of temperature, and is often expressed as:
\begin{equation}
I_o = KT^{5/2}e^{-V_g/2k_BT},
\end{equation}
in which $K$ depends on the details of the diode and $E_g$ is the band
gap of the semiconductor.
Taking the logarithm of $I$, we find:
\begin{equation}
V=\frac{n/2e}V_g - \frac{nk_B}{2}\left(\frac{5}{2}\ln T -\frac{\ln
I_f}{K} \right).
\end{equation}
The weak logarithmic dependence on $T$ can usually be ignored, and so
we find
\begin{equation}
V=V_0 - m(I) T.
\end{equation}
The voltage measured across the diode, for fixed forward bias current
is thus expected to vary linearly with temperature, and can be
calibrated using measurements at 77.4 K and room temperature.
It is important to note that the measurements {\bfseries\slshape 
must} be performed on the same (manually set) range on the DMM, to ensure a 
constant bias current. 

\subsection{Magnetometry and Lock-in Detection}
%% \label{sec:htsc-lock-in} %% No subsection numbering

Outside the dewar, a large coil is used to apply an AC field ($\vec H_{ext}$) 
to the probe.  To ensure field uniformity, it is quite useful to have the 
probe centred inside this coil.  The probe itself has two counterwound coils, 
as described in the Theory section, to detect magnetization in the sample 
rather than just the applied field.  

While the signal due to the sample is already quite small, we're looking at an 
even smaller signal --- we want to determine how $\lambda_L$ changes with 
temperature.  Recall from Equation \ref{eqn:htsc-detected} that the detected 
signal $\propto (\text{\slshape sample size} - \text{\slshape shell of 
thickness } \lambda_L)$.  The $\lambda_L$ term is the interesting one, but 
it's the smaller term by a factor $\frac{2\lambda_L}{t}\sim 10^3 - 10^5$.  Any 
minute changes in $\lambda_L$ will be completely drowned out by noise 
proportional to the sample size.  

The advantage we have in this experiment is that we know exactly what 
frequency our signal should have.  To separate it from broadband noise, all we 
need is a filter.  Because the noise is much stronger than the signal, we need 
a very narrow filter --- our signal has a vanishingly small frequency width, 
so the probability of having broadband noise at exactly the same frequency 
also vanishes.  Unfortunately, the feasability of constructing 
such a narrow filter for an arbitrary frequency vanishes at least as quickly.  

A common solution would be to do a Fourier transform of your detected
signal, so you can look at the frequency spectrum and pick off the
frequency you want.  The problem is getting a frequency spectrum in the 
first place.  Fourier transforms require significant amounts of data and 
data analysis.  The solution implemented in this experiment instead uses a 
special type of phase-sensitive filter and voltmeter known as a lock-in 
amplifier.

The lock-in is given reference and sample signals, with phases $\phi_{r}$ 
and $\phi_{s}$, and the reference signal is converted to a sine wave of 
unit amplitude:
\begin{align}
V_r &= \sin\left(\omega_r t + \phi_r\right) \notag \\
V_s &= {\cal A}\sin\left(\omega_s t + \phi_s\right) \notag
\end{align}
The reference and sample signals are then mixed to yield an output at 
their sum and difference frequencies.  
\begin{equation}
V_r\times V_s = \frac{\cal A}{2}\left[\cos\left(\left(\omega_s - 
    \omega_r\right)t + \phi_s - \phi_r\right) - \cos\left(\left(\omega_s + 
    \omega_r\right)t + \phi_s + \phi_r\right)\right]
\end{equation}
In this experiment, the output frequency will be the same as the input 
frequency, so the first term will be at DC and the second at a frequency 
of $2\omega$.  

Next, the mixed signal passes through a low-pass filter to yield only the 
DC component:
\begin{equation*}
V_r\times V_s \xrightarrow{\text{low-pass filter}} \frac{\cal 
    A}{2}\cos\left(\phi_r - \phi_s\right)
\end{equation*}
Frequencies other than the reference frequency do not give DC components, and 
can be removed by the low-pass filter.  However, while it's far easier to make 
a low-pass filter than a band-pass filter at arbitrary frequency, the filter 
still has a width --- the inverse of its time constant.  You may have noticed, 
though, that signals are maximized for $\phi_r = \phi_s$, and a 
phase difference of $\pi/2$ will not contribute to the output.  So we're 
getting phase information for free (or we can take advantage of a known phase 
relationship to further improve our data).  

This is the basis of phase- and frequency-specific detection.  Lock-in 
amplifiers provide an immense advantage when measuring small signals if 
the system can be driven by an AC voltage with a very stable amplitude and 
phase.  

Why is phase-sensitive detection important in this experiment?  Because of the 
derivative in Equation \ref{eqn:htsc-vcoil}, we're interested in the inductive 
response of the detector coils, which will be $-\pi/2$ out of phase with the 
driving field.  Since the driving coil is out of phase with the function 
generator by $+\pi/2$, the voltage from the sample's magnetic moment will be 
{\slshape in phase} with the function generator.  On the other hand, there 
will be resistive signals $\pi/2$ out of phase with the function generator --- 
resistance both in the wires and in the superconductor (it has zero DC 
resistivity, but a small AC resistivity).  Phase-sensitive detection allows us 
to distinguish between the inductive and resistive components.  

In this experiment, the counterwound detector coils provide $V_{sample}$ and 
we're primarily interested in the inductive response, at zero phase shift.  If 
time permits and you feel like investigating the resistive response, then you 
can set the reference phase to $\pi/2$.  

\section{Procedure}
\subsection{Study I --- Lock-in Signal Detection}
This part of the experiment will introduce you to lock-in detection of small 
signals.  

%Recall that resistance $R$ is determined by resistivity $\rho$, 
%length $D$ and cross-sectional area $A$ through 
%\begin{equation*}
%R = \int_0^D\frac{\rho}{A}dx.
%\end{equation*}

\begin{enumerate}
\item What are the highest and lowest available time constants on the 
lock-in?  What is the best frequency resolution you would expect from each?  
(This unit has a second filter stage, labelled as ``Post,'' for steeper 
roll-off.  You should disable it, by setting it to ``None.'')  

%\item As a test, use the function generator to measure the resistance of a small, known resistor, say a few $\Omega$. Connect this in series to a much larger resistance, say 10~k$\Omega$. Use to function generator to put a apply 10~V p-p to this combination, terminating the output with a 50~$\Omega$ BNC inline resistor. Calculate the current in this circuit and thus the potential drop across the smaller resistor. Use the lock-in amplifier to measure this voltage. Is it as expected? Take amplitude readings for phase shifts from -$\pi$ to $\pi$.  Does this behave as you expect?  

%\item {\bfseries Are 2 function generators available to check roll-off?}

\item As a test, we'll use the lock-in amplifier to measure the resistance of a very small resistor. 
You should find a small slab of aluminum at your bench, with four female 
banana connectors.  Use the digital multimeter's four-wire resistance function 
(method mentioned earlier) to find its resistance, and thus its resistivity.  
The voltage-sensing leads are labelled as ``$\sf\Omega$4W Sense'' and the 
current source (I$_+$) is marked with a lightning bolt 
(\includegraphics[height=10pt]{figs/htsc-zot.eps}).  Remember, you want to measure 
the voltage due to a current, and you're trying to eliminate any effects from 
resistance in the leads or contacts.  Perform a two-wire measurement for 
comparison.  

\item Now, connect the function generator to a $\sim 50 \Omega$ resistor (use the resistance box) in {\bf series} 
with the aluminum bar. This acts as a current source.  
You can measure the current through the bar by measuring the (AC) voltage across the known resistance with the multimeter.
Now connect the lock-in amplifier to the aluminum bar. Using the lock-in voltage measurement and the previous current measurement, determine the resistance, and resistivity, of the bar.

%
%\item Finally, replace the oscilloscope with the lock-in, remembering to also 
%connect the function generator to the lock-in's reference input (what phase?).  
%Using the lock-in signal, again determine the resistance and thus the 
%resistivity.  How do the various resistivity measurements compare to each 
%other and to the accepted value?  
\end{enumerate}

\subsection{Study II --- Magnetization of 
YBa$_\text{2}$Cu$_\text{3}$O$_\text{6.95}$}

You will now determine the magnetic properties of a crystal of YBCO by AC 
susceptometry.  Refer to Figure \ref{fig:htsc-probe} for a diagram of the 
apparatus.  

\begin{enumerate}

\item Open LabView program.

\item Take a measurement of the diode voltage
under bias current, using the four-wire resistance measurement
function of the multimeter, with the range manually set to
10k$\Omega$.  The multimeter must remain on this resistance range for
the duration of the experiment.  As long as the bias current stays
fixed, it is ok for you to record the diode voltage in units of ohms
(these are the units that the multimeter will supply the voltage in -
because it thinks its making a resistance measurement).  A resistance
value for 77K should already be present in the LabVIEW program,
completing the temperature calibration --- you don't need to adjust
this point.

\item Connect the function generator (10kHz, 10V$_\text{pp}$) to the lock-in's
Reference input and the outer coil, and the detector coil (susceptometer output) to the lock-in's
A input.  Raise the drive coil to envelope the susceptometer, watching the signal
on the lock-in.  It should reach a maximum, then decrease toward the centre of
the drive coil.  Why?  Find the position that minimizes the signal, and use 
this position for the remainder of the experiment.  

%\item Activate the detection circuitry (lock-in, pre-amp) and the applied 
%field (function generator:  10kHz, 10V$_\text{pp}$).  With the susceptometer 
%still outside of the drive coil, there should be no observable signal --- 
%verify this.  Insert the susceptometer into the drive coil, and watch the 
%signal on the lock-in.  It should reach a maximum, then decrease toward the 
%centre of the drive coil.  Why?  Find the position that minimizes the signal, 
%and mark it on the glass tube.  

\item The test tube containing the probe needs to have 1~atm He gas in it.  
To this end, pump out whatever gases may be in it using the vacuum pump, then 
gently fill it with helium gas and close the valve.  Fill the dewar with 
liquid nitrogen.  The diode's resistance should rise rapidly, then level off 
as the probe approaches 77K.  

%Make sure LabView is taking data.???cew

\item  With the probe at liquid nitrogen temperature, remove the helium.  Start the pump, and, leaving the He-line valve closed, pump down
%.  Slowly open
%the valve, and allow the probe to pump down 
to $\lesssim$ 200~torr.
Set the phase on the lock-in to zero, and ensure that the signal is
not off the scale.  {\bf Leave the pump pumping on the dewar for the
duration of the experiment, and then leave it pumping when you leave
the room. } With the helium removed, you may {\bfseries\slshape
slowly} warm the sample, using the DC power supply as necessary.  Take
data as you do so.  You want the probe to warm at about 0.5~K/min.
Continue recording the lock-in output and resistance to at least 105K.

\item You might expect your plot of lock-in voltage vs.\ temperature to be 
zero above \tc , where the field fully penetrates the superconductor,
but your graph will probably show an offset and a slope in this
regime.  What causes can you think of for the slope and offset? To
remove this background signal, fit the linear portion above \tc , and
subtract this line from the {\slshape entire} data set.

\item Rescale your data so the voltage values vary from zero to one.  Since 
$V\propto |\vec m(T)|$, this plot shows $\frac{|\vec m(T)|}{|\vec m(T_o)|}$ 
vs.\ $T$.  

\item Finally, use Equation \ref{eqn:htsc-last2} or \ref{eqn:htsc-last} to 
extract $\lambda_L(T)$.  To do this, take $\lambda_L(\text{77K}) =$ 3000\AA, 
and the sample's dimensions as 5.50 $\times$ 4.35 $\times$ 1.30~mm$^3$, 
$\pm$0.05 in each.  Generate a log-log plot of $\lambda_L(T)$ over the range 
\tc\ -- 10K $\rightarrow$ \tc .  The {\slshape x}-axis should use 
$\frac{\text{\tc}-\text{T}}{\text{\tc}}$ instead of {\slshape T}.  Is there 
any evidence of a power law $\lambda_L(T) \propto \left(\frac{\text{\tc}-
\text{T}}{\text{\tc}}\right)^n$?  (Note that the assumption $\lambda_L \ll t$ 
is not true very close to \tc , because $\lambda_L$ diverges at \tc .)  

\item Leave pump on (Tongkai will turn off). Lower coil, and put a piece of paper over the top to catch dripping condensation from the susceptometer.

\end{enumerate}

\section{References}
For E\&M, any undergraduate text will work (e.g.\ Griffiths' {\slshape 
Introduction to Electrodynamics}).  A high school-level (and overly optimistic 
and partially wrong) introduction to superconductivity may be found at 
\href{http://www.superconductors.org}{\tt http://www.superconductors.org}, 
but Tinkham's 4$^\text{th}$ year-level text {\slshape Introduction to 
Superconductivity} is {\bfseries much} better.  Additionally, some similar 
research has been done at UBC --- Chris Bidinosti's Ph.D.\ thesis, available 
from on-campus at \\ 
\href{http://www.physics.ubc.ca/~supercon/archive/bidinosti-phd.pdf}{\tt http://www.physics.ubc.ca/$\sim$supercon/archive/bidinosti-phd.pdf} 
may be useful, particularly parts of its Introduction. The growth of the 
YBCO crystal is described in\\ 
\href{http://arxiv.org/abs/cond-mat/0209418}{\tt 
http://arxiv.org/abs/cond-mat/0209418}.


\vspace{1em}
\noindent
For diode thermometry:
T. Huen, Rev.\ Sci.\ Instrum. {\bf 41}, 1368 (1970).
\section{Appendix --- Magnetic Moment of a Superconducting Platelet}

Consider a superconducting platelet of thickness $t$ and broad dimensions 
$\ell_x\times\ell_y$ in a uniform external magnetic field $H_{ext} \hat x$, as 
shown in Figure \ref{fig:htsc-xtal}.  Furthermore, assume that $\ell_i\gg t$, 
so that we can consider the platelet to be an infinite slab ($\lambda_b$ 
becomes $\lambda_L$ and $\lambda_c$ plays no role).  

\begin{figure}[htb]
\begin{center}
\psfrag{x1}{$\hat x$}
\psfrag{x2}{$\hat y$}
\psfrag{x3}{$\hat z$}
\psfrag{d1}{$\ell_x$}
\psfrag{d2}{$\ell_y$}
\psfrag{d3}{$t$}
\psfrag{lambda2}{$\lambda_b$}
\psfrag{lambda3}{$\lambda_c$}
\psfrag{Hext}{$\vec H_{ext}$}
\includegraphics[width=0.7\textwidth]{figs/htsc-xtal.eps}
\caption{\label{fig:htsc-xtal}The crystal provided, with the currents and 
penetration depths shown.  $\ell_x \approx$ 5.50~mm, $\ell_y\approx$ 4.35~mm, 
and $t\approx$ 1.30~mm.}
\end{center}
\end{figure}

The magnetic flux density ($\vec B$) at any point inside the platelet will be 
the sum of the exponentially decaying fields from the two sides of the sample:
\begin{equation}
B_x(z) = D\left[e^{(z-t/2)/\lambda_L} + e^{-(z+t/2)/\lambda_L}\right]
\end{equation}
where $D$ is a constant that must be chosen to satisfy the boundary 
conditions $B_x(\pm t/2) = \mu_oH_{ext}$.  The result is
\begin{equation}
B_x(z) = \mu_oH_{ext}\frac{\cosh(z/\lambda_L)}{\cosh(t/2\lambda_L)}.
\end{equation}

Finally, using the general expression $\vec B = \mu_o\left(\vec H+\vec 
M\right)$, we can derive the magnetization of an infinite slab of 
thickness $t$:
\begin{equation}
\vec M = H_{ext}\left(\frac{\cosh(z/\lambda_L)}{\cosh(t/2\lambda_L)} - 1\right)
\end{equation}

The total magnetic moment $\vec m$ is a good measure of how effectively the 
sample screens the field, and can be obtained experimentally.  For our case, 
\begin{align}
\vec m &= \iiint \vec M dV\notag \\
&= 
\ell_x\ell_yt\left(1-\frac{2\lambda_L}{t}\tanh\left(t/2\lambda_L\right)\right)H_{ext}\hat x
\label{eqn:htsc-last2}
\end{align}
or, $|\vec m| = $Volume of sample $\times$ fraction of volume screened $\times 
H_{ext}$.

In the limit where $\lambda_L \ll t$, this becomes
\begin{equation}
|\vec m| \approx \ell_x\ell_yt\left(1-\frac{2\lambda_L}{t}\right)H_{ext}.
\label{eqn:htsc-last}
\end{equation}
In this limit, the field effectively penetrates the sample to a depth of 
$\lambda_L$ on each broad side.  Either Equation \ref{eqn:htsc-last2} or 
\ref{eqn:htsc-last} can be used to relate the magnetic moment to the London 
penetration depth.  

%%%%
%% ADD:  Ensuring explosion-resistance on warming.  Better link between M and 
%% voltage?  More lines of math in appendix.  Frequency roll-off in place of 
%% phase, which can perhaps be done with a two-resistor circuit and 4 points.  
%% Photo of xtal.  Demonstration set-up with magnets.  Cu?  
%%%%

\chapter{Lumped Constant Transmission Line and Coupled Resonators}
\renewcommand\chapname{LC Line}
\chapternumberreset
\section{Purpose}

\begin{enumerate}
\item To examine the properties of a lumped-element transmission line and 
to compare these properties to transmission line theory.  
\end{enumerate}

\section{Theory}
A continuous lossless transmission line (for example a coaxial cable or a 
twin-conductor line) can be described by its characteristic impedance $Z_o = 
\sqrt{\frac{L}{C}}$ (the ratio of the voltage and current anywhere along a 
semi-infinite line with no reflections) and its propagation velocity $v = 
\frac{1}{\sqrt{LC}}$ where $L$ and $C$ are the line's inductance and 
capacitance per unit length respectively.  If a finite length of 
transmission line is terminated at one end in an impedance $Z_t$, 
travelling electromagnetic waves are reflected by the discontinuity with 
(complex) reflection co-efficient
\begin{equation}
R = \frac{Z_t - Z_o}{Z_t + Z_o}
\end{equation}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=\textwidth,clip]{figs/lc-field.eps}
\caption{Solid lines show the absolute magnitude of the net electric field 
with a baseline of zero.}
\label{fig:lc-field}
\end{center}
\end{figure}

This leads to standing wave patterns being set up along the line (figure 
\ref{fig:lc-field}a and b), like acoustic standing waves in organ pipes or 
optical standing waves in a laser.  One consequence of these reflections 
is that the ``local impedance" measured at any point along the line will 
depend on where the measurement is made, and will not, in general, equal 
$Z_o$.  Only if $Z_t - Z_o = 0$ is $R=0$, and then there are no standing 
waves (figure \ref{fig:lc-field}c).  

\begin{figure}[htbp]
\begin{center}
\psfrag{Oscilloscope}{\sf Oscilloscope}
\psfrag{Function}{\sf\!Function}
\psfrag{Generator}{\sf Generator}
\psfrag{Trigger}{\sf\!\!Trigger}
\psfrag{DVM}{\sf DVM}
\psfrag{Computer}{\sf Computer}
\psfrag{AC}{\sf AC}
\psfrag{Voltmeter}{\sf Voltmeter}
\psfrag{GPIB}{\sf GPIB}
\psfrag{VA}{$V_A$}
\psfrag{VB}{$V_B$}
\psfrag{VC}{$V_C$}
\psfrag{Zin}{$Z_{in}$}
\psfrag{Zt}{\!$Z_t$}
\psfrag{50W}{\sf\!50$\sf\Omega$}
\includegraphics[width=0.9\textwidth]{figs/lc-apparatus.eps}
\caption{Experimental setup.}
\label{fig:lc-apparatus}
\end{center}
\end{figure}

A simple way to observe these effects is by measuring the voltage, $V_B$ in 
figure \ref{fig:lc-apparatus}, at a fixed position, while varying the frequency 
of the launched wave.  There should be no standing wave structure when $Z_t = 
Z_o$, so one can find $Z_o$ by finding the value of $Z_t$ which produces no 
standing wave structure at low frequencies (ie. $Z_t$ for which $V_B$ is 
most independent of frequency).  Similarly, if $Z_t$ is set to zero the 
standing wave pattern can be observed as minima and maxima in $V_B$ as the 
frequency is swept.  From figure \ref{fig:lc-field}d, it can be seen that the 
electrical length of the line is equal to $\frac{\lambda}{4}$ at the lowest 
frequency maximum and $\frac{\lambda}{2}$ at the lowest frequency minimum.

The above theory for a continuous line can be treated as the limiting case of 
one constructed from discrete elements, which can be described using the 
properties of its constituent coupled resonating circuits (ladder networks).  
A lumped constant line obviously differs from a continuous line both 
quantitatively and qualitatively.  For instance, there is a maximum frequency 
which can propagate along the line, associated with the resonant frequency of 
the individual resonating elements.  The theory can be found in 
\underline{Electricity and Magnetism}, Bleaney and Bleaney; it is of great 
interest because of its analogy with the theory of elastic solids, which for 
some purposes may be treated as continua, but for others must be treated as an 
array of individual atoms.

\section{Procedure}

You are provided with a lumped constant delay line made by tapping a solenoid 
at constant steps along its length and attaching capacitors to the tapping 
points.  To a first approximation this may be treated as a ladder network -- 
$\pi$ section or T section?

In the setup used (Figure \ref{fig:lc-apparatus}), the synthesizer and the DVM 
are controlled by the computer through the IEEE-488 bus.  A LabView-based 
program on the computer's desktop instructs the function generator to produce a 
given output amplitude and frequency and then records the voltage measured by 
the AC voltmeter.  The program makes measurements at constant frequency 
increments in a user-specified frequency range and plots a graph of the voltage 
recorded as a function of frequency.  The data are saved into a directory of 
your choosing.  Data may be transferred off this computer by either floppy disk 
or SFTP.  Note that the program will only attempt to save the data once -- if 
you save directly to a floppy and it fills up, you must re-acquire the affected 
data.

The input resistor $Z_{in}$ is present to limit the power in the resonance 
circuit.  The value you choose for it must not be too close to $Z_o$, and 
you're better off below $Z_o$ than above.  Pick a value around $\frac{1}{5} - 
\frac{1}{2} Z_o$.  

\begin{enumerate}

\item Find approximate values for $L$ (using the standard solenoid equation and 
the travelling microscope provided) and $C$ (using the nominal value or a 
capacitance meter).  How should the capacitors on the ends of the line be 
treated?

\item From 1, estimate the propagation velocity and characteristic impedance of 
your line.  What frequency should correspond to having a half wavelength 
excited along your line?

\item By recording frequency sweeps of $V_B$ for different termination 
impedances, find the actual $Z_o$ and $v$ for the line.  Calculate $L$ and $C$ 
and compare to the measured values of step 1.

\item The above measurements and interpretations are based on a 
first-order treatment, which is sufficient for satisfactory results.  If you 
wish to do a more accurate, detailed analysis, however, you must consider (a) 
the capacitance between adjacent turns of the solenoid and (b) the mutual 
inductance coupling the inductance of each section to the adjacent section.  
What does the delay line circuit look like now?  How does this detailed 
analysis affect your answer?

\item Using the frequency response technique and the capacitance meter, 
find $Z_o$ and $v$ for the length of 
speaker wire supplied.

\end{enumerate}

\section{References}
\begin{enumerate}
\item Chipman, \underline{Transmission Lines}

\item Bleaney and Bleaney, \underline{Electricity and Magnetism}

\item Feynman, \underline{Lectures on Physics}, Volume II, Section 22-6
\end{enumerate}

\chapter{Guided Microwaves}
\renewcommand\chapname{Micro}
\chapternumberreset

\section{Purpose}
\begin{enumerate}
\item  To become familiar with microwave propagation in rectangular 
waveguides.

\item To become familiar with microwave generation and measurement
techniques.
\end{enumerate}

\section{Theory}

As the speed of electronics increases to the gigahertz region and beyond,
conventional circuit analysis starts to break down due to the fact that
the signals propagate at a finite speed throughout a circuit.  This is
due to the intrinsic relationship (Maxwell equations) between voltage and
current signals on the one hand, and electromagnetic wave propagation on
the other hand.  One of the simplest and most direct ways of demonstrating 
propagation effects is by launching a gigahertz signal into a rectangular 
metal waveguide, and using a rectifying diode to monitor the voltage at 
different points along the guide.  (How would the voltage vary along the 
guide if a kilohertz signal generator was connected to the guide?)  

To analyze signal propagation along a given conducting channel (here we
use a hollow rectangular metal waveguide, but we could equally well
consider a coaxial cable or the more-common strip-line geometry), one has 
to specify
the geometry and the material parameters.  Usually the metallic
components can be considered ideal conductors, and the dielectric
components as ideal insulators with real dielectric constants.  With this
information, Maxwell's equations can be solved for the harmonic
electromagnetic modes supported by this system in the frequency range of
interest.  The solutions for the ideal rectangular metal waveguide are
particularly simple, especially if one only considers solutions with
electric field vectors perpendicular to the axis of the guide (so called
transverse electric modes, or TE modes).  With reference to
Fig.~\ref{fig:micro1} for the geometry, the solution for the electric
field takes the form
\begin{equation}\begin{split}
\vec{E}(x,y,z) &= 
\left[ 
E^R_x \sin\left(\frac{n\pi}{b}z\right) \cos\left(\frac{m\pi}{a}x\right) \hat{x}
+ E^R_z \sin\left(\frac{m\pi}{a}x\right) \cos\left(\frac{n\pi}{b}z\right) \hat{z} 
\right]e^{-j\left(\omega t -\frac{2\pi y}{\lambda_g}\right)} \\
&+  \left[ 
E^L_x \sin\left(\frac{n\pi}{b}z\right) \cos\left(\frac{m\pi}{a}x\right) \hat{x}
+ E^L_z \sin\left(\frac{m\pi}{a}x\right) \cos\left(\frac{n\pi}{b}z\right) \hat{z} 
\right]e^{-j\left(\omega t +\frac{2\pi y}{\lambda_g}\right)}
\end{split}\end{equation}
where	
\begin{equation}
\left(\frac{1}{\lambda_g}\right)^2 = 
\left(\frac{1}{\lambda_0}\right)^2 -
\left(\frac{n}{2b}\right)^2 - 
\left(\frac{m}{2a}\right)^2 
\label{eq:lg}
\end{equation}

\begin{figure}[htb]
\begin{center}
\psfrag{x}{$\hat x$}
\psfrag{y}{$\hat y$}
\psfrag{z}{$\hat z$}
\psfrag{a}{$a$}
\psfrag{b}{$b$}
\includegraphics[width=0.5\textwidth]{figs/micro-guide.eps}
\caption{A waveguide.}
\label{fig:micro1}
\end{center}
\end{figure} 

Here $\lambda_g$ is the wavelength in the waveguide,
$\lambda_0=c/\nu$ is
the wavelength in free space, and $R$ and $L$ refer to right and left
propagating waves respectively. The $x$ and $z$ components of the
field in each direction are not independent.  The spectrum of allowed
modes is discrete (due to the similarity of the free space wavelengths
and the guide geometry) and they can be labelled by positive integer
indices ($m$ and $n$) that basically specify how many nodes the
electric field amplitude exhibits along the two transverse axes ($x$
and $z$ in Fig.~\ref{fig:micro1}).  One, but not both, of $(m,n)$, can
be zero.

From Eq.~\ref{eq:lg} it is clear that the solution propagates along the 
guide, unattenuated, if the square of the guide wavelength is real, but is
exponentially attenuated, with no oscillatory behaviour along $y$ at all,
if the square of the guide wavelength is negative.  For each mode {$m,n$},
therefore, there is a corresponding maximum wavelength (minimum frequency), 
beyond which electromagnetic waves will not propagate along the guide.  
This wavelength is called the cutoff wavelength, and is given by
\begin{equation}
\left(\frac{1}{\lambda_c}\right)^2 = 
\left(\frac{n}{2b}\right)^2 +
\left(\frac{m}{2a}\right)^2 
\end{equation}

Thus the propagation characteristics at a given frequency are completely
determined by the dimensions of the guide, as contained in the expression 
for the cutoff wavelength.

The right and left propagating field amplitudes are determined by the
nature of the microwave source, how it is coupled into the guide, and by
what terminates the guide at the end opposite the signal launcher.  
In the present experiment, since $b < a$, the cutoff frequencies for all
modes other than the first mode (TE$_{10}$) are higher than the 
frequencies available from the source, hence we only have to be concerned
with the ratio $\zeta=E^L/E^R$. The overall magnitude is a function of 
the source and its coupling into the guide.  The ratio $\zeta$ 
is by definition the reflection coefficient of the right propagating wave
at the termination of the waveguide.  As in conventional electronics, the
reflection coefficient $\zeta$ is related to the impedance mismatch between
(in this case) the guide itself and the load, $Z_l$:
\begin{equation}
\zeta=\frac{Z_l-Z_g}{Z_l+Z_g}
\end{equation}

The guide impedance, $Z_g$ can be obtained by solving Maxwell's equations
for the magnetic field as well as the electric field, and the result for
the TE$_{10}$ mode is $Z_g=120\pi \lambda_g/\lambda_0$~($\Omega$).  The 
superposition of left and
right propagating waves gives rise to standing waves as illustrated in
Fig.~\ref{fig:micro2}.  You should be able to show that the reflection 
coefficient $\zeta$,
and hence the termination impedance, can be determined by measuring the
Voltage Standing Wave Ratio (VSWR)
\begin{equation}
VSWR=\frac{V_{max}}{V_{min}}
\end{equation}
and the phase of the voltage maxima and/or minima with respect to the end
of the guide.
 
\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.9\textwidth,clip]{figs/lc-field.eps}
\caption{Solid lines show the absolute magnitude of the net electric 
field with zero baseline. Here $Z_0$ means the guide impedance $Z_g$.}
\label{fig:micro2}
\end{center}
\end{figure}

The Smith chart is an instrument designed to simplify the conversion from
VSWR and phase measurements to the reflection coefficient or load 
impedance.  The theory of the Smith chart is in the Appendix and several
books can be found explaining the use of the Smith chart.  ``Microwave
Theory'' and ``Measurements and Handbook of Microwave Measurements'' are
both in the lab library.

\section{Procedure}

\begin{figure}[htb]
\begin{center}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{Gunn}{Gunn}
\psfrag{Oscillator}{Oscillator}
\psfrag{Isolator}{Isolator}
\psfrag{Wavemeter}{Wavemeter}
\psfrag{Attenuator}{Attenuator}
\psfrag{Slotted Guide}{Slotted Guide}
\psfrag{Diode}{Diode}
\psfrag{Detector}{Detector}
\psfrag{Digital}{Digital}
\psfrag{Voltmeter}{Voltmeter}
\psfrag{Reflecting Sheet}{Reflecting Sheet}
\includegraphics[width=0.85\textwidth]{figs/micro-setup.eps}
\caption{Schematic diagram of the apparatus.  Operate in CW mode.}
\label{fig:micro3}
\end{center}
\end{figure}

The Gunn oscillator produces microwave power at a given frequency when
the applied voltage and the cavity micrometer are adjusted to the values
specified on the manufacturer's specification chart.  Check you have the
correct one for your diode.  The slotted guide allows you to sample the
field in the waveguide with a small copper probe; the voltage induced in
the probe is rectified by a microwave diode in the probe assembly and the
resulting DC voltage is observed on the digital voltmeter.  The
rectified DC voltages observed at low power levels are proportional to
the square root of the power level in the guide, and are therefore
proportional to the voltage in the guide at the probe wire.

Since the probe is required to sample the field without also interfering 
with the propagating wave, you may have to adjust the depth of the probe 
into the waveguide so that you are not loading the line but can still pick 
up a good signal.  To determine whether you must adjust the depth, follow 
these steps:

\begin{enumerate}
\item Turn on the power and attach the silvered plate as the waveguide
termination.

\item Move the detector along the guide to obtain a maximal reading.

\item Adjust the tuning plunger on the detector mount to further optimize
the output.

\item  Move the detector along the guide recording the positions at which
successive maxima and minima are observed.

\item The maxima and minima should occur at approximately equal distances from
each other along the length of the line.  If they are not, then you will 
have to adjust the depth.
\end{enumerate}

The depth is adjusted by loosening the locking mechanism at the base of
the detector mount and raising the entire mount.  Once you have found a
good depth you can re-tighten the locking mechanism.  Adjust the tuning
plunger on the detector mount to get the largest voltage output.  You can
also adjust the wave-meter micrometer and attenuator to get a reasonable
signal from the probe.  The manual for the detector mount will be located
at the bench.  You must use the same settings for all parts of both
tasks.

\section{TASK 1 --- Determination of Cut-off Wavelength}

\begin{enumerate}
\item With the reflecting sheet fixed in place, and no termination attached
to the waveguide end, measure $\lambda_g$ by moving the probe carriage 
along the guide.

\item Next keep the probe fixed and move the aluminum reflecting sheet to 
determine $\lambda_0$.

\item Deduce a value for $\lambda_c$ and compare it with the value you obtain 
by measuring the dimensions of the waveguide cross-section with a pair of 
calipers.
\end{enumerate}

\section{Task 2 --- Impedance Measurement}

\begin{enumerate}
\item Determine the VSWR and the fractional wavelength from the end of the
waveguide to the nearest maximum with a short-circuiting termination,
using the silver-coated plate.  How do these values compare to their
expected values?

\item Remove the short circuit, and replace the reflecting sheet used in
Part 1 by a sheet of microwave absorbing material inclined at a slight
angle to the axis of the waveguide to prevent reflections back to the
waveguide system.  With nothing attached to the end of the slotted
guide, measure the VSWR and deduce the distance of the nearest voltage
maximum to the end of the slotted guide.  From these measurements, deduce
the normalized impedance at the end of the guide using the Smith Chart.

\item Repeat step 2 using a rectangular horn attached to the end of the
slotted guide.  Measure the distance from the nearest voltage maximum to
the input flange of the horn.

\item From the normalized impedances found in steps 2 and 3, deduce
  the termination impedance for each case by multiplying the
  normalized impedance by the impedance in the rectangular guide.

\item Which method of termination provides a better match to the guided
waves?  Why is this so?
\end{enumerate}

\section{Appendix --- The Smith Transmission Line Chart}

Many graphical aids for transmission line computation have been devised.  
Of these, the most generally useful has been one presented by P.H.\ Smith,
which consists of loci of constant resistance and reactance plotted on a
polar diagram in which radius corresponds to magnitude of reflection
coefficient, and angle corresponds to phase of reflection coefficient
referred to a general point along the line.  The chart enables one to
find simply how impedances are transformed along the line, or to relate
impedance to reflection coefficient or standing wave ratio and positions of 
voltage minima.  By combinations of operations, it enables one to 
understand the behaviour of complex impedance-matching techniques and to 
devise new ones.

The normalized impedance, $\frac{Z_i}{Z_0}$, that would be measured at a 
distance $l$ from the end of the guide is given by
\begin{equation}
\frac{Z_i}{Z_0}=(r+jx)=\frac{1+\rho(l)}{1-\rho(l)}
\end{equation}
where $\rho$ is the complex voltage reflection coefficient $\zeta$, 
multiplied by a phase factor corresponding to the accumulated phase from the 
observation point to the end of the guide and back again. 

Now if we let $\rho = u + jv$ then
\begin{equation}
r+jx = \frac{1+(u+jv)}{1-(u+jv)}
\end{equation}
which may be separated into real and imaginary parts as
\begin{align}
r &= \frac{1-(u^2+v^2)}{(1-u)^2+v^2}\\
x &= \frac{2v}{(1-u)^2+v^2}
\end{align}
or
\begin{align}
\left(u - \frac{r}{1+r}\right)^2 +v^2 &= \frac{1}{(1+r)^2}\label{eqn:micro-constr}\\
(u-1)^2 + \left(v - \frac{1}{x}\right)^2 &= \frac{1}{x^2}
\label{eqn:micro-constx}
\end{align}

If we then wish to plot the loci of constant resistance $r$ on the $\rho$ 
plane ($u$ and $v$ serving as rectangular coordinates), Eq.\ 
\ref{eqn:micro-constr} shows that they're circles centred on the $u$ axis at 
($r/[1 + r],0$) and with radii $1/(1 + r)$.  The circles for $r$ = 0, 1/2, 1, 
and 2 are sketched on Fig.\ \ref{fig:micro4}.  From Eq.\ 
\ref{eqn:micro-constx}, the curves of constant $x$ plotted on the $\rho$ plane 
are also circles, centred at ($1,1/x$) and with radii $1/|x|$.  Curves for 
$x$ = 0, $\pm$1/2, $\pm$1, and $\pm$2 are sketched on Fig.\ \ref{fig:micro4}.  
These circles appear as arcs as they are only drawn until they meet the $r = 
0$ boundary.  Any point on a given transmission line will have some impedance 
with a positive resistive part, and will then correspond to some particular 
point on the inside of the unit circle of the $\rho$ plane.

\begin{figure}[htbp]
\begin{center}
\includegraphics*[width=0.8\textwidth]{figs/micro-smith.eps}
\caption{The Smith chart, as stolen from Spread Spectrum Scene,}
\href{http://www.sss-mag.com/smith.html}{\tt http://www.sss-mag.com/smith.html} .
%% (illegal to put \href inside \caption)
\label{fig:micro4}
\end{center}
\end{figure}

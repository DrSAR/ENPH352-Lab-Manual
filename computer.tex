\chapter{Computer Control Experiment}
\renewcommand\chapname{LabView}
\chapternumberreset
\section{Purpose}

\begin{enumerate}
\item To become familiar with the design and implementation of computer 
controlled measurement systems.
\end{enumerate}

\section{Computer Control}

Many of the other experiments in this course rely on computers and 
LabView-based software for data acquisition.  In this experiment, the 
emphasis is on learning a bit about how such computer control systems are 
developed.

There are three basic steps in developing a computer-based measurement
system:

\begin{enumerate}
\item Plan the measurement strategy, including what input signals are
needed, what measurements must be taken, and what the correct sequence is,

\item Learn the ``language(s)'' used to program and control the various
input and output devices required (note these are device-specific
``command languages'', as opposed to general programming languages such as
FORTRAN, Java or C++), and

\item Connect the instruments to the computer and write a software routine
to control the input and output devices and acquire the required data.
\end{enumerate}

In this experiment you will be working with a function generator
(HP/Agilent 33120A), a digital multimeter (34401A), and a Tektronix 
digital oscilloscope (TDS 300) as input and output devices, as well as a 
temperature controller, power supply and computer heatsink (as shown in 
Figure \ref{fig:comp-heatsink}).  You will be working in LabView, which 
is quite different from 
conventional software languages, because it was developed specifically for 
data acquisition system development.  

Instruments are most commonly connected to the
computer through an IEEE-488 (or GPIB) interface, which is a 
standard and largely transparent system for communicating between a 
computer and IEEE-488 enabled devices.  The computer is equipped 
with an IEEE-488 interface card.  The computer and devices are connected 
together with IEEE-488 cables in a ``daisy-chain'' manner, where any 
device can be physically connected to any existing cable that is connected 
to the computer through the ``chain'' (look at the cables to see how this 
can be done).  The exception here is the temperature controller, which is 
connected to the computer via a serial cable.  

\section{Temperature Control}

\begin{figure}[htb]
\begin{center}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{Temp}{Temperature}
\psfrag{Controller}{Controller}
\psfrag{Computer}{Computer}
\psfrag{Resistor}{100 W Resistor}
\psfrag{Thermocouple}{Thermocouple}
\psfrag{Heatsink}{Heatsink}
\psfrag{Fan}{Fan}
\psfrag{12V}{12V DC}
\includegraphics[width=0.65\textwidth]{figs/comp-heatsink.eps}
\caption{\label{fig:comp-heatsink}Schematic diagram of the temperature 
control apparatus used in Task 2.}
\end{center}
\end{figure}

The apparatus provided for temperature control is centred around a large
chip resistor mounted on a heatsink.  This 10$\sf\Omega$ resistor is rated
to dissipate 100~W at 25$^\circ$C and derated linearly to 0~W at
175$^\circ$C.  Temperature is measured using a thermocouple attached to
the heatsink and is read and optionally controlled by the temperature
controller, which itself can be controlled by the computer.  The
temperature controller outputs a voltage to control a DC power supply,
which runs current through the resistor.  The computer is connected to the
temperature controller by a 9-pin serial interface, which allows LabView
to send it commands and read the temperature.

The heatsink is also attached to a 12~V DC fan, which can be used to cool 
it.  


%Around here, there will eventually be a description of some of:  PID
%controls, thermal mass, heat loss mechanisms, etc.  Likely also another 
%equation or two.  For now, here's the definition of heat capacity: 

The heat capacity of an object is defined as

\begin{equation}
C\equiv\frac{\partial Q}{\partial T}
\end{equation}
where $C$, the heat capacity, reflects the rise in temperature
$\partial T$ arising from the addition of $\partial Q$ of heat to the
system.  There are a number of different mechanisms through which an
object can lose heat to its environment: convection, conduction, and
radiation.  Which of these will dominate here?

\section{Procedure}

\subsection{Task 1 --- The Basics}

Here you will familiarize yourself with SCPI controls and LabView.

\begin{enumerate}
\item From the Desktop, start LabView.

\item Ensure the computer is connected to the function generator and 
multimeter.

\item Connect the function generator to the oscilloscope with appropriate 
triggering, and to the digital multimeter.

\item Open FUNCGEN.VI in {\tt Computer Lab Task 1.llb}, which is in a
folder on the desktop, and set the GPIB address to match the function
generator's (displayed momentarily on the front panel at power on).  Use
this VI to control the function generator.  {\slshape Note:  this requires
you to familiarize yourself with LabView, the command language of SCPI
(Standard Commands for Programmable Instruments)  for instrumentation, and
the basic use of the digital oscilloscope.  USE THE MANUALS!  THIS IS PART
OF THE POINT OF THIS EXPERIMENT. Try using SCPI to change the signal
amplitude, frequency, and form (sine, square etc.)}.

\item Open DVM.VI, in {\tt Computer Lab Task 1.llb}, set the GPIB address 
to reflect that set on the digital multimeter, and collect data.  The 
34401A multimeter has a unique function accessible 
through SCPI:  the ability to display a message in ASCII text on its 
display.  Write a message (preferably not offensive), and demonstrate it 
to your TA or instructor.  

\item Write your own LabView program to change sequentially the amplitude 
of the signal from the function generator in equal step sizes over some
range, measuring and displaying the corresponding outputs using the 
digital multimeter.  
You can write your own simple program using FUNCGEN.VI and DVM.VI (or sub
VIs within them) as sub VIs in your loop. If you want to save your VI
on the hard disk, please create your own subdirectory in 
\verb+C:\Lab Data\Phys352+ and save there. Please don't save your program 
or data elsewhere.

\item Demonstrate this routine to your TA or the instructor.
\end{enumerate}

\subsection{Task 2 --- Temperature Control}

Here you will use what you've learned to control the temperature of a 
heatsink.

\begin{enumerate}

\item Set up the apparatus as indicated in Figure \ref{fig:comp-heatsink}, 
and familiarize yourself with the operation of the temperature controller, 
particularly how to set the temperature.  The red readout is the measured 
temperature, while the green readout is the setpoint.  The temperature can 
also be set through LabView.  

\item Change the setpoint to 50$^\circ$C and use the computer to record 
the temperature rise as a function of time.  Estimate the response time.  

\item How does turning on the fan change the amount of power dissipated to 
keep the heatsink at 50$^\circ$?

\item Determine the heat capacity of the heatsink.

\item Disconnect the heater from power and record the temperature as the
heatsink cools (with the fan off).  Is this an exponential decay (if not,
why not)?

\item Monitor the temperature response as a function of time to a step
increase in current, with the controller's control function disabled.  
(One way to accomplish this is to set the controller to something 
unobtainably high, then use the current-limiting knob on the power 
supply).  Does the temperature approach a steady state exponentially?

\item Determine what temperature you can attain as a function of
current dissipated in the resistor.

%\item Using LabView editing facilities, work through the program and 
%figure
%out how it works.  Write out the flow-chart for the overall program,
%explaining why you think each step is needed.

%\item Compare your data with the theoretical performance based on simple 
%circuit analysis.  (Electronic response functions are usually plotted as 
%log-log for amplitude, and linear-log for phase).
\end{enumerate}

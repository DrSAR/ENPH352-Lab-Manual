\chapter{Gamma Ray Spectroscopy}
\renewcommand\chapname{Gamma}
\chapternumberreset
\hyphenation{pos-i-trons}
\section{Purpose}

\begin{enumerate}
\item To gain experience with single particle detection apparatus and 
spectroscopic analysis techniques.

\item To become acquainted with particle-matter interaction phenomena.

\end{enumerate}

\section{Theory}
The ability to measure the energy of particles is fundamental to 
experimental investigations in many branches of physics.  Applications range 
from high-energy studies of elementary particles (quarks, muons, pions, 
gamma rays etc. at the MeV to GeV level), to low energy studies of solids 
(luminescence, Raman scattering etc. at the 1 eV level).  Gamma emitters 
are used in analytical chemistry and as tracers in medicine.  One typically 
deals with a well-defined source that emits a flux of the relevant particles 
either spontaneously (radioactive decay) or as a result of external 
excitation (collisions or laser excitation etc.).  The energy of the 
particles and the absolute flux depends strongly on the type of source and 
on the excitation conditions.  The design of appropriate detectors thus 
depends strongly on the nature of the source.  However, it is generally 
true that the detection and energy-analysis of the particles is based on 
monitoring their interaction with a medium, or collection of matter (e.g. 
scintillator, photocathode, ionization chamber etc.) {\bf wherein the 
interaction of the particles with the medium is well-characterized}.

\begin{figure}[htb]
\begin{center}
\psfrag{Sample}{\sf Sample}
\psfrag{Personal}{\sf Personal}
\psfrag{Computer}{\sf Computer}
\psfrag{Analyzer}{\sf Analyzer}
\psfrag{Oscilloscope}{\sf Oscilloscope}
\psfrag{Amplifier}{\sf Amplifier}
\psfrag{Power}{\sf Power}
\psfrag{Supply}{\sf Supply}
\psfrag{Positive}{\sf Positive}
\psfrag{Anode Output}{\sf Anode Output}
\psfrag{NaI}{\sf NaI Crystal, Phototube and Base}
\includegraphics[width=0.7\textwidth]{figs/gamma-block.eps}
\caption{Schematic diagram of the apparatus.}
\label{fig:gamma-apparatus}
\end{center}  
\end{figure}

In the present experiment we deal with the detection of gamma radiation 
(photons) using a scintillator, a photomultiplier tube, and a pulse-height 
analysis system (see figure \ref{fig:gamma-apparatus}).  The scintillator acts 
as a transducer that converts the energy of the original gamma ray into many 
lower-energy photons which can be detected by the phototube.  The phototube is
really another transducer that converts these low energy photons into 
electrical current that can be easily analyzed using conventional electronic 
circuitry.  The following describes the basic processes involved in this 
detection system.  For a more detailed discussion see the references listed at 
the end of the lab.

\subsection{Primary Interactions}

The incident particles initially interact with the doped NaI crystal that 
makes up the scintillator.  There are three primary interaction routes, 
i)~photoemission, ii)~Compton scattering, and iii)~electron-positron pair 
production, the relative strengths of which depend on the energy of the 
incident particles.

\subsubsection{Photoelectric Effect}
This effect represents the process in which the photon is completely 
absorbed and all its energy is transferred to a single atomic electron.

\subsubsection{Compton Effect}

This effect represents the process in which the photon scatters off an 
atomic electron and transfers only a fraction of its energy to that electron.

\subsubsection{Electron-Positron Pair Production}

In the pair production process a photon of energy greater than twice the 
electron rest mass is annihilated and an electron-positron pair is created.
In order to conserve both energy and momentum this process can only occur 
in the coulomb field of a nucleus.

\begin{figure}[htb]
\begin{center}
\includegraphics[clip,width=0.8\textwidth]{figs/gamma-contrib.eps}
\caption{The energy dependence of the relative contributions of the three 
primary interactions of gamma rays with matter.  The absorption coefficient 
in lead is plotted against the photon energy in units of 0.511 MeV.}
\label{fig:gamma-contrib}
\end{center}  
\end{figure}

For a more detailed account of the three processes, see the Appendix (section 
\ref{sec:gamma-appendix}). Figure \ref{fig:gamma-contrib} shows the energy 
dependence of the relative contributions of the the three processes.

For processes i) and ii), the effect on the scintillator of the primary 
interaction is the liberation of a high-energy electron, leaving an empty 
core orbital in the associated host atom.  In case iii), one or both of the 
secondary gamma rays from the ensuing electron-positron annihilation process 
will act essentially as the primary gamma ray, again resulting in a 
high-energy electron and an associated core hole.  These high-energy 
electrons will then initiate a cascade of further ionization processes 
(electron-atom collisions) that produce more and more free electrons, each 
with less and less energy as the process continues.  At the same time, 
electrons from higher-energy orbitals will fall into the vacant core states, 
emitting X-rays.  Some of these X-rays may escape the scintillator, and 
others will be absorbed just as the gamma rays were, producing more energetic 
electrons.  Eventually the original energy of the gamma ray is converted into 
many low energy electrons and holes that drift through the NaI crystal until 
encountering one of the impurities, which act as recombination centres.  The 
visible photons given off in this ``scintillation" process at the 
impurities 
are then detected by the photomultiplier tube.  Higher energy gamma rays 
produce more low energy electrons and holes, which in turn produce more 
visible photons, each of which contributes to the strength of the amplified 
electron pulse from the photomultiplier tube; hence the use of the phototube's
pulse height as a measure of the incident gamma ray energy.

For gamma rays that initially decay via the photoelectric effect, virtually 
all of their initial energy will be converted to low energy electron-hole 
pairs, and a reasonable fraction of these, $\sim$1.5\%, will produce visible 
photons.  Including the collection and conversion efficiency of the phototube,
the overall efficiency for converting the gamma ray energy into electric 
current is on the order of 0.15\%.  Each such gamma ray will thus produce an 
electrical pulse of a characteristic height, with some distribution about 
that height due to statistical effects involved in the overall transduction 
process.  The corresponding electrical pulses are referred to as the 
photopeaks.  

A gamma ray that initially Compton scatters within the scintillator may or 
may not escape the crystal.  If it does not, it may eventually give up all 
its energy to the crystal, in which case it will also produce an electrical 
pulse of the same height as the photopeak.  If the scattered gamma ray does 
escape the crystal, the energy detected by the phototube will be less than 
that associated with the photopeak, and will represent the amount of energy 
given to the electron in the Compton scattering process.  The corresponding 
spectrum is continuous, and is characterized by a high-energy edge.  Why?

When a gamma ray undergoes annihilation into an electron-positron pair, some 
of its energy goes into the excitation of high-energy electrons, while the 
rest is converted to two gamma rays, each of energy 0.511 MeV.  If these two 
gamma rays are both absorbed in the detector, the result will be an 
electrical signal of height equal to the photopeak.  If one or both of these 
secondary gamma rays escapes, there will be corresponding replicas of the 
photopeak at energies of 0.511 MeV and 1.02 MeV below the photopeak.  These 
are called single and double escape peaks respectively.

Another possible process involves gamma rays that go right through the 
crystal, but then Compton scatter in the walls of the housing or in the 
phototube material itself.  If the gamma ray is {\bf backscattered}, it will 
re-enter the crystal, where it might be detected.  What sort of spectrum 
would you expect from such events?

Finally, some radioactive isotopes directly emit positrons when they beta 
decay.  These positrons, of energy 0.511 MeV, can annihilate with an electron 
either in the crystal or within the original source, giving rise to two gamma 
rays of energy 0.511 MeV.  What features would you expect these events to 
produce in the final spectra?

\section{Procedure}
BEFORE STARTING:  Ensure that you are familiar with the techniques for safe 
handling of radioactive materials.  Before you leave, make sure you {\bf\it 
wash your hands}, as you may be handling both lead and radioactive sources!  

\begin{enumerate}
\item The power supply voltage should be at 1300 V.  Adjust the
  amplifier gain such that the $^\text{60}$Co source gives a maximum
  signal that is not off-scale (if the
  spectrum goes off the right side of the screen you need to turn down
  the gain).  Check your signals with an oscilloscope
  and familiarize yourself with the Personal Computer Analyzer
  software.

\item Record spectra from the $^\text{137}$Cs, $^\text{109}$Cd, and
  $^\text{60}$Co sources.  Using the known energies for the gamma rays
  emitted from these sources (chart on the west wall of the laboratory
  or a reference), generate a calibration curve for the gamma ray
  energy in terms of the channel number.

\item With this calibration, determine the energies of peaks observed in the 
$^\text{54}$Mn, $^\text{22}$Na and $^\text{133}$Ba spectra, and compare with 
their expected values.  

%% The $^\text{54}$Mn samples are fairly weak at this stage ($t_\frac{1}{2}$ = 
%% 312.3 days, $t_\text{purchase}\sim$1994), so you will need to take data for 
%% a longer period of time.

\item For all of the spectra, identify as many of the features as possible.  
You will need to determine and use the relationship between the energies of 
the Compton edge, photopeak and backscatter peak.  Report the width of each 
photopeak.  

\item For the $^\text{137}$Cs source, determine the efficiency of this 
detection system.  You will need to record the date and initial activity of 
the source.  
\end{enumerate}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\textwidth,clip]{figs/gamma-example.eps}
\caption{Example:  Spectrum of a $^\text{137}$Cs source.}
\label{fig:gamma-example}
\end{center}  
\end{figure}

\section{References}
\href{http://nucleardata.nuclear.lu.se/toi/nucSearch.asp}{\tt http://nucleardata.nuclear.lu.se/toi/nucSearch.asp} \\
\href{http://atom.kaeri.re.kr}{\tt http://atom.kaeri.re.kr} \\
%\href{http://www.isotopetrace.com/periodic.htm}{\tt http://www.isotopetrace.com/periodic.htm}\\
UBC radiation safety and methodology
manual:\\ 
\href{http://riskmanagement.sites.olt.ubc.ca/files/2015/09/Radiation-Reference-Manual-2011.pdf}{\tt http://riskmanagement.sites.olt.ubc.ca/files/2015/09/Radiation-Reference-Manual-2011.pdf}


\section{Appendix}\label{sec:gamma-appendix}
\subsection{Photoelectric Effect}
The photoelectric effect represents the process in which the photon is 
completely absorbed and all its energy is transferred to a single atomic 
electron.  The cross-section for the photoelectric effect has been derived 
by Heitler and in the non-relativistic limit is
\begin{equation}
\sigma_p = \frac{\sigma_TZ^5}{137^5}2\sqrt{2}\left(\frac{h\nu}{mc^2}\right)^{-\frac{7}{2}} cm^2
\end{equation}
where
\begin{quotation}
\noindent$Z$ = atomic number of the struck nucleus \\
$h\nu$ = energy of the incident photon \\
$mc^2$ = electron rest mass \\
$\sigma_T$ = $\frac{8\pi}{3}\left(\frac{e^2}{mc^2}\right)^2$ = 
$\frac{8\pi}{3}r_o^2$ \\
$r_o$ = $\frac{e^2}{mc^2}$ = classical radius of the electron = 2.8 $\times$ 10$^{-13}$ cm
\end{quotation}

\subsection{Compton Effect}
This effect represents the process in which the photon scatters off an atomic 
electron and transfers only a fraction of its energy to that electron.  (The 
dynamics of this process are discussed in greater detail below.)  At low 
energies the Compton scattering cross-section is
\begin{equation}
\sigma_c = \sigma_T\left(1-2\gamma + \frac{26}{5}\gamma^2\right)\ \ \ \ \ 
\text{for }\gamma \ll 1
\end{equation}
and for high energies
\begin{equation}
\sigma_c = \frac{3}{8}\sigma_T\frac{1}{\gamma}\left(\ln 2\gamma - 
\frac{1}{2}\right)\ \ \ \ \ \text{for }\gamma \gg 1
\end{equation}
where $\gamma$ = $\frac{h\nu}{mc^2}$

\subsection{Electron-Positron Pair Production}
In the pair production process a photon of energy greater than the equivalent 
electron rest mass is annihilated and an electron-positron pair is created.  
In order to conserve both energy and momentum this process can only occur in 
the coulomb field of a nucleus.  The cross-section for pair production rises 
rapidly above threshold and reaches a limiting value for $\frac{h\nu}{mc^2} 
\approx$ 1000 of
\begin{equation}
\sigma_{pair} = 
\frac{Z^2}{137}r_o^2\left(\frac{28}{9}\ln\frac{183}{Z^\frac{1}{3}} - 
\frac{2}{27}\right) cm^2
\end{equation}

\subsection{Compton Scattering}

\begin{figure}[htb]
\begin{center}
\psfrag{e-}{\sf e$^-$}
\psfrag{Electron}{$\phi$}
\psfrag{Photon}{$\theta$}
\psfrag{k1}{$\frac{h\nu}{c}$}
\psfrag{k2}{$\frac{h\nu'}{c}$}
\psfrag{p}{$p$}
\includegraphics[width=0.8\textwidth]{figs/gamma-compton.eps}
%\vspace{2.5in}
\caption{Compton Scattering of a photon from a free electron.}
\label{fig:gamma-compton}
\end{center}  
\end{figure}

The Compton scattering process is shown schematically in Figure 
\ref{fig:gamma-compton}.  An incident photon of momentum $\frac{h\nu}{c}$ 
scatters from an electron at rest.  The photon scatters at an angle $\theta$ 
with momentum $\frac{h\nu'}{c}$ and the electron recoils at angle $\phi$ 
with momentum $p$.  Using relativistic kinematics the energy of the recoiling 
electron is
\begin{equation}
E^2_{el} = p^2c^2 + m^2c^4
\end{equation}

The three vectors $\frac{h\nu}{c}$, $\frac{h\nu'}{c}$ and $p$ 
must lie in a plane.  Energy conservation therefore requires that
\begin{equation}
h\nu + mc^2 = h\nu' + \sqrt{p^2c^2 + m^2c^4}
\label{eqn:gamma-energyconsv}
\end{equation}
From momentum conservation, we obtain
\begin{eqnarray}
%\begin{align}
h\nu&=&h\nu'\cos\theta + cp\cos\phi \label{eqn:gamma-top}\\
0&=&h\nu'\sin\theta + cp\sin\phi \label{eqn:gamma-bottom}
%\end{align}
\end{eqnarray}
Eliminating $\phi$, the (unobserved) electron recoil angle, we obtain from 
equations \ref{eqn:gamma-top} and \ref{eqn:gamma-bottom}
\begin{equation}
h^2\nu^2 - 2h^2\nu\nu'\cos\theta + h^2\nu'^2 = c^2p^2
\end{equation}
Squaring equation \ref{eqn:gamma-energyconsv} and subtracting, we get
\begin{equation}
\frac{\nu - \nu'}{\nu\nu'} = \frac{h}{mc^2}\left(1 - \cos\theta\right)
\end{equation}

Using $E = h\nu$ for photons, this can be written as
\begin{equation}
\frac{1}{E'} - \frac{1}{E} = \frac{1}{mc^2}\left(1 - \cos\theta\right)
\end{equation}

The factor $\frac{h}{mc}$ = 2.42$\times$10$^{-10}$ cm is called the electron
Compton wavelength.  

%% X-ray Flourescence (Pb K/L edges appear in spectra)
%% Clarify cross-sections?  

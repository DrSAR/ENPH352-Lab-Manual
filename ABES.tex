\chapter{Absorption Band-edge Thermometry of Semiconductors}
\renewcommand\chapname{ABES}
\chapternumberreset
\section{Purpose}

\begin{enumerate}
\item To explore several forms of thermometry.

\item To become familiar with some fundamental properties of 
semiconductors.

\item To understand and familiarize yourself with optical spectroscopy as 
a measuring tool.
\end{enumerate}

\section {Introduction}
In this experiment, you will be characterizing the absorption edge of 
semiconductor samples at different temperatures.  One application of this 
technique is the measurement of the process temperature of semiconductors 
under ultra-high vacuum where other methods fail [1,2].  Absorption 
bandedge thermometry was studied extensively at UBC, and has now been 
commercialized.  

\section{Theory}
\subsection{The Semiconductor Bandgap}

Crystalline solids may be categorized as conductors or insulators 
depending on the distribution of electrons [3,4,5].  In a simple picture, 
the outer electrons in a metal, instead of remaining attached to their 
home ion, are delocalized over the entire crystal.  Electrons in an 
insulator are more tightly bound.  A semiconductor is both a poor 
conductor and a poor insulator.

\begin{figure}[!htb]
\begin{center}
{\bfseries a)}\includegraphics*[height=150pt]{figs/ABES-Cu1.eps}~~
{\bfseries b)}\includegraphics*[height=150pt]{figs/ABES-GaAs.eps}
\caption{\label{fig:bandgap-bands}Band structure of different solids: 
{\bfseries a)} Copper.  {\bfseries b)} Gallium arsenide (GaAs) near the 
$\Gamma$ point (the origin in $k$-space) --- labelled are the conduction 
band, two valence bands and the split-off band.}
\end{center}
\end{figure}

A more rigorous treatment requires considering electrons not in real space 
but in momentum (or wavevector) space, referred to as $k$-space, as well 
as quantum and statistical physics.  Wavevector space is the Fourier 
transform of real space, and states there are indexed by their crystal 
momentum $\vec p = \hbar\vec k$ instead of $\vec r$.  In $k$-space, a 
solid is defined using band structure:  a band describes energy levels 
$\varepsilon(\vec k)$ where electrons are allowed, as shown by the curves 
in Fig.\ \ref{fig:bandgap-bands}.  These bands are combinations of 
orbitals on all atoms in the crystal, and can only accommodate as many 
electrons as could be accommodated in their component real-space orbitals.  

The energy of the highest occupied electronic state at zero temperature is 
called the Fermi level, $\varepsilon_F$.  If the Fermi energy falls in the 
middle of a band (i.e.\ the band is partially-filled) as in Fig.\ 
\ref{fig:bandgap-bands}a, the crystal is metallic.  When this occurs 
(e.g.\ band $\Sigma_1$ in the upper left), there is always a level 
slightly higher in energy for the last electron to excite into;  hence, in 
an external field the electron will move and current will flow.  

Fig.\ \ref{fig:bandgap-bands}b shows part of the band structure of gallium 
arsenide (GaAs), a semiconductor.  For this material, the Fermi level 
lies somewhere between the ``valence'' band (downward facing parabola) and 
the ``conduction'' band (upward facing parabola), and the highest-energy 
electron lies at the very top of the valence band.  The gap between the 
valence and conduction bands is the forbidden energy gap, normally called 
the bandgap and labelled $\varepsilon_g$.  There are no states at any 
momentum in this energy range.  Under a moderate electric field, there are 
no allowed energy levels to move to, hence the material is insulating.  
For a pure GaAs crystal [4,6], the external field would have to exceed the 
energy of the bandgap in order to excite electrons to the conduction band 
and allow current to flow.

For the remainder of the discussion, we will only consider the band 
structure shown in Fig.\ \ref{fig:bandgap-bands}b.  As mentioned above, 
electrons could be excited from the valence band to the conduction band 
with a sufficiently strong electric field, but this can also be done with 
light.  If a photon impinges on the crystal with an energy less than 
$\varepsilon_g$, then it cannot be absorbed and will pass unmolested, just 
as visible light goes through a window pane.  However, if the energy of 
the photon exceeds $\varepsilon_g$, it can be absorbed by the material, 
exciting an electron from the valence band into the conduction band, and 
the material will be opaque to light of that particular color (this is why 
regular window glass protects us from ultraviolet light).  Hence, an 
experiment using transmission of light can determine the bandgap of a 
material --- the material would be completely transparent below 
$\varepsilon_g$ and completely absorbing above $\varepsilon_g$, leading to 
a step function absorption spectrum.  

This can be summarized as follows:
\begin{equation}
\begin{split}
h\nu \geq \varepsilon_g;~~ &\alpha \geq \alpha_g \\ 
h\nu < \varepsilon_g;~~ &\alpha \rightarrow 0
\end{split}
\end{equation}
where $h$ is Planck's constant, $\nu$ is the frequency of incident light, 
$\alpha$ is the optical absorption coefficient in cm$^{-1}$ and $\alpha_g$ 
is the optical absorption coefficient at the band-gap energy.  For GaAs, 
$\alpha_g=8000$cm$^{-1}$ [7].

The preceding discussion is strictly rigorous for a perfect, infinite 
crystal at absolute zero.  In this experiment, however, we will be dealing 
with real crystals which are finite and have imperfections, and the 
experiments will be done near room temperature.  Both properties  
contribute to a broadening of the step function, leading to an absorption 
bandedge known as the Urbach edge [1,2]. 

The absorption in the Urbach region can be described by:
\begin{equation}
\alpha (h\nu) = \alpha_g e^\frac{h\nu - \varepsilon_G}{\varepsilon_o}
\end{equation}
where $\varepsilon_o$ is the characteristic energy of the Urbach edge, 
$\varepsilon_G$ is the extrapolated optical bandgap energy, and $\alpha_g$ 
is again the optical absorption coefficient at the bandgap energy.  Note 
that we denote the Urbach edge optical bandgap with a capital $G$ 
subscript to distinguish it from the other definition of the bandgap; in 
practice, we can consider the two quantities to be the same for the 
purposes of this laboratory.  This equation is {\bfseries only} valid for 
$h\nu<\varepsilon_G$; in practice, the region where $\alpha\sim 
30-100$cm$^{-1}$ can be used to extrapolate for $\varepsilon_G$.

A full quantum mechanical treatment of optical absorption in direct 
bandgap semiconductor material gives the following relationship between 
$\alpha$ and $h\nu$:
\begin{equation}
\alpha h\nu \propto \sqrt{h\nu - \varepsilon_g}
\end{equation}
Hence, a plot of $(\alpha h\nu)^2$ versus $h\nu$ should give an 
extrapolated value at $\alpha h\nu = 0$ that corresponds to the bandgap.  
This equation is {\bfseries only} valid for $h\nu>\varepsilon_g$.  In 
practice, this last method will work well when one can extract good values 
of $\alpha$ but will not work when $\alpha$ is obscured, for instance due 
to normalization or dark signal uncertainties.  In summary, there are 
three methods that can be used to obtain a bandgap from optical absorption 
measurements.

In quantum theories of the Urbach edge for crystalline semiconductors, 
both $\varepsilon_o$ and $\varepsilon_G$ are proportional to the phonon 
population --- phonons are quantized acoustic waves (vibrations of the 
crystal lattice).  Using the Einstein model for phonons, the width of the 
Urbach edge is
\begin{equation}
\varepsilon_o = S_ok_B\theta_E\left[\frac{1+X}{2} + 
\frac{1}{e^\frac{\theta_E}{T}-1}\right]
\end{equation}
where the dimensionless parameter $X$ is a measure of the structural 
disorder, $\theta_E$ is the Einstein temperature, $S_o$ is a dimensionless 
constant related to the electron-phonon coupling, and $k_B$ is the 
Boltzmann constant.  $X$ is expected to be zero for a perfect crystal.  
The temperature dependence of the band gap is given by
\begin{equation}
\varepsilon_G(T) = \varepsilon_G(0) - S_gk_B\theta_E\left[ +
\frac{1}{e^\frac{\theta_E}{T}-1}\right]
\end{equation}
where $S_g$ is a dimensionless coupling constant and $\varepsilon_G(0)$ is 
taken as the published band gap at liquid-He temperature.

\subsection{Optical Absorption Measurements}

\begin{figure}[!htb]
\begin{center}
\includegraphics*[width=0.65\textwidth]{figs/ABES-det.eps}
\caption{\label{fig:bandgap-response}Silicon detector typical response.}
\end{center}
\end{figure}

In this experiment, the absorption coefficient will be extracted from 
optical transmission of light through the sample.  The spectrum read by 
the computer is a convolution of the incident light intensity reaching the 
detector and the detector response.  Fig.\ \ref{fig:bandgap-response} 
shows a typical spectral response for a silicon detector.  The signal 
$I(\lambda)$ read by the detector at wavelength $\lambda$, for light 
incident on a sample of thickness $d$ with wavelength-dependent absorption 
coefficient $\alpha$ is given by
\begin{equation}
I(\lambda) = S(1-R)I_o(\lambda)e^{-\alpha d}
\label{eq:bandgap-absorb}
\end{equation}
where $S$ is a dimensionless scattering factor, $R$ is the reflectivity of 
the material and $I_o(\lambda)$ is the intensity of light incident on the 
sample.  For semiconductors near the fundamental absorption edge, we can 
approximate the refractive index, $n$, to be wavelength independent; $R$ 
is then
\begin{equation}
R = \left|\frac{n-1}{n+1}\right|^2
\end{equation}

The transmission of light through the sample is thus
\begin{equation}
T = \frac{I(\lambda)}{(1-R)SI_o(\lambda)} = e^{-\alpha d}
\label{eq:bandgap-T}
\end{equation}
and can be inverted to extract $\alpha$ when the thickness of the sample 
is known.

\subsection{Thermocouples}
The theory of thermocouples is well covered in the Omega Corporation's 
practical temperature measurements publications [8,9].  These publications 
are available electronically [8] on the experiment computer as well as in 
the physical catalog [9] supplied with the experiment.  The student is 
referred to section Z of the catalog for the theory of operation and the 
wiring diagrams.

\section{Apparatus}

The apparatus consists of a diffraction grating spectrometer with a
CCD detector (Ocean Optics USB2000) connected to a computer by USB; a
type K thermocouple and power resistor mounted on the copper sample
mount for controlling the sample's temperature
%a Dewar flask (thermos); 
a halogen lamp; a variac
to control the lamp intensity.

\begin{figure}[hbt]
\begin{center}
\psfrag{Bulb}{Halogen lamp}
\psfrag{GaAs}{GaAs}
\psfrag{Copper}{Copper block}
\psfrag{Fan}{Fan}
%\psfrag{Heat}{Heat gun}
\psfrag{Fibre}{Fibre optic}
\psfrag{Computer}{Computer}
%\psfrag{Spectrometer}{Spectrometer}
\psfrag{Thermocouple}{Thermocouple}
\psfrag{Variac}{Variac}
\includegraphics[width=\textwidth]{figs/ABES-app.eps}
\caption{\label{fig:bandgap-apparatus}Schematic diagram of the apparatus.}
\end{center}
\end{figure}

Fig.\ \ref{fig:bandgap-apparatus} is a schematic diagram of the
apparatus.  The copper sample holder contains a hole in which the
fibre optic's lens is mounted.  Samples can be mounted to cover this
hole. Samples are mounted in a copper mount and held in place with a
plastic cover and four plastic screws. You should {\bf not} need to
remove the sample from its mount. To remove the sample from the
spectrometer, simply unscrew the copper mount from the main housing.

% and should only be 
%handled with the plastic tweezers provided --- the GaAs samples are 
%EXTREMELY fragile.  
%For the temperature-dependent 
%measurements, ensure that the thermocouple makes contact with the sample.

\section{Procedure}

Note: it will be a good idea to consult some of the references given.

\subsection{Task I --- The Apparatus}

\begin{enumerate}
\item Turn on the equipment. There is a desktop icon for the Ocean Optics 
spectrometer (SpectraSuite).  You will want to look at the spectral response 
of the detector with and without incident light.  Please save your data in 
{\tt \verb+My_Documents\PHYS352\[Yourname]+}.  This can be set easily when 
saving your first spectrum.  

\item What spectrum is detected without incoming light (i.e.\ dark 
signal)?

\item Qualitatively explain the spectrum obtained with incoming light.  
With directly incident light, what is the effect of the integration time?  
What is the effect of averaging over several curves?

\item There are 4 icons in the software labelled {\tt S}, {\tt A}, {\tt 
T}, and {\tt I} --- explain what they stand for and how you can access the 
{\tt A}, {\tt T} and {\tt I} functions.  Hint: think of normalization 
and equations \ref{eq:bandgap-absorb} to \ref{eq:bandgap-T}.  Will 
normalization be required for your measurements?  Why or why not?

\item Introduce a few material samples in front of the empty sample block 
(plastic, glass, hand,...) and look at the spectral response  (you may 
need to adjust the integration time).  Plot and explain your observations.  

\item Explain the thermocouple circuit.
% and the use of Dewar flask.
What do you measure for room temperature?

\item What are the temperature precision and accuracy of this 
thermocouple?  Are they temperature dependent?  Hint: see part II of 
reference 8 or 9. 
\end{enumerate}

\subsection{Task II --- Bandgap of GaAs from Absorption Measurements}

\begin{enumerate}
\item Measure the optical transmission through the GaAs sample (handle
  the sample with care and plastic tweezers).  Extract its absorption
  coefficient. To measure the sample thickness, {\bf do not} remove
  the sample from its holder, but measure the thickness of a second
  piece of GaAs provided.

\item For the sample provided, which has one unpolished side, scattering 
reduces the detected signal.  How can you normalize its transmission to 
obtain reliable values of $\alpha$?  Hint: consider what transmission you 
should expect for all values of $\alpha$, which is wavelength dependent.  

\item What is the bandgap of GaAs at room temperature?  Is it what you 
expected?  Which definition of the bandgap appears to give the best 
results?  Why?

\item Repeat for different GaAs wafer temperatures.  Does the temperature 
dependence behave as expected?  Hint:  refer to references 1 and 2.  
\end{enumerate}

\section{References}
\begin{enumerate}
\item Shane R.\ Johnson, {\slshape Optical Bandgap Thermometry In 
Molecular Beam Epitaxy}, Ph.D.\ thesis, UBC, 1995.

\item M.\ Beaudoin, A.J.G.\ DeVries, S.R.\ Johnson, H.\ Laman, and T.\ 
Tiedje, Appl.\ Phys.\ Lett.\ {\bfseries 70}, 3540 (1997).

\item See for instance:  Charles Kittel, {\slshape Introduction to 
Solid State Physics} (Wiley, 2005).

\item K.\ Seeger, {\slshape Semiconductor Physics --- An Introduction, 
8$^\text{th}$ Ed.} (Springer-Verlag, 2002), particularly chapters 1 and 2 
and sections 11-1 and 11-2.

\item For an advanced treatment (beyond the scope of this course) see: 
Neil W.\ Ashcroft and N.\ David Mermin, {\slshape Solid State Physics} 
(Saunders College, 1976).

\item To make semiconductors useful, impurities are incorporated (doped) 
into the crystal, in order to make them n-type (conduction by electrons in 
the conduction band) or p-type (conduction by ``holes'' or absence of 
electrons in the valence band).  Consult references 1-2 for more 
information.

\item J.I.\ Pankove, Phys.\ Rev.\ {\bfseries 140}, A2059 (1965)

\item The PDF documents are named {\slshape Practical $T$ meas part I...} 
and {\slshape Practical $T$ meas part II...} and come from the Library 
Reference Edition CD.

\item {\slshape The Temperature Handbook}, 
\href{http://www.omega.com}{http://www.omega.com}
\end{enumerate}

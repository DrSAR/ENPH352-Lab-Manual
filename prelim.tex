\chapter*{Department of Physics and Astronomy Safety Policy and Procedures}
\markboth{Safety}{Safety}
\addcontentsline{toc}{chapter}{Safety Policy and Procedures}

\section*{General Guidelines}

\subsection*{Safety Statement}
\begin{itemize}

\item Physics research presents a variety of technical situations involving a 
wide spectrum of potential hazards.  Laboratory work, therefore, may involve 
tasks that are potentially hazardous.

\item Faculty members and shop supervisors are responsible for safety in their
laboratories/shops.  They may delegate other qualified personnel to implement 
safety measures.
\end{itemize}

\subsection*{General Policy}
\begin{itemize}

\item Do not perform hazardous experimental work in any research or teaching 
laboratory, day or night, when you are entirely alone.  Make sure that someone
is aware of your activity and is available to assist you in case of emergency.

\item Make yourself familiar with potentially dangerous situations outlined in
the safety check list below.  Note:  some work areas are unique hazardous 
situations are regulated by specific safety provisions which you should follow
in addition to these guidelines.

\item Members of the department are required to familiarize themselves with 
the Workplace Hazardous Material Information System (WHMIS) legislation and 
sign a statement to that effect.

\item Safety Section on Legislation is located in the Physics Main Office 
which consists of resource material concerning Safety aspects of work, 
materials and procedures (resource person Bridget Hamilton).  The Material 
Safety Data Sheets (MSDS) are located in the physics stores.

\item The Physics Department Health and Safety Committee closely follows the 
University Policy PeA-11; provides advice and assistance on Safety matters and
regularly carries out inspections of the Physics premises.  Please refer to 
the bulletin board for the current members of the Physics Health and Safety 
Committee.

\item All accidents must be reported to the Physics Administrative Assistant,
and an accident report form must be filled out immediately.

\item Students will not be issued keys until they have read the Department 
Safety Regulations as well as the WHMIS regulations, and have signed a 
statement that they will comply with these regulations.  This also applies to
new members of the Department.

\item M.Sc. and Ph.D. theses will be accepted only after the candidates have 
signed a statement that they have disposed of hazardous chemicals in 
accordance with the safety regulations and are leaving their workplace in a 
safe condition.

\item First Aid Kits are provided by the department and can be obtained from 
the Physics stores.  Emergency telephone numbers are posted on the notice 
board and a copy can be obtained from stores for posting in your own 
laboratory.
\end{itemize}

\section*{Safety Checklist}

Many accidents happen due to avoidable factors such as negligence, improper 
use of equipment and undue haste.

\subsection*{Evacuated or Pressurized Equipment} 
(See also ``Cryogenic Liquids" below).

\begin{itemize}
\item Rotary pumps emitting oil mist should not be vented into the building.

\item Pulley belts have to be properly guarded.

\item Equipment containing mercury, especially pumps, gauges, etc. not in use
should be safely stored.  Mercury gauges and diffusion pumps should be fitted
with receptacles to collect spills in the event of damage to equipment.

\item Evacuated or pressurized vessels must be suitably shielded to protect 
personnel from implosions or explosions.

\item Users of gas traps must acquaint themselves with the possible hazards 
(see Vacuum Hazards Departmental Safety Regulations -- Appendix [Physics Main 
Office]).
\end{itemize}

\subsection*{Cryogenic Liquids}

{\bfseries Cold fluids and solids can cause severe burns.}

\begin{itemize}
\item Those using liquid helium must be authorized users (see list posted in 
Room 114), or be supervised by an authorized user.

\item Cryostats must be protected by safety pressure-release valves.

\item Glass dewars should be screened as far as possible to reduce risk of 
mechanical damage, and be protected against the hazards of implosion or 
explosion.

\item Thermally insulated gloves and safety goggles or glasses must be worn 
when transferring cryogenic liquids.

\item Liquid hydrogen systems can only be operated with the consent of the 
Department Head. (See ``Liquid Hydrogen:  Hazards \& Precautions" -- 
Safety 
Regulations).  [Physics Main Office, Room 325]

\item Users of cryogenic liquids must familiarize themselves with the 
information given in the Appendix entitled ``Vacuum Hazards -- Safety 
Regulations [Physics Main Office, Room 325]

\item All students who need to obtain liquid nitrogen from the main liquid 
nitrogen storage tank outside Stores must obey the posted regulations and must
be instructed in the proper procedures.  The instructions will be arranged by 
the Technical Services Coordinator.
\end{itemize}

\subsection*{Gases}

\begin{itemize}
\item Gas bottles must be secured to solid supports.

\item Gas bottles must be kept in areas where fire and mechanical shock are at
minimum risk.

\item Gas cylinder regulators are serviced by Canadian Liquid Air.  Do not 
dismantle.  Do not use oil or solvents.

\item Hazardous gases must not be vented inside the building.

\item Hazardous gases must be disposed of after suitable pacification.

\item Permission to use explosive gases must be obtained from the Department 
Head, and the Department Health and Safety Committee must also be advised.
\end{itemize}

\subsection*{High Voltage Equipment}

\begin{itemize}
\item High voltage equipment must be mounted inside robust grounded metal 
enclosures.

\item Instructions on shut-down procedures must be prominently displayed near 
the high voltage equipment and equipment must be fitted with a working safety 
interlock and working shut-down system.

\item Make sure you are protected against dangers of accidental short circuits
by wearing ear guards, goggles, etc.

\item Filmchecks must be used to verify that there are no x-ray hazards.

\item Checks for ozone and oxides of nitrogen must be carried out on a regular
basis.
\end{itemize}

\subsection*{Sources of Optical and Emission Radiation}

{\bfseries Lasers can cause severe eye damage; arc light can burn your 
skin.}

\begin{itemize}
\item Arcs must be properly screened to prevent radiation burn.

\item Combustion products should not be vented into the laboratories.

\item Microwave diathermy units must be properly screened.

\item High pressure arcs must be shielded to prevent explosion, radiation and 
release of poisonous gases (e.g. Quartz Halogen bulbs).  

\item Laser beams should be isolated from laboratory personnel -- particularly 
important for high power infrared lasers and Q-spoiled giant pulse lasers.

\item Radiation goggles must be worn for eye protection against exposure to 
ultraviolet rays (UV).
\end{itemize}

\subsection*{Fire Hazards}

\begin{itemize}
\item All persons working in the laboratory must be familiar with the fire 
procedures.

\item All exists, hallways, stairways and passages must be kept clear at all 
times and not be used as storage space.

\item Devices left running unattended (especially overnight) must be 
fire-proof.

\item Flammable liquids must be clearly labelled, stored in approved 
containers and limited to 1 litre for daily use.  (See National Fire Code of 
Canada, 1963, on file in Physics Main Office, Room 325).

\item Laboratories must be equipped with fire extinguishers appropriate to the
hazardous materials used in the laboratory.  Occupants of the laboratory must 
be familiar with the location and operation of the fire fighting equipment. 
Laboratory supervisors must advise the Health and Safety Committee of the need
to upgrade or replace fire equipment.
\end{itemize}

\subsection*{Mechanical Hazards}

{\bfseries Moving machinery can be dangerous.}

\begin{itemize}
\item Unstable heavy objects must be firmly secured.

\item Moving machine parts must be enclosed by suitable guards.

\item Wheels and load bearing parts on trolleys, cranes, etc., must be checked
for sufficient capacity when transporting heavy loads (e.g. magnets).  The 
location and nature of such machinery must be reported to the Physics Health 
and Safety Committee.

\item Goggles, face masks, gloves and other protective equipment must be 
available to all laboratory personnel using hazardous machinery such as 
lathes, millers, drill pressures, etc.

\item Walkways in laboratories must be kept clear of obstruction at all times.

\item Equipment using fluids must be fitted with a conservation tray in the 
event of leakage.
\end{itemize}

\subsection*{Handling and Disposal Dangerous Substances}

\begin{itemize}
\item Persons ordering chemicals and solvents must obtain MSDS for hazardous 
substances.  Please refer to the WHMIS regulations.

\item Make sure suitable storage space is available before ordering chemicals.
Check with the appropriate codes for limits on quantities, compatibilities of 
substances and other requirements.

\item Know the antidote and disposal methods before you use a hazardous 
chemical.

\item Make sure antidotes for dangerous poisons and neutralizing agents for 
corrosive substances are kept close to where the dangerous materials are used.

\item Make sure hazardous materials are labelled with permanent labels (see 
WHMIS) identifying the substance.  Blank workplace labels are available from 
the Physics stores.  It is required that labels also bear the owner's name and
date of acquisition.

\item Waste substances must be diagnosed of in accordance with the University 
regulations.  Check with the Physics stores for the correct procedures.  
Arrangements can also be made with the Chemistry Department stores for 
disposal of these substances.

\item Make sure the location of abnormally dangerous materials, and the 
name(s) of user(s) have been reported to the Physics Health and Safety 
Committee.
\end{itemize}

\subsection*{Radioactive Materials}

\begin{itemize}
\item Radioactive materials must be operated in accordance with University 
Regulations, see Manual on Radiation Hazards Control.  A copy of this report 
is on file in the Physics Main Office, Room 325.
For the UBC Radiation Safety and Methodology Manual, see:\\
\href{http://riskmanagement.sites.olt.ubc.ca/files/2015/09/Radiation-Reference-Manual-2011.pdf}{\tt
http://riskmanagement.sites.olt.ubc.ca/files/2015/09/Radiation-Reference-Manual-2011.pdf}




\end{itemize}

\chapter*{Introduction to the Lab}
\markboth{Introduction}{Introduction}
\addcontentsline{toc}{chapter}{Introduction to the Lab}

\section*{What is Expected}
The ENPH352 lab is designed to let students investigate concepts they have
learned in their physics classes to date.  The labs are all quite different 
and students have a choice as to which ones they want to do.  Since this is a 
class for engineering students, some emphasis is placed upon learning various 
techniques.  We expect you to demonstrate, through your reports, that you 
understand all aspects of the lab.  Briefly explain the important concepts and
summarize any relevant mathematics in a ``Theory" section of your report.

In addition, we expect you to become confident in using computers.
Many of the experiments collect data using LabView.  Equipment and
computer manuals are often located right at the bench, but if not, can
be retrieved from the Lab Engineering Technician or Teaching Assistant.  Don't
hesitate to get them out during a lab, but please remember to return
them at the end of the day.  For most labs, we expect a thorough
computer-assisted analysis.  Whenever possible, don't just treat the
programs or apparatus like a black box.  Try to go through those
you're using to understand how they work, and write down what you
learn.  If you're confused, just ask.

You should all have accounts on the Physics server.  If you do not have one by 
the end of the second week, let the TA or professor know.  You can access your 
Physics account from any terminal on campus and also from PCs with SSH 
software (such as those in the ENPH352 lab).  You should also be able to use 
most webmail sites, to send your data to yourself.  Do not expect to find 
USB ports on our computers.  

Data should be graphed as it is taken, and final graphs should be 
done on a computer.  Some of our computers have programs such as GNUplot or 
Mathematica installed. 
If you would rather use another scientific plotting program, feel free to do 
so.  Beware of office software; excel-type chartjunk will be penalized.

%write a formal report on your choice of any of the labs you have 
%done.  This is basically a chance to take your best experiment and write a 
%report on it as if you were writing a manuscript for publication.  The key 
%idea here is -- DON'T WRITE A BOOK, but explain things well and show us you 
%REALLY KNOW WHAT YOU ARE TALKING ABOUT.  Some guidelines for writing formal 
%reports will be explained to you closer to the end of the semester.  The 
%formal report is worth 25\%, with the lab reports making up the rest of the 
%marks.  

\section*{Lab Notebooks}

You should always write your data, observations, and calculations directly 
into your notebooks, and never onto scrap pieces of paper.  If you are 
recording data and make a mistake, simply cross it out.  If you make a big 
mistake and have an entire page of useless calculations, just draw a line 
through it and write OMIT and explain why you believe it's flawed.  Don't tear 
out the page.  If the data turn out to be of value after all (as often happens), they are easily 
resurrected.  Remember, this is intended to be a working lab notebook.  It 
will hopefully be relatively neat and fairly easy to follow, but it need 
not be perfect.  We require you to record your data in pen, and white-out 
is forbidden.  An engineer's notebook is a legal document, and there can 
be no evidence of tampering. Use the unambiguous ISO date format (YYYY-MM-DD) for each entry.

You have a choice of keeping a paper notebook or an electronic notebook (a professional type with time stamps and history etc.).

If you choose paper you will need to have two softcover (yellow) lab notebooks so that you can 
hand in a completed lab while working on the next one.  

The format and contents of your notebook are to be like a working engineer's 
or scientist's notebook.  In it you will record all useful information about 
the work you have done --- a brief statement of your objectives, your method 
for attaining them, your results in raw and reduced form (usually tabular), 
graphs arising from your results, your analysis and conclusions.  Scientists 
and engineers aim at having their lab notebook communicate their work to their 
colleagues, supervisors, clients, and to themselves in the future when they 
must understand what they have done previously with as little reworking, 
re-analysis and confusion as possible.  A good lab book can be photocopied and 
submitted to a client as a record of work in progress or as a preliminary 
version of the final report.  

A working lab book evolves as your work progresses; you can move backward in 
the book to fill in details or augment explanations as you go, and forward to 
lay out a logical plan of work to be done, and how, and what mode of display 
of results you will use.  You are strongly advised to plan and explore the 
experiment before the time you will actually do it.  You can then come to the 
lab with your notebook already containing a plan of what you will do, and how. 
Your book could already contain tables laid out for data entry, algorithms 
specified for reducing the data, graph pages designed, and a checklist to 
ensure all experimental work has been done.  If you record things in your book 
which you later decide to be wrong or otherwise to be ignored, that's OK --- 
indicate this, and leave it there.  Data and analysis in the logbook need 
to have a date associated with them, particularly if parts of the report 
are out of sequence.  

The notebook is not a formal report.  It is not glossy, typed or unnecessarily 
lengthy.  For a client, or for completion of a research project, a well 
prepared final publication is necessary, but it is not necessary or done in 
this course.  The notebook may have pasted into it graphs and other output 
from instruments and computers, or photocopies of valuable information from 
other sources.  {\bfseries We do not accept in your notebook any material cut, 
scanned, printed, or photocopied from this manual}.  You may not 
refer to this lab manual at all, except in identifying the experiment.  Your 
description of purpose, methods, theory, apparatus etc.\ must be 
self-contained and original, not a transcription of the manual.  

To many of you the preceding discussion of notebook technique will have been 
an unnecessary repetition of what you already know from previous lab courses.
Others may not find this so, and may wish to ask us for guidance 
in this technique.  It is your responsibility to do so.

As with all undergraduate labs, you will do best if you arrive well
prepared.  Come in advance, look at the apparatus and study the manual(s).  
Being well prepared allows you to deal with the possibility of equipment
malfunctions.  If necessary, consult the available instruction manuals
for the equipment beyond what is provided in this lab manual.  The lab has a
collection of books relating to experimental physics. 
And of course there is also the internet, but pay attention to the provenance of the material. In all cases: caveat emptor.

\section*{Data Analysis and Presentation of Results}

An essential part of an engineering experiment is the presentation of the 
fully analyzed results in the form of a well prepared report for a client or 
prospective client (who is as likely to be within your own organization as 
external to it).  Your lab workbook, if well done, may be considered as the 
preliminary state of such a report.  Since time is money, engineers plan to 
record, analyze and display as immediately as possible the results and 
conclusions of the experiment.  This immediate analysis and display has a 
second enormous virtue; it shows at once when an experiment is going wrong.

You are free to use any appropriate software tool to help in your analysis.  
There are several PC-based data analysis programs on the PCs in the lab, and 
you are also encouraged to explore the (often more powerful) packages 
available on the UNIX system.  

When grading the submitted lab notebooks, we will be looking, amongst other 
things, for:

\begin{itemize}

\item Entries should appear in chronological order, i.e. the order in which things were done. Do not leave spaces to be filled in later. At the end of each lab period, get your notebook initialled by your instructor or TA.

\item Engineering level of presentation:  orderly, easy to follow, clear and 
definite.  Since this is a working notebook, it may well contain certain 
material you consider not to be taken into account.  Please indicate this.  

\item Epitome of problem:  Precise, clear statement of what is to be done in 
the experiment which follows.  The theory or theoretical statements to be 
tested or established.  No derivation of theory is required, only the relevant 
formula followed by a definition of the symbols.  Also an outline of the 
measurements and procedures critical to the results.  

\item How it was done:  Circuit diagrams, apparatus used, critical properties 
of the apparatus, relevant calibrations, settings and why they were chosen, 
and procedures used.

\item Results obtained:	 
\begin{itemize}
\item Unprocessed data. For large amounts of data, just give the
  location of the original data files and be prepared to produce
  them. Formulate a good scheme for naming files (e.g. including the
  date, your name/initials); the data file name should point to the
  page in your lab notebook that describes fully how the data was
  collected. Your lab notebook should point back to the data file
  name. Show summary graphs (properly labelled) of unprocessed data.
\item Reduced graphs etc.\ which reveal final results
\item Analysis of results --- extracting the physical results from the data 
and an estimate of the errors
\end{itemize}

\item In all labs (and homework 1) you will be required to produce publication quality graphs to display your data and communicate your results. It is difficult to generate such graphs in any spreadsheet-based application (such as Open Office, Excel, etc). Accordingly, you should use a scientific graphing program such as Gnuplot (free), Matlab, Igor, Origin, etc.

\item Conclusion:  including as an essential component: how did results bear 
on the second point.

\item Abstract: We would like you get practice in the writing of
  proper scientific abstracts.  The abstract should be the last thing
  you write in your book for each experiment.  It should be a short,
  \textbf{self-contained} summary of the main results of the
  experiment.  You can't possibly write a useful abstract until the
  experiments are finished, the data analyzed, and the conclusions
  drawn. There is plentiful advice on the web about how to do this; e.g. the AIP style manual.
\end{itemize}

Your lab book should be written at a level of completeness so that a
reader who is knowledgeable in the physics of the experiment can
understand what you are doing at every step.  Everything you write
should be written so that if you were to come back to this lab book
two years from now, you would be able to use the results to write a
formal report from it.  

If your lab notebook is not complete, do not expect to receive a passing grade for that experiment.
Consult the checklist on the course webpage before you start and before you hand your notebook in for marking.

\section*{Hints}

Do not labour over something for hours.  Try to figure it out yourself and if 
you can't, then ask for help.

You are expected to attend the lab during the allotted time for ENPH352,
since the same room and equipment are used for the 4$^\text{th}$ year
physics lab three afternoons a week.  You \textbf{must} attend on
weeks where you are to give a presentation and the week before where
will we discuss and grade a draft of your presentation.  However, if
you do end up having 3 midterms and 6 assignments due in one week, we
don't mind if you do your lab work at some other time the week before
or after. You are not guaranteed access to the equipment or assistance
outside your scheduled lab.  If you do come in outside of the
scheduled lab period, you {\slshape must} sign in and out at the front
on the sheet provided. We keep track of who attends. 

\section*{Challenging tasks}

These are marked with a *, and we expect these tasks to cause a lot of head scratching; they will be best attempted if and when you have completed all other tasks and have time to think.



\section*{Plagiarism}
While students are encouraged to talk with each other and help each
other where appropriate, copying from current or pre-existing lab
reports, marked or unmarked, is strictly prohibited.  This is
considered plagiarism, which UBC's Senate typically punishes with an
8-12 month suspension and a mark of zero in the course.  The UBC
Calendar discusses the evils of plagiarism at
\href{http://students.ubc.ca/calendar/index.cfm?tree=3,54,111,959}{http://students.ubc.ca/calendar/index.cfm?tree=3,54,111,959}
.

N.B.\ We make frequent updates to the manuals; mostly changes are clarifications to experimental procedures occasioned by discussions with a current student. Always use the online version, and never trust any old copy left on computer desktops by previous students.

\section*{Finding Help}

You can find the contact info of relevant TA's and professors written
on the blackboard near the entrance to the laboratory, and on the
course website at
\href{http://phas.ubc.ca/~enph352/}{http://phas.ubc.ca/\~{}enph352/} .



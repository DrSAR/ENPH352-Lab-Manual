\chapter{Thermal Noise}
\renewcommand\chapname{Noise}
\chapternumberreset
\section{Purpose}

\begin{enumerate}
\item To become familiar with the occurrence and characteristics of thermal 
noise.

\item To become familiar with calibration techniques.

\item To determine one of the fundamental constants in nature.
\end{enumerate}

\section{Theory}
Due to the random thermal motion of the charge carriers in a material, a 
fluctuating potential will occur across the terminals of any device which 
has electrical resistance.  This fluctuating potential, which is small but 
quite measurable, is called thermal noise or Johnson noise (after J.B.\ 
Johnson, who made the first studies of thermal noise in 1928).  The mean value
of the fluctuating potential is zero if averaged over a long period of time.  
However, the mean square value is finite and can be measured with a suitable 
detector.

Using general thermodynamic arguments and applying the principle of 
equipartition, Nyquist (1928) showed that for a pure resistance $R$ the 
mean square of the noise voltage $\overline{V_n^2}$ in a frequency increment 
$\Delta f$ about $f$ is given by 
\begin{eqnarray}\label{eqn:nyquist1}
\overline{V_n^2} = 4k_BTRp(f)\Delta f \\
p(f) = \frac{hf}{k_BT} \left\{\frac{1}{e^\frac{hf}{k_BT} - 
1}\right\}\label{eqn:noise-pf}
\end{eqnarray}
where \\
$\overline{V_n^2}$ = mean square noise voltage in (volts)$^2$ \\
$k_B$ = Boltzmann's constant \\
$T$ = absolute temperature \\
$R$ = resistance in ohms \\
$p(f)$ = Planck factor \\
$\Delta f$ = bandwidth over which the measurement of $\overline{V_n^2}$ is 
made.

At the frequencies and temperatures encountered in this experiment $p(f)$ is 
very close to unity --- why? --- and we're left with the most common form of 
Nyquist's theorem:
\begin{equation}\label{eqn:nyquist2}
\overline{V_n^2} = 4k_BTR\Delta f
\end{equation}
where the noise power per unit bandwidth is independent of frequency.  Noise 
with this type of constant power spectrum is often called ``white noise."

Since the noise voltage $\sqrt{\overline{V_n^2}}$ is very small ($4\times 10^{-7}$V
for $R$=1000$\mathsf\Omega$, $T$=300K and $\Delta f$=10kHz), it must be 
amplified before it can be measured.  For an amplifier whose power ($V^2$) 
gain, $G(f)$, varies with frequency, the mean square noise voltage at the 
output of the amplifier, with a resistor $R$ across the input, is
\begin{equation}
\label{eqn:noise3}
\overline{V_o^2} = 4k_BTR\int_0^{\infty} G(f)df
\end{equation}

An array of three amplifiers is used in this experiment --- a pre-amp, the 
main amplifier, and an RMS to DC converter/amplifier whose gain is controlled 
{\slshape indirectly} via the {\bf INPUT RANGE CONTROL}. It is convenient at 
this point to collect the three gains into a single overall gain, $G(f)$.  
Although the various amplifiers will be treated from here on as a single black 
box, it's useful to know what exactly is being done to the signal, 
particularly if something goes wrong --- since there are three separate 
amplifiers, there are three points where the signal can get clipped and three 
pieces of equipment that can overload.  

Substituting into equation (\ref{eqn:noise3}) and rearranging, we obtain an 
expression for $k_B$,
\begin{equation}
\label{eqn:noise}
k_B = \frac{V^2_{DC}}{4TR\gamma}
\end{equation}
where $V_{DC}$ is the output from the RMS/DC converter and $\gamma$ is the 
gain bandwidth product, defined as: 
\begin{equation}
\gamma = \int_0^\infty G(f) df
\end{equation}

Since $T$ can be easily measured, $k_B$ can be determined by finding the 
relation between $V^2_{DC}$ and $R$, and by measuring $\gamma$.  It should be 
emphasized that the above expression only applies if
\begin{enumerate}
\item the output of the system is linearly proportional to 
$\sqrt{\overline{V_n^2}}$ and
\item the reactive component of the resistor's impedance is negligible over 
the bandwidth of the amplifier.
\end{enumerate}

This latter condition requires that the upper frequency cut-off not be too 
high, allowing the gain to approach zero before this effect kicks in.  

The gain of the system at a given frequency is given by:
\begin{equation}
\label{eqn:G(f)}
G(f) = \left(\frac{V_{out}}{V_{in}}\right)^2
\end{equation}
where $V_{out}$ (=$V_{DC}$) is the voltage out of the RMS to DC converter 
given an RMS input voltage $V_{in}$.  $V_{out}$ can be easily measured, but 
since $V_{in}$ is very small, it cannot be directly measured.  It can, 
however, be calculated if one uses a calibrated attenuator.

\section{Procedure}
The electronic system used in this experiment consists of a pre-amplifier 
(small ``brass" box) with a gain of $\approx$200, connected to a main unit 
containing a power amplifier with adjustable gain, a filter used to set the 
pass band, followed by an RMS to DC converter, whose conversion gain can be 
set indirectly via the {\bf INPUT RANGE CONTROL} lever.

The unit should be turned on and given at least 5-10 minutes to warm up before
taking any data.  While the amplifier is warming up you should familiarize 
yourself with the settings and be sure you can answer the following questions 
before proceeding to the next step.  The accuracy of this experiment depends 
on your knowledge of the amplifier and converter system and on your 
understanding of what you are trying to measure.

Connect the signal generator through an attenuator to the amplifier input and 
view the {\bf AC MONITOR} on the oscilloscope.  Set the attenuator to reduce 
the source voltage to a level enough above the noise level to be 
distinguishable from it, yet low enough that you don't overload the amplifier 
--- typical settings are between 60 and 110~dB, depending on what you choose 
as an input voltage.  Adjust the attenuator and try varying the frequency and 
amplitude of the signal generator.  \underline{TRY NOT TO OVERLOAD THE 
AMPLIFIER}.

What happens to the voltage response at high and low frequencies?\\
Why do you think it does this?\\
How linear is this amplifier?\\
What does it do with signals that are too large?

Try changing the settings and observe the results. What happens to the output 
when you change the bandwidth, gain and range? 

When selecting the settings to be used for the actual experiment, you should 
recognize that the same settings are used for the whole lab.  Therefore it is 
crucial that you choose appropriate settings.  Spend some time testing the 
different settings keeping these requirements in mind:
\begin{itemize}
\item The signal must be amplified enough so that you can measure the noise 
voltages of the resistors
\item You must be able to measure and integrate the complete gain curve (zero 
to infinity) using the available apparatus
\item High frequencies should be avoided, since the load begins to have 
significant reactive components.
\end{itemize}

\subsection{Task 1: Measurement of Noise Voltage}
$\star\star\star${\bf Warning}:  Connecting the oscilloscope changes the data 
--- only connect the scope for troubleshooting, never when taking data.

\begin{enumerate}
\item Connect the various resistances (ranging up to 10~k$\mathsf\Omega$) 
across the pre-amplifier input, and measure the noise level with the DMM 
connected across the {\bf DC OUTPUT} of the amplifier.  

\item Plot your $V_{DC}$ vs.\ $R$ data to get a relationship you can use when 
calculating $k_B$.  Why did the voltmeter not read zero when $R$ was zero?  
\end{enumerate}

\begin{figure}[htb]
\begin{center}
\psfrag{Task1}{\Large\sf Task 1}
\psfrag{Task2}{\Large\sf Task 2}
\psfrag{Preamp}{\Large\sf Pre-amp}
\psfrag{Attenuator}{\Large\sf Attenuator}
\psfrag{Function}{\Large\sf Function}
\psfrag{Generator}{\Large\sf Generator}
\psfrag{Oscilloscope}{\Large\sf Oscilloscope}
\psfrag{DVM}{\Large\sf DVM}
\psfrag{input}{\large\sf input}
\psfrag{DCmonitor}{\large\sf DC monitor}
\psfrag{ACmonitor}{\large\sf AC monitor}
\psfrag{Amplifier}{\Large\sf Amplifier}
\includegraphics[width=0.8\textwidth]{figs/noisefig.eps}
\caption[]{A block diagram of the thermal noise apparatus.}
\label{fig:noiseblock}
\end{center}  
\end{figure}

\subsection{Task 2: Measurement of $G(f)$}
$\star\star\star${\bf Warning}: The DC output will likely change when the {\bf
AC MONITOR} or signal generator is connected to the oscilloscope.  If this 
happens, take data with the {\bf AC MONITOR} disconnected and, to avoid 
clipping, just watch that the current doesn't exceed 80~$\mu$A on the 
analog dial meter.

\begin{enumerate}
\item To avoid reflections, BNC lines carrying variable-frequency AC require 
50~{$\sf\Omega$} terminations, which are built into many (but not all) 
devices' BNC connections.  You will need to make sure your lines are properly 
terminated.  You can assume that the input impedance of the amplifier system 
is \underline{very} high.

\item Making sure not to overload the amplifier or clip the signal, determine 
a value for $G(f)$ over the entire range of frequencies.  Take enough data to 
resolve the shape of $G(f)$ so you can accurately determine $\gamma$.

\item Do some more-rigorous measurements of $G(f)$ at a few representative 
frequencies by fixing the frequency and attenuation and then plotting 
$V^2_{out}$ vs $V^2_{in}$.  Why is this method more rigorous?

\item Determine $\gamma$, and therefore a value for Boltzmann's constant, 
using the results from Task 1.

\item (Optional) As a good exercise you should also construct a Bode plot to 
see how closely the -3~dB drop from the peak gain relates to the frequency 
cutoff points.
\end{enumerate}

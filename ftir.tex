\chapter{Fourier Transform Infrared Spectroscopy of Air and CO$_\text{2}$}
\renewcommand\chapname{FTIR}
\hyphenation{fou-ri-er}
\hyphenation{in-fra-red}
\chapternumberreset
\section{Purpose}

\begin{enumerate}
\item To understand and familiarize yourself with infrared spectroscopy as 
a measurement tool.

\item To measure and understand the absorption spectra of several gases.
\end{enumerate}

\section{Introduction}
When radiation emitted from the earth's surface travels through the 
atmosphere, some of it is entirely absorbed by the gases present and some of 
it passes almost unaffected. The wavelength regions most useful for measuring 
surface emission are those away from atmospheric gases' absorption bands. 
These transparent ``atmospheric window'' regions are found in the infrared and 
microwave regions of the spectrum. For instance, the spectral regions 3.5 -- 4 
and 8 -- 12~$\mu$m are ``windows'' in which very little thermal radiation 
is absorbed by the atmosphere.  Carbon dioxide (CO$_\text{2}$) is a minor 
constituent of the atmosphere, but is the main gas responsible for the earth's 
greenhouse effect because it has an absorption band within these wavelengths, 
closing this window. Apart from keeping the planet warm, it is used in many 
applications, notably CO$_\text{2}$ lasers.  Water vapor also has absorption 
bands within these wavelengths.

Fourier transform infrared (FTIR) spectroscopy is a common laboratory tool 
used for applications ranging from identification of species in chemical 
mixtures to quantized levels in semiconductor quantum well structures.  It 
is a versatile tool that allows the resolution of extremely fine spectral 
lines in a reasonable time.  Although the technique has been known for many 
years, practical implementations had to await the advent of both the Fast 
Fourier Transform (FFT) algorithm and computers.  Today, FTIRs are ubiquitous 
tools in both research and applications laboratories.

\section{Theory}
\subsection{Absorption Lines in Air and CO$_\text{2}$}
\begin{figure}[htb]
\begin{center}
\includegraphics*[width=0.6\textwidth]{figs/ftir-spectra.eps}
\caption{IR absorption spectra of common atmospheric gases, from {\slshape 
Handbook of Geophysics and Space Enviroments} by S.L.\ Valley, Ed. (1965), as 
reproduced in reference 1.} \label{fig:ftir-spectra}
\end{center}
\end{figure}

Figure \ref{fig:ftir-spectra}, reproduced from reference 1 shows the absorption 
spectra of common atmospheric molecules and air's aggregate absorption due to 
the sum of all the constituent gases.  Some of these absorption lines are 
sharp, while others are broad.  In the mid-infrared (MIR) region of the 
electromagnetic spectrum, molecules absorb (or emit) light through the 
interaction between the photons and the vibrating electric dipoles of the 
molecules.  Hence, molecules such as CO, which have a polar bond (C has 4 
valence electrons while O has 6), and thus a strong oscillating dipole moment 
when they vibrate, absorb strongly at specific energies.  Non-polar molecules, 
such as O$_\text{2}$ and N$_\text{2}$, which together with monatomic gases 
constitute 99\% of the atmosphere, do not have a resulting dipole moment and 
are therefore transparent in the MIR.  

\subsection{Vibrational Modes}
Electromagnetic waves in the infrared (IR) region with wavelengths
$\lambda$ between 780~nm and {2000~$\mu$m}, (wavenumbers $\nu$ between
12800~cm$^\text{-1}$ and 5~cm$^\text{-1}$) can stimulate molecular
vibration (and rotation) modes.  Spectroscopists prefer using
wavenumber because it scales linearly with energy.  Often the symbol
$k$ is used for wavenumber, but since the instrument manual uses
$\nu$, we adopt $nu$ for wavenumber as well.  The light can
only be absorbed if there is a net change of dipole moment due to the
vibrational motion of the molecule, i.e.\ a transition dipole moment.
If the molecule's atoms are modeled as balls and the bonds between
them as perfect springs, any vibration can be described classically by
Hooke's law for a spring. The frequency $f$ for the resulting
vibration is given in equation \ref{eqn:ftir-hooke},
\begin{equation}
f=\frac{1}{2\pi}\sqrt{\frac{K}{\mu}}
\label{eqn:ftir-hooke}
\end{equation}
where $K$ is the spring constant and $\mu = \frac{m_1m_2}{m_1+m_2}$ is the 
reduced mass of the system.  

The potential and kinetic energies of this harmonic oscillator may be obtained 
from quantum mechanics, where the eigenvalues (energy levels) are quantized as 
given in equation \ref{eqn:ftir-nrg}.  The selection rules for IR absorption 
state that $\Delta n = \pm 1$.  

\begin{equation}
E_n = hf\left(n+\frac{1}{2}\right)
\label{eqn:ftir-nrg}
\end{equation}
where $h$ is Planck's constant, $f$ is the frequency from above and 
$n$=0,1,2,\dots\ is an integer called the vibration quantum number.  At 
room temperature almost all molecules are in the vibrational ground state 
($n$=0). The observed wavenumbers $\Delta\nu$ for the first harmonics can be 
calculated from the absorbed energy $\Delta E = E_1-E_0 = hc\Delta\nu$, which 
combines with 
equation \ref{eqn:ftir-hooke} to give
\begin{equation}
\Delta\nu = \frac{1}{2\pi c}\sqrt{\frac{K}{\mu}}
\label{eqn:ftir-deltanu}
\end{equation}
Therefore the observed wavenumbers (or frequencies) depend on the strength 
of the bonds between the atoms and on their masses.

\begin{figure}[htb]
\begin{center}
\psfrag{a}{\bfseries a)}
\psfrag{b}{\bfseries b)}
\psfrag{c}{\bfseries c)}
\psfrag{d}{\bfseries d)}
\psfrag{e}{\bfseries e)}
\psfrag{f}{\bfseries f)}
\psfrag{g}{\bfseries g)}
\includegraphics[width=0.6\textwidth]{figs/ftir-modes.eps}
\caption{Vibrational modes of diatomic and triatomic species.  {\bfseries 
a)} Diatomic stretching mode $\nu_1$.  {\bfseries b)} and {\bfseries e)} 
Triatomic symmetric stretching modes $\nu_1$.  {\bfseries c)} and {\bfseries 
f)} Triatomic bending modes $\nu_2$.  {\bfseries d)} and {\bfseries g)} 
Triatomic antisymmetric stretching modes $\nu_3$.}

\label{fig:ftir-modes}
\end{center}
\end{figure}

Diatomic molecules, such as CO, can only have one vibrational mode, consisting 
of a stretching (or compressing) along the axis of the molecule.  This is 
illustrated in figure \ref{fig:ftir-modes}.  Triatomic molecules, on the other 
hand, have additional degrees of freedom.  This will create symmetric and 
anti-symmetric stretching modes in addition to bending modes.  These 
vibrational modes are denoted by $\nu_1$, $\nu_3$ and $\nu_2$ respectively, 
and are also shown in figure \ref{fig:ftir-modes}.  CO$_\text{2}$, a linear 
molecule, lacks a permanent dipole moment (it has a centre of inversion).  
Hence, it will have no absorption in the symmetric vibration mode.  In the 
anti-symmetric and bending modes however, an oscillating dipole moment will be 
created and IR radiation can be absorbed.  A more thorough discussion of these 
phenomena can be found in references 1 and 2.  Table \ref{table:ftir-modes} 
shows the vibrational modes of some common atmospheric radiative molecules 
(adapted from reference 1).

\begin{table}
\begin{center}
\caption{\label{table:ftir-modes}IR-active vibrational modes of various 
atmospheric gases in wavenumbers (cm$^\text{-1}$).}
\begin{tabular}{|l|c|c|c|c|}\hline
Species & $\nu_1$ & $\nu_2$ & $\nu_3$ & $\nu_4$ \\ \hline
CO & 2143 &  &  &  \\ \hline
CO$_\text{2}$ &  & 667 & 2349 &  \\ \hline
N$_\text{2}$O & 1285 & 589 & 2224 &  \\ \hline
H$_\text{2}$O & 3657 & 1595 & 3776 &  \\ \hline
O$_\text{3}$ & 1110 & 705 & 1043 &  \\ \hline
NO & 1904 &  &  &  \\ \hline
NO$_\text{2}$ & 1306 & 755 & 1621 &  \\ \hline
CH$_\text{4}$ & 2917 & 1534 & 3019 & 1904 \\ \hline
\end{tabular}
\end{center}
\end{table}

\subsection{FTIR Spectrometers}
Fourier transform infrared spectrometers have caught on against dispersive 
spectrometers since the 1980s. FTIR spectrometers are faster, more exact, 
and have a higher signal to noise ratio. The first advantage is often 
called Felgett's advantage and comes from the fact that all wavelengths 
are measured simultaneously. Secondly, an FTIR spectrometer has a laser to 
measure the position of the moveable mirror, which allows simultaneous 
calibration of wavelengths.  Finally, it does not require slits or 
monochromators, allowing for measurements at higher intensity.  These 
advantages can be utilized, since the Fourier transformation of the resulting 
interferograms to spectra can be calculated nowadays within seconds. 

\begin{figure}[htb]
\begin{center}
\includegraphics*[width=\textwidth]{figs/ftir-burst.eps}
\caption{A sample interferogram.}
\label{fig:ftir-burst}
\end{center}
\end{figure}

The polychromatic IR source beam is split and sent through a Michelson 
interferometer before it probes the sample.  A moveable mirror reflects the 
beam in such a way that it superimposes and interferes with the incoming one. 
As the system's path length is varied, each wavelength alternates between 
constructive and destructive interference, so the signal at the detector, the 
sum of all wavelengths present, is a complicated interferogram containing 
information about the intensity at each frequency (see figure 
\ref{fig:ftir-burst}).  The absorption of the sample alters this 
interferogram. From the interferograms, usually single channel intensity 
spectra are calculated using the Fast Fourier algorithm (FFT). The principle 
of operation is depicted in Figure \ref{fig:ftir-path}, with the actual 
device shown in figure \ref{fig:ftir-open}.

\begin{figure}[htb]
\begin{center}
\includegraphics*[width=0.6\textwidth]{figs/oriel-path.eps}%
\caption{Schematic of the Michelson interferometer at the heart of the 
FTIR technique. Refer to reference 3 for a more detailed explanation.}
\label{fig:ftir-path}
\end{center}
\end{figure}

\begin{figure}[htb]
\begin{center}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{Beam}{\textcolor{white}{Beam}}
\psfrag{Splitter}{\textcolor{white}{Splitter}}
\psfrag{Laser}{Laser}
\psfrag{Mirror}{Mirror}
\psfrag{Corner Cube Mirrors}{Corner Cube Mirrors}
\psfrag{Source}{Source}
\psfrag{Retro Reflector}{Retro Reflector}
\psfrag{Detector}{Detector}
\psfrag{Moving}{Moving}
\psfrag{Stage}{Stage}
\psfrag{Analogue to}{Analogue to}
\psfrag{Digital }{Digital}
\psfrag{Convertor}{Convertor}
\psfrag{Boards}{Boards}
\includegraphics*[width=\textwidth]{figs/ftir-open1.eps}
\caption{The interferometer's constituent parts.  The gas cell (brass) must be 
inserted between the source and the interferometer unit for some parts of the 
lab.}
\label{fig:ftir-open}
\end{center}
\end{figure}

Using equations \ref{eqn:ftir-T} and \ref{eqn:ftir-A}: 
\begin{align}
\frac{I}{I_o} &\equiv T = 10^{-\varepsilon cd}\label{eqn:ftir-T} \\
A &= -\log T = \varepsilon cd,
\label{eqn:ftir-A}
\end{align}
intensity spectra can be converted into transmission ($T$) or absorbance ($A$) 
spectra, with $I_o$ representing the intensity of the background or reference 
spectra and $I$ the intensity of the sample spectrum.  Chemists usually prefer 
absorbance spectra because sample concentrations ($c$) can be estimated by 
the Beer-Lambert law \ref{eqn:ftir-A} for a known molar absorption 
coefficient $\varepsilon$ and sample thickness $d$.  During the last decades 
FTIR spectroscopy has been increasingly used for investigations of 
biomolecules, like proteins and DNA.  High signal-to-noise ratio and fast 
accumulation of scans made it possible to get highly-resolved difference 
spectra. Applications of FTIR spectroscopy include investigating kinetic 
pathways or interactions between proteins and their environment, like 
embedded membrane proteins and surrounding lipids. 

More information on the FTIR technique can be obtained from the MIR 8000 
spectrometer manual (Reference 3) as well as references 4 and 5.

\subsection{Lineshape}
Although the simplified theory presented here states that absorption lines 
are found at specific energies, each spectral line has a finite width.  
This width comes in part from the natural linewidth of the optical 
transition but is dominated by Doppler broadening, also called temperature 
broadening, and collision broadening which is also sometimes called 
pressure broadening.  The natural linewidth arises in part from the 
Heisenberg uncertainty principle and is so small that it can ignored in 
our case.  Doppler broadening is caused by the rapid translational motions 
of gas molecules.  These motions are completely random and therefore have a 
Gaussian (Normal) distribution about an average value, $\nu_o$, described by
\begin{equation}
P(\nu) = \frac{1}{\sqrt{2\pi}\sigma}e^{-\frac{(\nu-\nu_o)^2}{2\sigma^2}}
\label{eqn:ftir-gaussian}
\end{equation}
where $\sigma$, the standard deviation, describes the width of the line.  What 
is relationship between $\sigma$ and the full width at half maximum (FWHM) 
of the line?

Collision broadening is caused by disturbances to the absorption process 
during molecular collisions.  Collision broadening varies directly with 
pressure and gives rise to a Lorentzian (Cauchy) lineshape:
\begin{equation}
P(\nu) = \frac{1}{2\pi}\frac{\Gamma}{(\nu-\nu_o)^2 + 
\left(\frac{\Gamma}{2}\right)^2}
\label{eqn:ftir-lorentzian}
\end{equation}
where $\Gamma$ is the FWHM of the line.  The Gaussian shape is more compact 
while the Lorentzian lineshape is narrow but has ``wings" that extend far 
beyond the centre energy.

\section{The Apparatus}
The apparatus consists of an FTIR spectrometer with a detector, an MIR source 
and a gas cell.  The gas cell consists of a chamber with silicon windows at 
either ends.  Ensure that you {\bfseries turn on the FTIR and the MIR source 
as soon as you arrive} --- they take at least 30 minutes to stabilize.  The 
LEDs on top of the unit should be on and may be fluctuating at first but will 
eventually stabilize.  You should also hear the mirror moving inside the 
unit.  

The FTIR is entirely controlled by the computer.  The FTIR program has a 
tendency to crash often; this is annoying but should not prevent you from 
completing the experiment.  Most of the program functions are accessed by 
SINGLE click instead of the usual Windows double-click.  It is relatively 
straightforward and self-explanatory, and you should be comfortable with the 
program after a short while.  It will allow you to collect data and perform 
most of the math needed to analyze the spectra. Most of the features are 
explained in detail in reference 3 which should always remain near the 
experiment.  The software is based on MatLab, which can treat all the output 
files directly but also allows you to export plain-text files in order to 
graph or manipulate your data using other software.  You will need to use a 
scientific software program, such as Mathematica or Origin, for your data 
anaylsis.  

\section{Procedure}
\begin{figure}[htb]
\begin{center}
%\includegraphics*[width=0.3\textwidth]{figs/ftir-cell.eps}%
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{Source}{Source}
\psfrag{Detector}{Detector}
\psfrag{Computer}{Computer}
\psfrag{Moving Stage}{Moving Stage}
\psfrag{Gas cell}{Gas cell}
\psfrag{ADC, Amplifier}{ADC, Amplifier}
\psfrag{Boards}{Boards}
\psfrag{and Filter}{and Filter}
\includegraphics*[width=0.7\textwidth]{figs/ftir-block.eps}
\caption{Block diagram of the FTIR Spectrometer apparatus.}
\label{fig:ftir-block}
\end{center}
\end{figure}

\begin{enumerate}

\item A note on the computer DAQ system

This FTIR is a valuable piece of apparatus, costing around \$35K. Good hardware does not age if looked after, but in this case the hardware can only be run by a computer system that existed when the hardware was built, and computers do age, very rapidly. As a workaround the old DAQ computer is linked to a more modern machine to facilitate data transfer.

\item The interferogram and transmission throughput of the system:

Turn on the FTIR spectrometer and power supply. The FTIR software is accessible via the ``FTIR" icon on the desktop.  Ensure 
that the IR source is installed on the FTIR main unit as shown in figure \ref{fig:ftir-block}, and that one of the 
connection flange setscrews is snug ({\bfseries NOT TIGHT}). If the gas cell is in place, take it out (this is a simple press-fit connection; do not remove any screws or bolts). Open {\tt Mirmat} to initialize the spectrometer --- the detector type is DTGS and you will want a resolution of 4~cm$^\text{-1}$ for this part.  
Click {\tt Accept} and then choose {\tt Fresh Start} from the next dialog 
box.  Instructions on how to use the software may be found in the 
instrument manual.  

The interferometer should begin scanning and accumulating data.  Click on 
{\tt Auto Scale} to view the entire signal.  The unit is currently slightly 
misadjusted such that the interferogram is not completely visible in the data 
acquisition window.  Change the axes' limits to display the full 
interferogram (put {\tt Minx} = -3500 in the top (time domain) part of the {\tt axes limits} 
dialog box).  The highest intensity part of the interferogram is called the 
centre burst.  At any time, you may click on {\tt SPV} to send either the 
interferogram or spectrum to the graphical treatment dialog box.  Explain the 
general shape of the interferogram.  Why is the interferogram shifted 
(hint:  it should not be)?  What would happen to the interferogram at 
higher (or lower) resolution?  

In the interferogram, you were collecting the throughput signal of the 
source through the FTIR.  Can you explain the general shape of the 
spectrum (hint: what is the IR source)?  Why does this spectrum exhibit 
lower-intensity troughs?  What is the useful spectral range of this FTIR 
system with the current source and optics?  Although you will acquire data at 
all wavenumbers in the remainder of this lab, you need only perform the 
analyses on the system's useful spectral range.  What are the 
relationships between wavenumber, wavelength and energy?  What are the 
relationships between the interferometer resolution in the different 
units? 

\item The effect of purging the apparatus:

At 4~cm$^\text{-1}$ resolution, acquire a spectrum without nitrogen flow.  You 
will want to make use of the {\tt Auto Avg} feature in the {\tt Mirmat} set-up
initialization screen.  Choose loop mode and collect about 50 scans (1 loop) 
of data, saving the spectrum.  Now gently flow N$_\text{2}$ gas through the 
FTIR and source units and let them purge for a while.  Take a new spectrum 
after 5 min.\ of purge and another one 5 min.\ later (saving both, of 
course).  What do you observe?  Using the graphical dialog box and the math 
calculator supplied by the software, plot and save both the transmittance 
and absorbance of air over the useful spectral range.  Identify as many 
peaks as you can (hint: you can switch the {\slshape x}-axis between 
several units, which may be useful for comparison with published data).  
For the peaks of a given molecule, are the absorption peaks equidistant in 
energy?  Can you identify the water peaks?  

{\slshape NOTE: there is a button in the graphical dialog box which allows you 
to put desired spectra in different memory boxes (A, B, C, ?); use this 
feature but make sure that you don't get confused as to which spectrum has 
been saved in a given memory as the math calculator uses those memory labels 
in its formulae. However, you may find it easier just to export all the data to MATLAB and do the manipulations there.}

Continue purging the system with N$_\text{2}$ for the remainder of the lab.

\item Signal to noise ratio of the interferometer:  

Acquire a signal at 4~cm$^\text{-1}$ resolution.  Acquire the data in loop 
mode with 2 loops of 25 scans (save these with a suitable filename).  Use the 
software's math calculator and the instructions in the Spectra-Physics manual 
to determine the Signal to Noise (S/N) ratio over the useful spectral range.  
Repeat for 1~cm$^\text{-1}$ resolution.  If time allows, repeat for 
1~cm$^\text{-1}$ resolution but 50 scans per loop.  

\item Throughput of the gas cell: 

Install the gas cell and purge it too with N$_\text{2}$ making sure that the 
valve on the gas cell is open.  Wait a sufficient amount of time for the 
system to be well purged.  Acquire signal at 4~cm$^\text{-1}$ resolution and 
use the throughput spectrum from the last step as your normalization spectrum.  
Measure the optical throughput of the gas cell (which corresponds largely 
to the transparency of the windows).  What is the transmittance and 
absorbance of this system?  Is it transparent in the spectral range of 
interest for the study of the CO$_\text{2}$ vibrational absorption lines?  

\item Absorption of CO$_\text{2}$ gas:  

Replace the N$_\text{2}$ flowing through the gas cell with CO$_\text{2}$. 
Adjust the CO$_\text{2}$ pressure to be no more than a few psi - just enough to make sure 
there is flow through the cell; let the gases exchange over a period of 1 
minute.  {\bfseries IMPORTANT}: shut the gas cell valve after 1 minute and fan 
the area with your labbook --- CO$_\text{2}$ in sufficient quantities will 
cause headaches.   Fortunately, our lab is large and fairly well ventilated. 

Acquire a signal at 4 cm$^\text{-1}$ resolution.  What are the transmittance 
and absorbance of CO$_\text{2}$?  Try to identify the vibrational mode 
corresponding to each peak.  Export the absorbance peaks to {\tt .txt} files 
to fit the lineshapes.  Are the peaks more Gaussian or Lorentzian in shape?

\item Absorption of air:

Let the cell fill with air and take data. Plot both air and CO$_\text{2}$ data on the same graph. 

\item Use these data to measure the fraction of CO$_\text{2}$ in the atmosphere. Use absorption data given in Table~{\ref{tab:CO2}}.

\begin{table}
\begin{center}
\begin{tabular}{|c|c|}
\hline
Frequency (cm$^{-1}$) & Absorption $a$(cm$^{-1}$) \\ \hline
2260&	0.3476\\
2272&	0.5289\\
2284&	0.4268\\
2296&	1.8156\\
2308&	6.7811\\
2320&	22.5790\\
2332&	49.6230\\
2344&	32.6236\\
2356&	51.3564\\
2368& 51.8147\\
2380&	7.1494\\
2392&	0.0493\\
2404&	2.0160E-05\\
2416&	3.6899E-05\\
2428&	1.5972E-05\\
2440&	4.4422E-05\\ \hline
\end{tabular}
\end{center}
\caption{Absorption of CO$_2$ at standard temperature and pressure
  (STP), convoluted with the lineshape of the laboratory spectrometer
  run with a resolution of 4~cm$^{-1}$. The exponential form is given,
  so the transmission $T = exp(-acd)$ where $c$ is the concentration
  with respect to 100\% CO$_2$ at STP, and $d$ is the
  pathlength. Original absorption line parameters are from the HITRAN
  database.}
\label{tab:CO2}
\end{table}

\end{enumerate}

\section{References}
\begin{enumerate}
\item Earl J.\ McCartney, {\slshape Absorption and Emission by 
Atmospheric Gases} (John Wiley \& Sons, 1983).

\item Alois Fadini and Frank-Michael Schnepel, {\slshape Vibrational 
Spectroscopy} (Ellis Horwood Limited, 1989).

\item Spectra-Physics, {\slshape MIR 8000 Modular Infrared Fourier 
Transform Spectrometer} (Spectra-Physics, 2003).

\item J.\ Kauppinen and J.\ Partanen, {\slshape Fourier Transforms in 
Spectroscopy} (Wiley-VCH, 2001).

\item Peter R.\ Griffiths, James A.\ de Haseth, {\slshape Fourier 
Transform Infrared Spectrometry} (John Wiley and Sons, 1986).

\item HITRAN database www.cfa.harvard.edu/hitran/
\end{enumerate}

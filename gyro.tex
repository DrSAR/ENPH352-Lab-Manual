\chapter{Air Suspension Gyroscope}
\renewcommand\chapname{Gyro}
\chapternumberreset
\section{Purpose}
To measure the absolute rotation rate of the Earth, and to verify equation \ref{eqn:gyro-main}.

\section{Theory}
The gyroscope precession equation is
\begin{equation}
\label{eqn:gyro-main}
\vec\tau = \vec\Omega\times\vec J
\end{equation}
where $\vec\tau$ is the torque applied to an object spinning with angular 
momentum $\vec J$, and $\vec\Omega$ is its resulting precessional angular 
velocity.

\begin{figure}[htbp]
\begin{center}
\psfrag{Alpha}{$\alpha$}
\psfrag{Theta}{$\theta$}
\psfrag{om}{$\vec\omega$}
\psfrag{OM}{$\vec\Omega$}
\psfrag{arr}{$r$}
\psfrag{Eh}{$a$}
\includegraphics[height=2in]{figs/gyro-ball.eps}
\caption{The gyroscope rotor.}
\label{fig:gyro-ball}
\end{center}
\end{figure}

Our gyroscope, consisting of a steel ball with one flat surface, is depicted 
in Figure \ref{fig:gyro-ball}.  It spins about a horizontal axis with angular 
velocity $\omega$, suspended by an air jet about its geometrical axis.  The 
flat surface displaces the centre of gravity from the geometrical centre by a 
distance $r$, leading to a torque.  In this configuration $\vec\Omega$ and 
$\vec J$ are perpendicular, hence
\begin{align}
\tau &= mgr \\
\vec J &= I\vec\omega
\end{align}
where the moment of inertia $I=mk^2$ and $k$ is the moment of gyration.  Thus 
equation \ref{eqn:gyro-main} becomes
\begin{align}
mgr &= \Omega mk^2\omega\sin\frac{\pi}{2}\ \ \ \ \ \mbox{or}\\
%\frac{\Omega k^2\omega}{gr} &= 1 \label{eqn:gyro-fit}
\Omega =  &= \frac{gr}{k^2\omega} \label{eqn:gyro-fit}
\end{align}
The object of the experiment is therefore to measure $\Omega$, 
$\frac{k^2}{r}$ and $\omega$ in order to verify equation 
\ref{eqn:gyro-fit}.  In the Appendix (section \ref{sec:gyro-appendix}) it 
is shown that
\begin{equation}
\frac{k^2}{ra} = \frac{30\left(1 + \cos\alpha\right) -20\left(1 + 
\cos^3\alpha\right) + 6\left(1 + \cos^5\alpha\right)}{15\sin^4\alpha}
\end{equation}
where $a$ and $\alpha$ are as shown in figure \ref{fig:gyro-ball}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[height=2.7in]{figs/gyro-setup2.eps}
\caption{The air suspension gyroscope.}
\label{fig:gyro-setup}
\end{center}
\end{figure}

The essential parts of the apparatus are shown in Figure \ref{fig:gyro-setup}. 
The gyro rotor consists of a steel ball bearing freely suspended on a jet of 
air.  A wire gauze filter in the air inlet fitting removes turbulence from the 
incoming air jet, to minimize spurious torques.

A flat spot ground on one side of the ball provides both an accurately 
measurable gravitational torque and a mirror from which to reflect a laser 
beam for precision timing of the precession period.  The ball is permanently 
magnetized and the field coil, carrying a 60Hz oscillating current, drives the 
magnetized ball as a synchronous motor.  The spin axis is perpendicular to the 
flat face of the ball.

\begin{figure}[htbp]
\begin{center}
\psfrag{Coil}{\sf Coil}
\psfrag{5kW}{\sf 5k$\sf\Omega$}
\psfrag{Off}{\sf Off}
\psfrag{MaxLag}{\sf Max phase lag}
\psfrag{MinLag}{\sf Min phase lag}
\includegraphics[width=0.4\textwidth]{figs/gyro-circuit.eps}
\caption{The field control circuit.}
\label{fig:gyro-circuit}
\end{center}
\end{figure}

The driving magnetic field is controlled by the circuit shown in Figure 
\ref{fig:gyro-circuit}.  Power comes from the Variac variable voltage source.  
``Minimum phase lag" provides maximum dynamic stability, for easy starting, for 
rapid correction of rotor orientation, and for rapid damping of nutation and 
hunting.  With the field control switch in the ``Maximum Phase Lag" (up) 
position a 5 k$\mathsf\Omega$ series resistor drops the coil current.  Maximum 
phase lag minimizes spurious electromagnetic torques, for best experimental 
accuracy.

\section{Procedure}

\begin{enumerate}
\item Clean the rotor with methanol and mark the edge of the flat face with a 
felt pen to give an easily observable dot. 

\item With the flat face of the rotor upward, slowly increase the
  suspension airflow until the rotor is freely suspended.  To minimize
  spurious air torques, adjust the airflow to slightly above the
  minimum needed to provide a free stable suspension.  Too much
  airflow will cause turbulence and too little airflow causes friction
  --- both effects increase the precession period. You can investigate
  these effects once you have the rotor precessing.  Orient the rotor
  with the normal to its flat face horizontal, and set it spinning
  about this normal by directing the accelerating air jet along its
  top surface.  Where should the jet be directed to have maximum
  effect?  Steady the centre of the flat face lightly with the point
  of the plastic rod.  You will need to keep the rod there through the
  initial acceleration.  Once the rotor is spinning, remove the rod
  and allow the ball to precess as you accelerate it.  The stroboscope
  should be set to ``Line" for 60 flashes per second (think about why
  ``Line'' is necessary and simply setting the flash rate to 60
  flashes per second manually won't work?).  Watching the dot on the
  flat face, what sequence of stationary patterns do you see as the
  gyro accelerates?  A small amount of wobble usually damps out if the
  angular momentum is built up smoothly.  Never let the wobble become
  large enough to carry the flat face of the rotor below the rim of
  the jet.  Any periodic uncovering of the jet orifice produces an
  unstable rotor bounce, which can damage the jet.  If the wobble
  becomes enough to blur the flat face, stop the rotor and start over.

{\bf CAUTION}:  The rotor reaches 3600 rpm in a minute or two.  You should 
be aware that if you were successful in spinning it above 7000 rpm or so, 
there exists a possibility of explosion from centripetal stress.  

\item When the rotor reaches 3600 rpm, apply 120V minimum phase lag power to 
the coil.  Damp out any hunting about the dynamic equilibrium by 
antisynchronous application of the air jet.  Describe in your report the 
orientation of the magnetization of the rotor, the direction of the magnetic 
field produced by the drive coil, and how these together keep the rotor at 
3600 rpm.

\item Direct the laser beam at the rotor so that it hits the flat face and 
reflects from it.  The height of the reflected beam on the black wall 
surrounding the apparatus, together with the height of the laser aperture, will
allow you to check the vertical alignment of the flat face.  Use a level to 
determine how flat and horizontal the bench is.  If necessary, adjust the 
rotor's vertical alignment by applying {\slshape small} torques with the air 
jet at the appropriate point on the rotor, remembering equation 
\ref{eqn:gyro-main}. Any residual nutation or hunting should be removed 
automatically by the minimum phase lag magnetic field during the first few 
precessions.  As the initial oscillations subside, reduce the applied voltage.  
The dot on the face may rotate a little as you do this.  The hunting period 
with maximum phase lag should be $\sim$5-6 seconds.  

\item Once you are familiar with getting the rotor up to speed and locked into 
synchronization, you can measure the rotor dimensions with the dial micrometer 
and the calibrated blocks.  

$\star$ The remaining steps must all be completed on the same day.  Before 
taking data, make sure you are using the same rotor and that it is clean.

\item Get the rotor up to speed and turn on the minimum phase lag.  Allow it 
to precess long enough that the nutation subsides, then reduce the voltage to 
take data.  Measure the precession period several times, adjusting the 
suspension air flow rate to determine which air flow gives the minimum period. 
{\bf Do not reduce the suspension air flow too much with the rotor at speed.  
The spherical surface of the suspension jet will be damaged if it is shut off 
with the rotor spinning}.  Make a note of the directions of $\vec\omega$ and 
$\vec\Omega$ --- verifying the vector equation \ref{eqn:gyro-main} requires 
doing a quick check of the direction, not just the magnitude.  

\item When you have found the best suspension airflow rate and the transient 
disturbances have damped out, record the next few (3-5) precession periods.  
Then shut off the magnetic field, slow the rotor with the air jet, and 
continue to accelerate it with opposite $\vec\omega$.  Measure the precession 
period a few times in this direction.  Make sure you do not change the 
suspension airflow rate between the two directions. The data should be taken 
in the second direction immediately after the first direction.

\item While you are waiting to take time data, calculate the difference in period you expect for the two directions. Do you use the sidereal or solar period of the Earth?

%\item From the results of the two different directions, determine the
%  absolute rotation rate of the Earth, $\Omega_{\earth}$. Plot
%  $\Omega_{meas}$ against $\Omega_{\earth}$ and determine the value of
%  $\Omega$ for $\Omega_{\earth} = 0$.
\item From the results of the two different directions, determine the
  absolute rotation rate of the Earth, $\Omega_{\earth}$. Combine the
  results from the two directions to find the absolute rotation rate of the 
  gyroscope. Should you combine the periods or frequencies from the two directions?


\item In order to calculate $\frac{gr}{k^2\omega}$, you must first
  determine an accurate value of $g$ for the lab. Hebb is located at
  49.2661$^\circ$N and 123.2516$^\circ$W, and the elevation in the lab
  is about 110m.

\item Calculate the experimental values of $\frac{gr}{k^2\omega}$,
  $\Omega$, and their uncertainties.  You should find that there is
  one experimental quantity that contributes the most to the final
  uncertainty.  State which quantity this is and compare your results
  with the expected result.  Is equation \ref{eqn:gyro-main} verified?
\end{enumerate}

\section{Error Analysis}
It is fairly easy to validate equation \ref{eqn:gyro-fit} with relatively good 
accuracy, and furthermore to know what factors limit the accuracy.  You are 
required to calculate these and to check that your result comes within the 
predicted uncertainty.  The analysis is performed in the Appendix.  Proceeding 
in the usual way, one differentiates equation \ref{eqn:gyro-fit} to obtain:
\begin{align}
\label{eqn:gyro-error}
\begin{split}
\left[\frac{d\left(\frac{gr}{k^2\omega}\right)}{\frac{gr}{
k^2\omega}}\right]^2  = & 
\left[\frac{d\omega}{\omega}\right]^2 + \left[\frac{da}{a}\right]^2 + 
\\
&\left[\left\{\frac{-30\sin\alpha + 
20\left(3\cos^2\alpha\sin\alpha\right) 
- 6\left(5\cos^4\alpha\sin\alpha\right)}{30\left(1 + \cos\alpha\right) - 
20\left(1 + \cos^3\alpha\right) + 6\left(1 + \cos^5\alpha\right)} - 
\frac{15\sin^3\alpha\left(4\cos\alpha\right)}{15\sin^4\alpha}\right\}d\alpha\right]^2
\end{split}
\\
\begin{split}
% &\left[\frac{d\Omega}{\Omega}\right]_{\mbox {pred}}^2
 =  &
\left[\frac{d\omega}{\omega}\right]^2 + \left[\frac{da}{a}\right]^2 +
\\
&\left[\left\{\frac{-30\sin^5\alpha}{30\left(1 + \cos\alpha\right) 
- 20\left(1 + \cos^3\alpha\right) + 6\left(1 + \cos^5\alpha\right)} - 
\frac{4\cos\alpha}{\sin\alpha}\right\}d\alpha\right]^2
\end{split}
\end{align}

Convince yourself that these equations are correct.  

\section{Appendix}
\label{sec:gyro-appendix}
The radius of gyration $k^2$ with respect to the horizontal axis of symmetry 
and the distance $r$ to the centre of gravity as shown in Figure 
\ref{fig:gyro-ball} can be evaluated using the two integrals
\begin{align}
Mk^2 &= \iiint\rho\left(r\sin\theta\right)^2dV \\
Mr &= \iiint\rho\left(r\cos\theta\right)dV
\end{align}
where $M$ is the mass, $\rho$ is the density and $dV$ is the volume 
element.  The most convenient volume element is a thin disk, as shown in 
figure \ref{fig:gyro-ball}, for which the radius of gyration 
$k^2=\frac{1}{2}$ and
\begin{equation}
dV = -\pi\left(a\sin\theta\right)^2d\left(a\cos\theta\right)
\end{equation}

With these values, and throwing in a ($-$) sign because $r$ is negative in 
this co-ordinate system, 
\begin{align}
\frac{k^2}{r} &= -\frac{\int\limits^\pi_\alpha\frac{1}{2}\left(a\sin\theta\right)^2
\pi a^2\sin^2\theta d\left(a\cos\theta\right)}{\int\limits^\pi_\alpha\left(
a\cos\theta\right) \pi a^2\sin^2\theta d\left(a\cos\theta\right)}
\notag \\
\frac{k^2}{ra} &= -\frac{-\int\limits^\pi_\alpha\frac{1}{2}\sin^5\theta 
d\theta}{-\int\limits^\pi_\alpha\sin^3\theta\cos\theta d\theta}
\notag \\
&= \frac{-\frac{1}{2}\int\limits^\pi_\alpha\sin\theta
\left(1-\cos^2\theta\right)^2}{\frac{\sin^4\theta}{4}\Bigr\rvert^\pi_\alpha}
\notag \\
&= \frac{\left[-\cos\theta - 2\left(-\frac{\cos^3\theta}{3}\right) 
- \frac{\cos^5\theta}{5}\right]^\pi_\alpha}{-\frac{1}{2}
\left(-\sin^4\alpha\right)}
\notag \\
&= \frac{\left(1 + \cos\alpha\right) + \frac{2}{3}\left(-1 - 
\cos^3\alpha\right) + \frac{1}{5}\left(1 + \cos^5\alpha\right)}{\frac{1}{2}
\sin^4\alpha}
\\
&= \frac{30\left(1 + \cos\alpha\right) -20\left(1 + \cos^3\alpha\right) + 
6\left(1 + \cos^5\alpha\right)}{15\sin^4\alpha}
\end{align}

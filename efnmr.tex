\chapter{NMR:  Spin-Spin Coupling}
\renewcommand\chapname{EF-NMR}
\chapternumberreset
%\section{Purpose}
%To become familiar with NMR principles.

\section{Introduction} 

Nuclear Magnetic Resonance (NMR) is a quantum mechanical effect which has
been well-studied in physics and has been put to extensive use in
chemistry, and more recently in medicine as Magnetic Resonance Imaging or
MRI (people are often scared of the word ``nuclear").  Many nuclei, most
notably the proton, have non-zero spin angular momentum (which is
quantized), and these magnetic moments tend to align with an applied DC
field.  The timescales for growth toward equilibrium and decay of any
transverse magnetization are in general quite different, and can provide
information about the nuclei's environments.

Despite the fact that NMR is a quantum phenomenon, we recognize that most
students in Physics 352 do not have the quantum background necessary to
fully understand the derivation of NMR.  A classical picture can be used
to convince you of many of the effects you will observe, but keep in mind
that they offer a misleading view of NMR.  As long as you don't think too
hard about what's going on or attempt to predict behaviour that isn't
mentioned, classical mechanics should be perfectly adequate.  As an
example of what can go wrong, the first sentence of the Theory section
seems perfectly reasonable until you look up the radius of a proton or
electron and compare the required speed with the speed of light.

%A short Appendix (section \ref{sec:efnmr-quantum}) is 
%provided with a quantum mechanical derivation of some of the main 
%results.  

\section{Theory}

Classically, nuclei with spin act like spinning charged spheres --- you
can convince yourself that these have both angular momentum and a magnetic
moment.  In this experiment, we will work with the hydrogen nucleus (a
proton) and $^\text{19}$F --- since $^\text{19}$F behaves very much like a
proton, we will only concern ourselves with the proton in this section.  
The proton will have a magnetic moment $\vec\mu$ and angular momentum
$\vec J$, related by
\begin{equation}
\vec\mu = \gamma\vec J
\label{eqn:efnmr-m=gJ}
\end{equation}
where $\gamma$ is called the gyromagnetic ratio (use caution when reading 
the references --- the meaning of this term is highly source-dependent), 
and varies by isotope.  Angular momentum is quantized in units of $\hbar$, 
so $\vec J = \hbar\vec I$, where $\vec I$ is the nuclear spin.  

The energy $U$ of the nucleus due to an applied $\vec B$ field is 
\begin{equation}
U = -\vec\mu\cdot\vec B
\end{equation}
The direction of the applied field $\vec B_o$ is taken to be the $\hat z$ 
direction, so
\begin{equation}
U = -\mu_zB_o = -\gamma\hbar I_zB_o
\end{equation}
As for all angular momenta, the allowed values of $I_z$, $m_I$, are 
quantized as $m_I = I, I-1, 
\ldots, -I$.  Since both $^\text{1}$H and $^\text{19}$F have 
$I=\frac{1}{2}$, the allowed values of $I_z$ are $m_I = \pm\frac{1}{2}$, 
shown in figure \ref{fig:efnmr-splitting}.  
\begin{figure}[htb]
\begin{center}
\psfrag{B=0}{$\vec B=0$}
\psfrag{B=Bo}{$\vec B=\vec{B_o}$}
\psfrag{DelU}{$\Delta U$}
\psfrag{spindown}{$m_I=-\frac{1}{2}$}
\psfrag{spinup}{$m_I=+\frac{1}{2}$}
\includegraphics[width=3in]{figs/nmr-splitting.eps}
\caption{\label{fig:efnmr-splitting}A proton's spin states with and 
without an applied magnetic field.}
\end{center}
\end{figure}
A key tenet of quantum mechanics is that energy and frequency are 
proportional, with the constant of proportionality being Planck's 
constant, so the energy difference (splitting) between the two states may 
be written as an angular frequency:  
\begin{equation}
\Delta U = 2\left(\gamma\hbar\frac{1}{2}B_o\right) = \hbar\omega_o
\label{eqn:efnmr-omega}
\end{equation}
where the resonant frequency is $\omega_o = \gamma B_o$.  

%For the proton, 
%$\gamma = 2.675\times 10^8 \frac{\text{rad}}{\text{s}\cdot\text{T}}$, or 
%\begin{equation}
%f_o = 42.58\frac{\text{MHz}}{\text{T}}B_o
%\end{equation}

Since we're working at non-zero temperature, the lower and higher energy 
states' populations ($N_1$ and $N_2$ respectively) will be governed by 
Boltzmann statistics:  
\begin{equation}
\frac{N_2}{N_1}=e^{-\frac{\Delta U}{k_BT}}=e^{-\frac{\hbar\omega_o}{k_BT}}
\end{equation}
giving the net magnetization
\begin{equation}
M_z=\left(N_1-N_2\right)\mu_z=N\mu\tanh\left(\frac{\mu B}{k_BT}\right)
\approx N\frac{\mu^2B}{k_BT}
\end{equation}

\section{Spin-Lattice Relaxation Time}\label{sec:efnmr-T1}
Equilibrium magnetization doesn't appear spontaneously; $M_z$ grows 
exponentially toward equilibrium when the sample is placed in a magnetic 
field or displaced from equilibrium (see figure \ref{fig:efnmr-T1}).  The 
time constant governing this growth is called $T_1$, the spin-lattice 
relaxation time.  
\begin{figure}
\begin{center}
\psfrag{xaxis}{\sf Time}
\psfrag{yaxis}{\sf Magnetization}
\psfrag{Mo}{$\vec{M_o}$}
\psfrag{4T1}{\sf 4$T_1$}
\psfrag{3T1}{\sf 3$T_1$}
\psfrag{2T1}{\sf 2$T_1$}  
\psfrag{1T1}{\sf 1$T_1$}
\psfrag{0}{\sf 0}
\includegraphics[width=8cm]{figs/nmr-T1.eps}
\caption{Exponential growth of the magnetization toward its equilibrium value.}
\label{fig:efnmr-T1}
\end{center}
\end{figure}
\begin{equation}
\frac{dM_z(t)}{dt} = \frac{M_o-M_z(t)}{T_1}
\label{eqn:efnmr-T1}
\end{equation}

If an unmagnetized sample is placed in a magnetic field ($M_z(0) = 0$), 
then direct integration of equation \ref{eqn:efnmr-T1} gives
\begin{equation}
M_z(t) = M_o\left(1-e^{-\frac{t}{T_1}}\right)
\end{equation}
%This is absolutely {\bf\underline{NOT}} what you will be dealing with in 
%this experiment -- you will have different initial conditions!

$T_1$ values can vary from microseconds to seconds for solid and liquid
samples, and can be several weeks for some gas samples ($^3$He for
example).  If a nucleus finds itself in the higher-energy state when the
field is turned on, it will be inclined to decay to the lower-energy
state.  In doing so, it must change not only its energy but also its
angular momentum, and both the energy and angular momentum must be
transferred out of the system, either through collisions or radiation.  
In order for it to decay in the first place, it requires an interaction of
some sort (usually a collision), since the initial and final states are
both eigenstates, and their orthogonality prohibits transitions under
normal circumstances.  Variations in $T_1$ between different materials are
due to the frequency and effectiveness of interactions between particles
and their surroundings (the ``lattice", whether or not it actually is a
lattice) at producing a transition and removing energy and angular
momentum.  The study of these processes is a major topic in NMR research.

%\begin{figure}[htbp]
%\begin{center}
%\psfrag{zed}{$\hat z$}
%\psfrag{why}{$\hat y$}
%\psfrag{ecks}{$\hat x$}
%\psfrag{Eh}{\large\bf a)}
%\psfrag{Bee}{\large\bf b)}
%\psfrag{See}{\large\bf c)}
%\psfrag{Dee}{\large\bf d)}
%\psfrag{Eee}{\large\bf e)}
%\psfrag{Equilibrium}{\sf Equilibrium}
%\psfrag{Mz=Mo}{$M_z=M_o$}
%\psfrag{pi pulse}{\sf$\pi$ pulse}
%\psfrag{Mz=-Mo}{$M_z=-M_o$}
%\psfrag{t=tau-}{$t=\tau$}
%\psfrag{Mz(tau)}{$M_z(\tau)$}
%\psfrag{second pulse}{\sf$\frac{\pi}{2}$ pulse}
%\psfrag{Myx}{$M_{xy}$}
%\psfrag{FID}{\sf FID}
%\psfrag{Mxy precesses}{\sf$M_{xy}$ precesses}
%\includegraphics[width=\textwidth,clip]{figs/nmr-zerocross.eps}
%\caption[]{Measuring $T_1$: {\bf a)}~The initial magnetization is 
%$M_o\hat z$. 
%{\bf b)}~A $\pi$ pulse is used to invert the magnetization to $-M_o\hat 
%z$. 
%{\bf c)}~$M_z$ decays back toward equilibrium until $t=\tau$\ldots\ {\bf 
%d)}~when a $\frac{\pi}{2}$ pulse is used to rotate the magnetization into 
%the 
%$xy$ plane\ldots\ {\bf e)}~where it precesses briefly, and can be 
%measured, 
%before it vanishes again. In these figures, $\vec B=B_o\hat z$.} 
%\label{fig:efnmr-zerocross}
%\end{center}
%\end{figure}

%In practice, $T_1$ is measured using a sequence of two pulses of radio 
%frequency radiation (RF).  A $\pi$ pulse is used to invert the magnetization,
%then after a delay of $\tau$, a $\frac{\pi}{2}$ pulse rotates whatever 
%magnetization there may be into the $xy$ plane where it can be measured (see
%figure \ref{fig:efnmr-zerocross}).  How and why the magnetization can be 
%rotated 
%is described in section \ref{sec:rotating}, and why it's measured in the 
%$xy$ plane is described in section \ref{sec:efnmr-apparatus}.  

\section{Spin-Spin Relaxation Time}\label{sec:efnmr-T2} 

In thermal equilibrium, there is no magnetization in the $xy$ plane,
since angular momentum can only be known about one axis at a time.  This
can also be justified classically:  The torque $\vec\tau$ on each is
$\vec\mu\times\vec B$, but $\vec\tau = \frac{d\vec J}{dt}$ and $\vec J =
\frac{\vec\mu}{\gamma}$ from equation \ref{eqn:efnmr-m=gJ}, so
\begin{equation} 
\vec\mu\times\vec B = \frac{1}{\gamma}\frac{d\vec\mu}{dt}
\label{eqn:efnmr-precess1} 
\end{equation} 
or, for the whole sample, 
\begin{equation} 
\frac{d\vec M}{dt} = \gamma\vec M\times\vec B 
\label{eqn:efnmr-precess2} 
\end{equation}

It can be shown from equation \ref{eqn:efnmr-precess2} that the magnetic 
moment will precess, with the precessional frequency $\omega_o = \gamma 
B_o$ first seen in equation \ref{eqn:efnmr-omega}.  Since the particles in 
the sample started out with random phases, they will continue summing to 
zero.  

What would happen, though, if we started out with the magnetic moments 
rotating in phase in the $xy$ plane?  This situation is a combination of 
spin-up and spin-down (in $z$) states, and will decay exponentially with a 
time constant $T_2$, the spin-spin relaxation time:  
\begin{equation}
\frac{dM_{xy}}{dt} = -\frac{M_{xy}(t)}{T_2}
\label{eqn:efnmr-T2}
\end{equation}

As the name suggests, this decay is due not only to the $T_1$ processes,
but also to the spins' magnetic moments interacting with each other.  
Each spin sees not only the DC field $\vec{B_o}$, but the fields from its
neighbours.  This means that different protons see different fields
depending on their environment, and there is actually a range of
precessional frequencies.  If the spins start out in phase, they soon
dephase, and the rotating coherent $xy$ magnetization goes to zero.  
$T_2$, then, provides information on the distribution of local fields at
the nuclear sites.

%The sensible way to find $T_2$ would seem to be flipping the equilibrium 
%$M_z$ into the $xy$ plane and watching it decay.  This is called the Free 
%Induction Decay (FID),  Unfortunately, the magnet in the lab has its own 
%field inhomogeneities, such that the ``sweet spot" in the middle allows a 
%maximum 0.3ms decay time.  If $T_2\lesssim$0.3ms, this isn't a problem, 
%but most samples require an additional trick.  

%The spin-echo technique allows the measurement of any $T_2$, without the 
%necessity of buying a better and more expensive magnet.  If we first 
%rotate the magnetization from $\hat z$ into the $xy$ plane, then rotate by 
%a further $\pi$ a time $\tau$ later, the dephasing due to the magnet's 
%inhomogeneity is reversed, and the spins rephase a further time 
%$\tau$ after that, for another FID.  This works like the following 
%egalitarian kindergarten footrace:  Each kid runs in a straight line as 
%fast as he or she can until the teacher blows a whistle, at which point 
%the kids run back.  The faster kids go farther, and must return a greater 
%distance, so all return at the same time.  The whistle in this case is the 
%$\pi$ rotation.  The spins in larger fields precess faster by 
%$\Delta\theta (t)$ until $t=\tau$, at which point the magnetization is 
%flipped $\pi$, and their headstart becomes an impediment.  At 
%$t=2\tau$, the $\Delta\theta$ from $t=\tau$ to $2\tau$ has completely 
%cancelled the opposite $\Delta\theta$ from t=0 to $\tau$, and all spins 
%are back in phase.  The spin-spin interactions can't be reversed in this 
%manner, so the echo's height will be lower than the original FID, due to these 
%$T_2$ processes.  A plot of echo height vs. delay time is a graph of the $T_2$ 
%decay with the magnet's inhomogeneity cancelled out.  

%\section{Rotating $\vec M$ by $\pi$ or $\frac{\pi}{2}$}\label{sec:rotating}
%
%An RF magnetic field at $\omega_o$ will rotate the magnetization, with the 
%length of the pulse determining the angle.  The quantum mechanical explanation
%is contained within the appendix (section \ref{sec:efnmr-quantum}). What 
%follows 
%is the classical explanation, using rotating co-ordinate frames.
%
%If we add to our DC field a rotating (circularly polarized) field $\vec{B_1}$, 
%our total field is
%\begin{equation}
%\vec B(t) = B_1\cos\omega t\hat x + B_1\sin\omega t\hat y + B_o\hat z
%\end{equation}
%The convenient rotating co-ordinate frame for this problem has its axis along 
%the static field and rotates at $\omega$.  In this frame, $B_o$ and $B_1$ 
%are joined by an effective field along the $\hat{z^*}$ direction, of magnitude
%$-\frac{\omega}{\gamma}$ (this keeps the magnetization stationary in our 
%rotating frame, which is why we chose it).  The effective field in the rotating
%co-ordinate frame is
%\begin{equation}
%\vec B^*_{eff}=B_1\hat{x^*}+\left(B_o-\frac{\omega}{\gamma}\right)\hat{z^*}
%\end{equation}
%
%The equation of motion from equation \ref{eqn:efnmr-precess2} is
%\begin{equation}
%\frac{d\vec M}{dt} = \gamma\vec M\times\vec B^*_{eff}
%\label{eqn:precess3}
%\end{equation}
%which shows that $\vec M$ will precess about $\vec B_{eff}^*$.  
%
%If the rotating field is at $\omega_o$, $\frac{\omega}{\gamma} = 
%\frac{\omega_o}{\gamma} = B_o$, and $\vec B^*_{eff} = B_1\hat{x^*}$.  The 
%magnetization precess about this effective field at a rate $\omega_1 = 
%\gamma B_1$.  If we turn off $B_1$ at the instant that $\vec M$ reaches the 
%$xy$ plane, we have created a coherent magnetization in the $xy$ plane.  This 
%is a $\frac{\pi}{2}$ pulse, rotating $M_z$ into $M_y$.  If we leave $B_1$ on 
%for twice as long, we have a $\pi$ pulse, which inverts the original 
%magnetization to $-M_z$.  Keep in mind that the magnetizations thus created 
%will decay back to equilibrium, and that any $xy$ magnetization will rotate in 
%the lab frame.  
%
%\begin{figure}[htb]
%\begin{center}
%\psfrag{ecks}{$\hat x^*$}
%\psfrag{why}{$\hat y^*$}
%\psfrag{zed}{$\hat z^*$}
%\psfrag{Eh}{\large\bf a)}
%\psfrag{Bee}{\large\bf b)}
%\psfrag{See}{\large\bf c)}
%\psfrag{Dee}{\large\bf d)}
%\psfrag{Eee}{\large\bf e)}
%\psfrag{Equilibrium}{\sf Equilibrium}
%\psfrag{Mz=Mo}{$M_z=M_o$}
%\psfrag{pulse1}{\sf$\frac{\pi}{2}$ pulse}
%\psfrag{My=Mo}{$M_y=M_o$}
%\psfrag{t=tau-}{$t=\tau$}
%\psfrag{t=2tau}{$t=2\tau$}
%\psfrag{DMfast}{$\Delta M_{fast}$}
%\psfrag{DMslow}{$\Delta M_{slow}$}
%\psfrag{pulse2}{\sf$\pi$ pulse}
%\psfrag{My=-Mo}{$M_y=-M_o$}
%\includegraphics[width=\textwidth,clip]{figs/nmr-rotating.eps}
%\caption{The spin echo: {\bf a)}~The initial magnetization is $M_o\hat z$. 
%{\bf b)}~A $\frac{\pi}{2}$ pulse is used to rotate the magnetization into the 
%$xy$ plane. {\bf c)}~The $xy$ magnetization dephases (within the $xy$ plane) 
%due to inhomogeneous field distributions, with the extremes being $\Delta 
%M_{fast}$ (high field) and $\Delta M_{slow}$ (low field), until 
%$t=\tau$\ldots\ {\bf d)}~when a $\pi$ pulse is used to flip the magnetization 
%(flip the $xy$ plane like a pancake).  The spins continue to precess as 
%before, until\ldots\ {\bf e)}~$t=2\tau$, when they are again in phase, and an 
%echo may be observed. After $t=\tau$, the spins again dephase, and the signal 
%disappears. In these figures, the decay of the $xy$ magnetization and the 
%growth of $M_z$ have been omitted for clarity.}
%\label{fig:efnmr-rotating}
%\end{center}
%\end{figure}
%
%Now a graphical explanation of the spin echo is possible -- see figure 
%\ref{fig:efnmr-rotating}.  

\section{Spin-Spin Coupling}

\begin{figure}[htb]
\begin{center}
\psfrag{B=0}{$\vec B=0$}
\psfrag{B=Bo}{$\vec B=\vec{B_o}$}
\psfrag{DelU}{$\Delta U$}
\psfrag{spindown}{{\Large$\downharpoonright$}}
\psfrag{spinup}{{\Large$\upharpoonright$}}
\psfrag{Jay}{$\frac{J}{2}$}
\psfrag{with}{$J$-coupling}
\psfrag{upup}{{\Large$\upharpoonright$}{\scriptsize$\upharpoonright$}}
\psfrag{upd}{{\Large$\upharpoonright$}{\scriptsize$\downharpoonright$}}
\psfrag{dup}{{\Large$\downharpoonright$}{\scriptsize$\upharpoonright$}}
\psfrag{dd}{{\Large$\downharpoonright$}{\scriptsize$\downharpoonright$}}
\includegraphics[width=4in]{figs/efnmr-coupling.eps}
\caption{\label{fig:efnmr-J}A proton's spin states in the presence 
of a second nuclear spin, where having the two spins aligned is 
energetically favourable.}
\end{center}
\end{figure}

When two NMR-active nuclei are coupled to each other, whether they
interact directly or via electrons in the molecule, their energies split.  
The energy for the first spin to be up will depend on whether the other
spin is up or down, and vice versa.  When one spin is flipped, it changes
the sign of its interaction with both the applied field and the second
spin.  Figure \ref{fig:efnmr-J}\ shows the states present when having the
two spins aligned is energetically favourable.  Note that flipping the
first spin does not change the energy by $\hbar\omega_o$, but by
$\hbar\omega_o\pm\frac{J}{2}$ depending on the state of the second spin.

The splitting, $J$, between the observed transition energies (for flipping
the first spin) is an indication of how strongly it interacts with the
other spin.  The second spin will have the same splitting $J$ appear in
its spectrum.

If we can flip one spin independent of the other (e.g. if they have 
different gyromagnetic ratios and thus resonant frequencies), we should 
find absorptions at energies of 
$\hbar\omega_o\pm\frac{J}{2}$, and the up and down first spins will
precess at frequencies $\omega_o\pm\frac{J}{2\hbar}$ about an applied
field.  If the second spin's populations were equal (infinite 
temperature), we would have a signal proportional to
\begin{align}
N_1e^{i\left(\omega_o+\frac{J}{2\hbar}\right)t}
+ N_2e^{i\left(\omega_o-\frac{J}{2\hbar}\right)t} 
&= \frac{N}{2}e^{i\omega_ot}\left(e^{i\frac{J}{2\hbar}t}
+ e^{-i\frac{J}{2\hbar}t}\right)\\
&= Ne^{i\omega_ot}\cos\frac{Jt}{2\hbar}
\end{align}
where $Ne^{i\omega_ot}$ describes the precession in the absence of 
interloping neighbours.  This result amounts to putting the unperturbed 
precession signal inside a sinusiodal envelope.  

\section{Apparatus}\label{sec:efnmr-apparatus}

\begin{figure}[htb]
\begin{center}
\psfrag{Coil1}{Sample coil}
\psfrag{Coil2}{Bucking}
\psfrag{coil3}{coils}
\psfrag{Sample}{Sample}
\psfrag{Bee}{$\vec B_{earth}$}
\includegraphics[width=0.6\textwidth,clip]{figs/efnmr-coils.eps}
\caption{\label{fig:efnmr-coils}Schematic diagram of the EF-NMR apparatus 
and sample.  The more distant gradient coils are not shown.}
\end{center}
\end{figure}

The sample is held in a Nalgene bottle, which is placed inside a set of
three coils (which should be perpendicular to the earth's field) --- see
figure \ref{fig:efnmr-coils}.  The central coil is used to apply a DC
field to polarize the sample and is later reused to measure its
precession.  A set of two bucking coils surround this, farther from the
sample.  The bucking coils combined have the same product of turns times
area as the inner coil.  Any stray fields will induce the same voltage in
the bucking coils as the inner coil, so the coils are connected in series
such that this noise cancels out.  Rotating magnetization in the sample
induces much stronger currents in the inner coil than the outer, so this
signal survives essentially intact.  The coil assembly is mounted as far
as possible from noise sources and ferromagnetic materials.

If you have rotating magnetization inside a coil, and the axis of rotation
is not parallel to the coil's axis, an AC signal is induced in the coil
--- this is how AC wall power is generated, and it allows you to measure
$M_{xy}$.

%Left unmolested, $M_z$ would grow to its
%equilibrium value and be completely boring, but any $M_{xy}$ will precess
%with angular frequency $\omega_o$ (see section \ref{efnmr-sec:T2}).  

%\begin{figure}[thb]
%\begin{center}
%\psfrag{Pulse}{\sf Pulse}
%\psfrag{Programmer}{\sf Programmer}
%\psfrag{ReF}{\sf RF}
%\psfrag{RF Synthesized}{\small\sf RF Synthesized}
%\psfrag{Oscillator}{\small\sf Oscillator}
%\psfrag{Amplifier}{\sf Amplifier}
%\psfrag{Helmholtz}{\sf Helmholtz}
%\psfrag{Coils}{\sf Coils}
%\psfrag{xBo}{$\bigotimes \vec{B_o}$}
%\psfrag{Sample}{\sf Sample}
%\psfrag{CW-RF}{\sf CW-RF}
%\psfrag{Mixer}{\sf Mixer}
%\psfrag{RF Amplitude}{\small\sf RF Amplitude}
%\psfrag{Detector}{\small\sf Detector}
%\psfrag{Receiver}{\small\sf \!\!Receiver}
%\psfrag{Oscilloscope}{\sf Oscilloscope}
%\psfrag{Sync}{\sf Sync}
%\psfrag{Signal}{\sf Signal}
%\includegraphics[width=\textwidth]{figs/nmr-block.eps}
%\caption{A block diagram of the NMR spectrometer.}
%\label{fig:efnmr-block}
%\end{center}
%\end{figure}
%
%The NMR spectrometer unit consists of several parts (see figure 
%\ref{fig:efnmr-block}):  An RF source (the master oscillator), a pulse 
%programmer
%to turn the RF on and off in short pulses, and the mixer.  The mixer 
%effectively multiplies the detector signal with the original RF, to 
%generate 
%an envelope waveform, which will have beats if the oscillator is not 
%tuned to 
%$\omega_o$.  You have a dual-channel oscilloscope to let you view the 
%detector 
%and mixer signals simultaneously.  

%To allow the equilibrium magnetization $\vec{M_o}$ to be established 
%between repetitions of the experiment, one must wait at least 3$T_1$, and
%preferably 6-10 $T_1$'s, between pulse sequences.  Water ($T_1\approx$ 
%3s) 
%would be annoying to work with, and you can imagine your frustration if 
%you tried using $^3$He gas, with a $T_1$ of about a week.  Since several 
%adjustments must be made before data can be taken, these samples would 
%make for an infuriating experiment.  Mineral oil has a $T_1$ on the order 
%of a few tens of milliseconds at 25$^\circ$C, so a repetition time of 
%100-200ms should be adequate.  
%
%\section{Getting Started}
%Before doing anything else, you should turn on the magnet and its water 
%cooling.  The magnet's field may never fully stabilize, but the greater 
%its headstart, the easier your measurements will be.  
%
%\subsection{Single Pulse}
%Typical NMR pulse widths range from 1-35ms.  To start with, let's observe 
%a single A pulse.  The pulse programmer settings are:  
%
%\begin{tabular}{ll}
%A-width & halfway\\
%Mode & Int\\
%Repetition time & 10ms, variable 10\% \\
%Sync & A\\
%A, B pulses & On, Off\\
%Sync Out & To scope's external trigger input\\
%A \& B Out & To scope Channel 1\\
%\end{tabular}
%
%\noindent Set the oscilloscope to trigger on the rise of the sync pulse, 
%use a sweep rate of 2-10$\sf\mu$s/cm, and 1V/cm vertical scale.  Change 
%the A-width and observe the effect on the pulse.  Switch the mode to Man, 
%and observe the pulse when you press the main start button.  Set the 
%scope's sweep rate to 1ms/cm and repetition time to 10ms, then change the 
%variable repetition time from 10\% to 100\% -- what do you observe?  
%
%\subsection{The Pulse Sequence}
%To measure $T_1$ or observe a spin echo, at least two pulses are 
%required.  
%
%\begin{tabular}{ll}
%Delay Time & 0.10$\times 10^0$ (100$\mu$s)\\
%Mode & Int\\
%Repetition Time & 100ms, variable 10\%\\
%Sync & A\\
%A, B pulses & On, On\\
%\end{tabular}
%
%\begin{figure}[htb]
%\begin{center}
%\includegraphics[angle=-90,width=0.8\textwidth]{figs/nmr-pulses.eps}
%\caption{A two-pulse sequence}
%\label{fig:efnmr-pulse}
%\end{center}
%\end{figure}
%
%\noindent The pulse train should now appear like figure 
%\ref{fig:efnmr-pulse}.  
%Change the A and B widths, change the delay time, change sync to B, turn 
%off A, then B, change the number of repetitions, and observe what happens 
%(i.e. fiddle with the equipment).  Look at a two-pulse sequence with delay 
%times from 1 to 100ms (1.00$\times$10$^0$ to 1.00$\times$10$^2$).  Be 
%careful reading this number -- the decimal point is not where you expect!  
%
\section{Procedure}
%\begin{figure}[htb]
%\begin{center}
%\includegraphics[angle=-90,width=0.8\textwidth]{figs/nmr-fid.eps}
%\caption{A free induction decay envelope and the detector signal mixed with 
%the original RF, with the spectrometer not quite tuned to resonance.  The 
%beats are clearly visible.}
%\label{fig:efnmr-fid}
%\end{center}
%\end{figure}

%\subsection{Spin-Lattice and Spin-Spin Relaxation Times ($T_1$ and $T_2$)}

%As the time constants for exchange of energy and angular momentum within 
%the sample and with the surroundings, $T_2$ and $T_1$ are important 
%parameters to know and understand.  

\begin{enumerate}

\item Turn on the main EF-NMR controller and its power supply, place a 
125~mL Nalgene bottle full of water inside the centre coil, and connect an 
oscilloscope to the outputs.  

\item Ensure you have an optimal signal (see procedure in the manual on the bench). Note you may need to change the field gradient adjustment.

\item Depressing the start button runs a current through the central coil 
for a set period of time (controlled via a dial below the digital 
readout), polarizing the sample along an axis that's
(hopefully) perpendicular to the earth's field.  When this polarizing
field shuts off, the newly formed magnetization slowly decays while
precessing about the earth's field.  The timescale for the former process
is $T_1$, while the timescale for the latter is $T_2$.  Measure $T_2$ and, 
by varying the polarization time, determine $T_1$.

\item In place of the water, insert a bottle of a fluorinated oil.  Repeat 
the above experiment to determine $T_1$ and $T_2$ for $^{19}$F in the oil.  

\item From the precession frequencies of $^{19}$F and $^1$H, determine 
$^{19}$F's gyromagnetic ratio.  Determine the magnitude of the earth's 
magnetic field.  

\item Look carefully at the free induction decay signals from the
  water and the fluorinated oil. Aside from the amplitude and frequency, do you see
any difference? Take a Fourier transform of the two free induction decays and plot
the spectra. Can you explain the differences between them?


%\item One oil contains protons and fluorine atoms in close proximity, and
%thus exhibits spin-spin coupling.  Find the coupling $J$ between the
%protons and $^{19}$F nuclei in this oil.

\item It's also possible to study the effect of changing concentrations of 
dirt in water.  Dirt probably meaning CuSO$_4$, as it's actually magnetic.

\item There are field gradient coils around the sample, allowing $x$,
  $y$ and $z$ gradient fields.  If you have time and the inclination,
  you can explore the use of the gradients (please record the current
  settings on the gradients before you adjust them, and return them to
  their optimal settings when you are done).  Is is possible to use
  the gradients to create an image of your sample?

\end{enumerate}

%\item Re-tune the spectrometer to resonance for a single pulse FID 
%signal.
%
%\item Change the repetition time, reducing the FID to roughly 
%$\frac{1}{2}$
%of its largest value.  This gives an idea of the order of magnitude of 
%$T_1$.  
%\end{enumerate}
%
%At this repetition, the magnetization can't fully return to equilibrium, 
%but 
%does make it partway back.  This measurement is useful as it gives you an 
%idea 
%of the time constant you want to measure, and helps you choose the delay 
%settings.  
%
%The pulse sequence we'll use to find $T_1$ is 
%
%\begin{displaymath}
%\pi \xrightarrow{\hspace{15pt}\tau\hspace{15pt}}\frac{\pi}{2}\mbox{, FID}
%\end{displaymath}
%
%The $\pi$ pulse inverts the magnetization ($M_z \rightarrow -M_z$).  A 
%time 
%$\tau$ later, whatever magnetization exists gets rotated $\frac{\pi}{2}$ 
%into 
%the $xy$ plane for observation, where it decays in an FID.  After the 
%first 
%pulse, the magnetization grows exponentially back from $-M_z$ to $M_z$ 
%(it 
%does {\bf not} rotate!), but the spectrometer can only measure precessing 
%$xy$ 
%magnetization, so the magnetization must be knocked over into the $xy$ 
%plane 
%to be seen.  The $\frac{\pi}{2}$ pulse acts to sample $M_z$ at the time 
%selected.  
%
%You should be able to easily work out from equation \ref{eqn:efnmr-T1} 
%the 
%equation governing the growth back to equilibrium.  A two-digit value of 
%$T_1$ may be found by finding the zero-crossing point (which is not 
%itself 
%$T_1$), but a better value is obtained by graphing the decay back to 
%equilibrium and fitting to the correct function.  Find $T_1$ by both 
%methods.  
%
%\subsection{Spin-Spin Relaxation Time $T_2$}
%
%To find $T_2$, a simple $\frac{\pi}{2}$ pulse will not suffice, due to 
%inhomogeneities in the DC field.  You'll need to use the spin-echo 
%technique.  The pulse sequence is
%\begin{displaymath}
%\frac{\pi}{2} \xrightarrow{\hspace{15pt}\tau\hspace{15pt}} \pi
%\xrightarrow{\hspace{15pt}\tau\hspace{15pt}}\mbox{ echo}
%%$\frac{\pi}{2}$ --- $\tau$ --- $\pi$ --- $\tau$ --- echo
%\end{displaymath}
%
%The $\frac{\pi}{2}$ pulse creates transverse ($xy$ plane) magnetization, 
%which 
%dephases because of inhomogeneities in the applied field.  The $\pi$ 
%pulse 
%reverses these dephasing effects, and the magnetization eventually 
%rephases, 
%forming an echo signal.  The reduction in height from the original FID to 
%the 
%echo is due to $T_2$ processes, so plotting the echo height versus the 
%total 
%delay time (2$\tau$) will give $T_2$.  
%
%Determine $T_2$.  
%

\section{References}
\begin{enumerate}
\item The Basics of NMR, by Joseph P.\ Hornak:
\href{http://www.cis.rit.edu/htbooks/nmr}{\tt
http://www.cis.rit.edu/htbooks/nmr}

\item WebElements:  \href{http://www.webelements.com}{\tt 
http://www.webelements.com}
\end{enumerate}

Lab Manual for ENPH 352 at UBC
==============================
If you submit good pull requests for improvement in the latex text, you will get bonus points on your lab book.

The [current version of the lab manual](https://drsar.gitlab.io/ENPH352-Lab-Manual/enph352.pdf) is recreated automatically when commits are pushed to gitlab (branch master?). 

Building
--------
```
latex enph352
latex enph352
dvips enph352
ps2pdf enph352.ps
```
Because some of the figures use psfrag to replace labels, and that doesn't seem to work properly if you use latexpdf.

Alternative: `latex, latex, dvipdf`

Making and submitting changes
-----------------------------
Fork this project, make your changes, rebase to the most recent status of this repo and submit pull request with an instructive message of your change. Get your change (typos, fixed broken links, correction) accepted. Profit.

\chapter{Electromagnetic Skin Depth of Metals}
\renewcommand\chapname{Skin Depth}
\chapternumberreset
\hyphenation{max-well}
\section{Purpose}

\begin{enumerate}
\item To understand and familiarize yourself with the occurrence and principles
of electromagnetic shielding.

\item To quantitatively examine the skin depth problem by determining the skin 
depths of some metallic pipes.
\end{enumerate}

\section{Theory}
It is common knowledge that electromagnetic radiation does not pass easily 
through a metal.  The time-varying electric and magnetic fields generate 
relatively large screening currents in the metal;  the characteristic 
length scale of the fields' resulting attenuation is the skin depth.  This 
shielding ``efficiency" is strongly dependent on the frequency of the 
radiation, as determined by the Maxwell equations.  

Let a long metal pipe be immersed in a uniform magnetic field which is varying 
with time as $e^{-i\omega t}$.  The magnetic field is parallel to the cylinder 
axis, and this direction is taken to be the $\hat z$ direction of a cylindrical
co-ordinate system.  The pipe has an outer diameter $2R_2$ and inner diameter 
$2R_1$.  

The amplitude and phase of the magnetic field inside the pipe can be calculated
using Maxwell's equations.  Our situation has cylindrical symmetry, so $\vec E$
and $\vec H$ depend only upon the radius.  Moreover, $\vec H$ has only one 
component, $H_z$, and this component does not depend on $z$ -- how good is this 
approximation?  Applying boundary conditions at the inner and outer surfaces of 
the pipe and taking the conductivity $\sigma$ to be non-zero, an exact solution 
may be found to compute the attenuation and phase difference for the fields 
inside and outside the pipe.  Due to the cylindrical nature of the problem, the 
solution involves Bessel's functions.

The solution can be simplified at high frequencies because Bessel's functions 
have rather simple limiting forms for large arguments.  This solution is:

\begin{align}
\frac{H_i}{H_o} &= \rho e^{i\phi}
\\
\rho &= 2\sqrt{\frac{R_2}{R_1}}\left\{\frac{e^{-k_o\left(R_2 -
R_1\right)}}{\sqrt{1+ R_1k_o + \frac{R_1^2k_o^2}{2}}}\right\}
\\
\phi &= k_o\left(R_2 - R_1\right) + \arctan\left(\frac{R_1k_o}{2 + 
R_1k_o}\right)
\\
k_o &= \sqrt{\frac{\omega\sigma\mu}{2}}
\end{align}
where
\begin{align}
H_o &= \text{magnetic field outside the pipe}\notag \\
H_i &= \text{magnetic field inside the pipe}\notag \\
R_1 &= \text{inner radius of the pipe} \notag \\
R_2 &= \text{outer radius of the pipe} \notag \\
\sigma &= \text{conductivity of the metal (}\approx{\sf 3\times 10^5
\frac{1}{\Omega\cdot cm}\text{ for Al})} \notag\\
\mu &= \text{permeability of the metal}\notag\\
\omega &= \text{angular frequency}\notag
\end{align}

$\star$NOTE: This high frequency approximation breaks down for
$k_o\left(R_2 - R_1\right) \lesssim 1$.  You should check what frequency
this is.  It is recommended that you still take data below this point, to
see how seriously the equations' breakdown is, and to check where it
occurs.

\section{Procedure}
\begin{figure}[htb]
\begin{center}
\psfrag{Tek 2232}{\sf Tek 2232}
\psfrag{Oscilloscope}{\sf Oscilloscope}
\psfrag{Ch.1}{\sf Ch.1}
\psfrag{Ext}{\sf Ext}
\psfrag{GPIB}{\sf GPIB}
\psfrag{Computer}{\sf Computer}
\psfrag{Amp}{\sf Amp}
\psfrag{Pickup Coil}{\sf Pickup Coil}
\psfrag{Field Coil}{\sf Field Coil}
\psfrag{Aluminum Pipe}{\sf Aluminum Pipe}
\psfrag{HP 3324A}{\sf HP 3324A}
\psfrag{Sync}{\sf Sync}
\psfrag{Out}{\sf Out}  
\psfrag{out}{\sf out}
\psfrag{in}{\sf in}
\psfrag{Function}{\sf Function}
\psfrag{Generator}{\sf Generator}
\psfrag{100kohm}{\sf 100k$\sf\Omega$}   
\psfrag{50ohm}{\sf 50$\sf\Omega$}
\psfrag{x100}{\sf$\times$100}
\includegraphics[width=15cm]{figs/skinfig.eps}
\caption{A schematic diagram of the skin depth apparatus.}
\label{fig:skinfig}
\end{center}
\end{figure}

In this experiment you will measure the phase and amplitude of an alternating 
magnetic field inside a metal pipe relative to the field without the pipe.  The
schematic diagram of the apparatus is shown in figure \ref{fig:skinfig}.  A 
Physics 409 lab at the same station uses steel pipes as well as aluminum and 
copper, so make sure that your aluminum pipe is really aluminum -- there are 
some very significant differences between aluminum and steel!

A coil approximately 6" long, 2" in diameter, and wound with approximately 10 
turns per cm, is connected to an HP3324A synthesized function generator through
a 50$\sf\Omega$ resistor to avoid short circuiting the output from the 
oscillator.  This system is used to generate an AC magnetic field over
the frequency range 100 Hz to 80 kHz.  A pick-up coil consisting of 200 turns 
of \#38 Formex insulated copper wire wound on a $\sf\frac{3}{8}$" diameter form
is used to probe the magnetic field generated in the primary coil.  The small 
e.m.f.\ generated in the pick-up coil is first amplified by a pre-amp with a 
100k$\sf\Omega$ termination, then displayed on a Tek 2232 digital storage scope.

A LabView-based program on the computer's desktop is used to input the data 
from the scope to the PC.  It will guide you through the data collection 
process and will control the function generator; you must control the 
oscilloscope.  The program obtains a digitized version of the oscilloscope 
trace (1024 points), then fits it to a sinusoid of the form $A + 
B\cos\left(\omega t - \phi\right)$.  For the most accurate fit, the voltage 
settings on the oscilloscope should keep the waveform as large as possible and 
there should be between 2 and 5 waveform periods visible.  The program will 
prompt you to change the time scale on the oscilloscope between data points.  
The program outputs a tab-separated text file ({\tt *.SKD}) with the frequency 
andfitting parameters ($A$, $B$ and $\phi$).  It can also output waveform 
files (the digitized scope traces) if you want to check the fit.  
%
%\begin{figure}[htb]
%\begin{center}
%\psfrag{xaxis}{\sf Time}
%\psfrag{yaxis}{\sf \!\!\!\!\!\!Voltage}
%\psfrag{1}{}
%\psfrag{-1}{}
%\psfrag{0.5}{\sf\ 0}
%\psfrag{1.5}{\sf\ \!0}
%\psfrag{1.0}{$\!\!\!\!\pm\frac{\pi}{2}$}
%\psfrag{A}{\sf A}
%\psfrag{B}{\sf B}
%\psfrag{0}{$\!\!\!\!+\frac{\pi}{2}$}    %%Check signs!!!
%\psfrag{2}{$\!\!\!\!-\frac{\pi}{2}$}    %%Check signs!!!
%\psfrag{0.0}{}
%\psfrag{one}{\bf\Large 1}
%\psfrag{two}{\bf\Large 2}
%\psfrag{three}{\bf\Large 3}
%\psfrag{four}{\bf\Large 4}
%\includegraphics[width=10cm,angle=-90,clip]{figs/sine-grace.eps}
%%\includegraphics[width=10cm,clip]{sine-grace.pdf}
%\caption{The four quadrants and their origins.}
%\label{fig:skin-phase}
%\end{center}
%\end{figure}

The phase is measured from the nearest peak to the leftmost edge of the 
waveform image, so there is some ambiguity about what ``phase" precisely means. While the data acquisition program is working, plot out the equation for $\phi$ as a function of $\omega$ with MATLAB and compare the result with what you see emerging from the experiment. 


%The origin could be at either a max or min (B or A in figure 
%\ref{fig:skin-phase}), and the closest peak may lie to the right or left of the
%start of the waveform.  See figure \ref{fig:skin-phase} for the quadrants.
%
%{\slshape NOTE: This experiment is comparatively light on data collection and 
%heavy on data analysis.  Budget your time accordingly.}  

\begin{enumerate}
\item Open the Skin Depth program and follow the equipment setup instructions.

\item Using calipers, accurately measure the inner and outer radii of the two 
pipes.  Using an accepted value for each conductivity, determine at what point 
the high-frequency approximation should break down.

\item Take data at frequencies between 100~Hz and 80~kHz, ensuring that the 
signal is not clipped or distorted by the pre-amp.  Measure the amplitude and 
phase at each frequency for aluminum, copper, and air.  
%You will need to 
%record the quadrant for each measurement.  
You may need to re-take some of 
the high-frequency pipe data with a higher-amplitude signal from the function 
generator if the signal devolves into noise.  This is easily corrected for 
later, especially if there's overlap in the data.  Note that the program only 
attempts to save your data once, so save it to the hard disk first, where 
there will certainly be space for it.  You can transfer it elsewhere later, by 
floppy, SSH/SFTP or web.  

\item Calculate the expected values of the amplitude ratio and phase 
difference at each frequency, and compare them to their measured values.  

\item Do a non-linear least squares fit to your data to find the 
conductivities.  How different are your values from the accepted values?  

\item For each metal, compare the results obtained by solving each data point 
for a skin depth value versus the expected results using your average $\sigma$ 
found above and the expected $\sigma$.  
\end{enumerate}

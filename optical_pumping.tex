\chapter{Air Flow in Channels}
\renewcommand\chapname{Air Flow}
\chapternumberreset


\section{Purpose}

To measure, analyze and understand the flow of air in a channel of 
varying cross-section. 

%The apparatus is a demonstration device used to 
%show ``Bernoulli" levitation. 

\section{Introduction}

Engineers have to deal with fluid flow in channels in a great variety of 
different contexts, e.g. oil pipelines, air-conditioning systems, wind 
tunnels, to name three diverse applications. Here we ask you to make 
and analyze measurements on a ``Bernoulli" levitation demonstration.
This system offers a 
the possibility of a quantitative description without recourse to 
computational fluid dynamics. It is also a challenge to your physical 
intuition.

\subsection{Theory}


The reduction of pressure in a moving stream of fluid is commonly called
the Bernoulli Effect. It can be simply and dramatically demonstrated by
blowing air into the narrow end of a small funnel. A ping-pong ball placed
in the funnel cannot be blown out; the harder one blows, the more it
sticks in place.  A quantitative description of this
system is not easy however, due to the complicated geometry and
airflow.

To avoid these complexities, we have produced a geometrically simple 
system which
lends itself to a quantitative description.  Air is blown 
vertically downwards through a 
hose which exits in a flat horizontal sheet. Another flat sheet brought 
up to the orifice will be held in place despite the fact that the air 
is pushing downwards. The acceleration of the air in the gap causes a 
drop in pressure which more than compensates for the high pressure in the 
hose. Flowing liquids will also produce the same effect, as one can easily 
show using a flat sheet placed against a water jet in a swimming 
pool or hot tub.



\begin{figure}
\begin{center}
\includegraphics[width=4in]{figs/demo.eps}
\caption{General arrangement and dimensions of Bernoulli demonstration.
The air is piped into the top from a commercial shop-vac.}
\label{fig:air1}
\end{center}
\end{figure}



Our design (fig.\ref{fig:air1}) was fabricated from 3/8" acrylic sheet, a 
material
chosen for its transparency and the smoothness of its surface. What we
show here is that the system is amenable to a fairly straightforward
analysis which illustrates various principles of fluid motion.
The source of air is an outlet of a  common half-horsepower shop-vac 
(which has never been used for cleaning). The nozzle (inner radius 
$r_{hose}$ = 
14~mm) 
is 
inserted into a hole in a horizontal sheet of acrylic, blowing downwards.
The edge of the hole is rounded with an approximately 1~mm radius of 
curvature.
A flat disk of 3/8" acrylic (radius 150~mm)  brought up to the hole at 
first experiences a 
strong downward force, but on being pushed closer it is suddenly grabbed 
by 
the air flow and held in place about 1~mm below the fixed sheet of 
acrylic. This arrangement can levitate approximately 2~kg. In our 
apparatus, the pressure can be measured in the hose and at three different 
radii above the suspended plate. The most important two pressures to 
measure 
are the one  in the hose and that just outside the hose radius, where the 
air velocity 
is the highest and the pressure the lowest. 
It is the thin disk 
of high 
velocity air just outside the hose which is  responsible for the 
levitation.
The pressures are displayed 
by two large analog gauges, chosen for visual effect. 
The hose pressure is controlled using a Variac power supply for the 
shop-vac.
   
\subsection{The Calculation}
%: Bernoulli's Equation, Head Loss and Friction Factors}

The air flows relatively slowly down the hose and then is forced into the 
small gap above the plate. Mass conservation dictates that the air speeds 
up considerably here, and Bernoulli's equation gives the associated 
pressure drop.

\begin{equation}
P_1 + \frac{1}{2}\rho_1 v_1^2 + \rho_1 g h_1 = 
P_2 + \frac{1}{2}\rho_2 v_2^2 + \rho_2 g h_2 
\end{equation}

Here the subscripts 1 and 2 refer to the hose and the entrance to the 
channel respectively; $P$ is the pressure, $\rho$ the density, $v$ the 
velocity, $g$ the acceleration due to gravity, and $h$ the height. 
For the flow between the two plates, consider a wedge of air between 
them as shown in fig.\ref{fig:air2}.
Its radial extent is from $r$ to $r+dr$, azimuthal extent $\delta$, and a 
constant thickness 
 $x$.
Air flows through the wedge at rate of $dq$~kg/s from left to right, 
entering with velocity $v$ and pressure $P$ and leaving with velocity 
$v+dv$ 
and pressure $P+dP$. We can solve the momentum equation for the wedge
by considering the two forces, one from the pressure difference and one 
from wall friction, which cause the acceleration of the air passing 
through.

\psfrag{pv}{$P,v$}
\psfrag{dq}{$dq$}
\psfrag{r}{$r$}
\psfrag{r+dr}{$r+dr$}
\psfrag{v+dv}{$v+dv$}
\psfrag{p+dp}{$P+dP$}
\psfrag{theta}{$\theta$}

\begin{figure}
\begin{center}
\includegraphics[width=4in]{figs/wedge.eps}
\caption{Wedge of air between the two plates used in the calculation.}
\label{fig:air2}
\end{center}
\end{figure}



The force due to wall friction $F_f$ is given in terms of
the friction factor $f$, which can be applied to flow
between flat plates as follows:

\begin{equation}
F_{f} \approx -xrdr\frac{f}{4x} \rho v^2\cdot \delta
\end{equation} 

The friction factor can be either be read off a ``Moody Chart" if 
the Reynolds number is fixed and known, or the factor can be 
calculated 
from the unlikely empirical expression for turbulent flow:

\begin{equation}
\frac{1}{\sqrt{f}} = 2.0 log_{10}(0.64 Re_{D_h} \sqrt{f}) - 0.8
\end{equation}  

The characteristic Reynolds Number for channel flow,
$Re_{D_h}$,
uses the characteristic length $D_h$ defined to be 4 times the
 area of the channel divided by the wetted perimeter, i.e.
twice the gap distance.


\begin{equation}
 Re_{D_h} = \frac{\rho v D_h}{\mu}, \ \ \ \ D_h \approx 2x
\end{equation}

By considering the rate of change of momentum of air passing through the 
wedge, and assuming incompressibility:

\begin{equation}
dp = \rho v^2 dr \left(\frac{1}{r} - \frac{f}{4x}\right)
\end{equation}

\begin{enumerate}

\item Derive the above equation.

\item Justify the assumption of incompressibility.

\end{enumerate}

\section{Levitation Measurements}

\begin{enumerate}

\item Record the ambient air temperature and pressure. Calculate the air 
density.

\item Turn on the shop-vac and levitate the plate without extra weights.

\item Measure the pressure at the four available points. Take care that 
the 
gauges are properly zeroed. Estimate the errors.

\item Repeat with different weights (supplied - no more than 1~kg) on the 
plate. Take care not to let the plate drop; it will chip the acrylic.

\end{enumerate}

\section{Analysis}

1. Solve the differential equation for pressure however you wish. 
Plot the theoretical pressure as a function of radius, 
and add your data points.

2. What is the ``spring constant" of the plate (i.e. variation of force with 
gap size)? Does your theory reproduce this?

3. Does the plate rest at the only position of zero force? Is there another? 
Why does the plate choose one and not the other?



\section*{Bibliography}

 Frank M. White, {\bf ``Fluid Mechanics"},  2nd 
edition, McGraw-Hill (1986)


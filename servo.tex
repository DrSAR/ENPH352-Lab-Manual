\chapter{Servomechanism}
\renewcommand\chapname{Servo}
\chapternumberreset
\section{Purpose}

\begin{enumerate}
\item To construct and study a closed loop servo system.
\end{enumerate}

\section{Theory}

In an automatic control system we are able, by means of transducers, to 
compare the actual output (shaft position, flow rate, etc.) with the 
output required. The system must be able, if there is a difference or 
error, to respond by modifying the signals sent to an actuator.  Because 
this system feeds back information about the actual state for 
comparison with the desired state, it is called a closed-loop system.  
In this experiment you will be studying speed and position control, so the 
transducers used will be a tachogenerator and a rotary potentiometer.  The 
actuator that you will be controlling is a motor.  Most of your voltage 
supply and measuring needs will be met by an analogue-to-digital card, 
controlled under LabView.  

A servomechanism is (usually) an electromechanical analogue of an 
amplifier (it can be operated by other than electrical means --- e.g.\ 
fluidic servos and amplifiers).  The analogy lies in the equations of 
motion governing the dynamics of such systems and the uses to which 
they are put. 

The same equations apply to the electromechanical servo as to an 
electronic amplifier, and the same difficulties apply; large forward 
gains are required to minimize the error signal, but large forward gains 
coupled with requirements for high frequency response lead to instability 
of the system.  The same analysis holds and the same cures are applied.  
Additional complexities arise in mechanical systems, where two types of 
friction are present: Coulomb (independent of speed) and viscous 
(proportional to speed).  Viscous friction is analogous to ohmic 
resistance, but Coulomb friction is difficult to mimic --- it's like the 
forward resistance of a diode.  

\section{Procedure}

There is an extensive equipment manual/write-up available which describes 
various measurements that illustrate the properties of the servo system.  
The write up is wordy and covers far more work than you could possibly 
hope to do in the time available.  However, you may wish to peruse it and 
try some of its exercises in order to help you with this lab. 

\subsection{Task 1 --- Operational Amplifier}

The operational amplifier is a high-gain, low-drift DC amplifier, and is 
designed to be independent of variations in the power supply voltage.  The 
advantage of an amplifier with a very high gain, up to 10$^\text{6}$, is 
that it can be used to perform mathematical operations with a high degree 
of accuracy. In this lab, two types of operational amplifier (OU150A and 
PA150C) are used. 

\begin{figure}[htb]
\begin{center}
\psfrag{PowerSupply}{Power Supply}
\psfrag{0}{0}
\psfrag{OP}{OP}
\psfrag{+15}{$+$15}
\psfrag{-15}{$-$15}
\psfrag{Vo}{$V_o$}
\psfrag{V1}{$V_1$}
\psfrag{V2}{$V_2$}
\psfrag{P1}{P$_1$}
\psfrag{P2}{P$_2$}
\psfrag{R1}{R$_1$}
\psfrag{R2}{R$_2$}
\psfrag{OU150A}{OU150A}
\includegraphics[width=.6\textwidth]{figs/servo-1.eps}
\caption{Use of the OU150A op amp. R$_1$ = R$_2$ = 100k$\Omega$.}
\label{fig:servo1}
\end{center}
\end{figure}

\begin{figure}[htb]
\begin{center}
\psfrag{PowerSupply}{Power Supply}
\psfrag{0}{0}
\psfrag{+15}{$+$15}
\psfrag{-15}{$-$15}
\psfrag{V1}{V$_1$}
\psfrag{V3}{$V_3$}
\psfrag{V4}{$V_4$}
\psfrag{P1}{P$_1$}
\psfrag{P2}{P$_2$}
\psfrag{PA150C}{PA150C}
\includegraphics[width=.6\textwidth]{figs/servo-2.eps}
\caption{Use of the PA150C pre-amp.}
\label{fig:servo2}
\end{center}
\end{figure}

\begin{enumerate}
\item OU150A is used as an adder or subtractor (inputs of opposite 
polarity) in this lab. Using the circuit shown in Fig.\ \ref{fig:servo1}, 
verify that the OU150A with negative feedback has the property that
\begin{equation}
\text{V}_o=-\frac{\text{R}_2}{\text{R}_1} (V_1+V_2) = -(V_1+V_2).
\end{equation}
Applying different values of both positive and negative voltage for $V_1$ 
and $V_2$, measure the output $V_o$. Plot a graph of the input voltage 
$(V_1 + V_2)$ versus the output $V_o$. Determine the range of input (sum) 
voltages over which the OU150A responds linearly.  

\item The output of the OU150A can be either positive or negative, but we 
cannot use this polarity to control the direction of motor rotation using 
the servo amplifier SA150D directly.  The pre-amplifier PA150C solves this 
problem. Using the circuit shown in Fig.\ \ref{fig:servo2}, graph $V_3$, 
$V_4$ and $(V_3 - V_4)$ against input voltage $V_1$.  Calculate the gain 
of the PA150C, and determine its input voltage range.  Double the input 
signal by connecting PA150C terminals 1 and 2 --- what is the output 
voltage now?  
\end{enumerate}

\subsection{Task 2 --- Tachometer Calibration}

\begin{figure}[htb]
\begin{center}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{OutputPot}{Output Pot}
\psfrag{MT150F}{MT150F}
\psfrag{ServoAmp}{Servo Amp}
\psfrag{30:1Gear}{30:1 Gear}
\psfrag{PA150C}{PA150C}
\psfrag{Tachometer}{Tachometer}
\psfrag{Vt}{$V_t$}
\psfrag{Vin}{$V_\text{in}$}
\psfrag{+15}{$+$15}
\psfrag{-15}{$-$15}
\psfrag{P2}{P$_2$}
\psfrag{P1}{P$_1$}
\psfrag{+q'}{$\theta$}
\psfrag{-q}{$\theta'$}
\psfrag{q=-30q'}{$\theta'=-\frac{1}{30}\theta$}
\includegraphics[width=.75\textwidth]{figs/servo-3.eps}
\caption{Setup for Task 2.}
\label{fig:servo3}
\end{center}
\end{figure}

Set up the circuit shown in Fig.\ \ref{fig:servo3} (Power supply 
connections for +15, common, and $-$15 are not shown in this or 
subsequent diagrams).  

The tachometer is a DC electrical generator which produces an output 
voltage $V_t$,
\begin{equation}
V_t=k_t \frac{d\theta'}{dt}
\end{equation}
where $\theta'$ is the angular position of the Output Pot, in radians.

\begin{enumerate}
\item Determine $k_t$ by varying $V_\text{in}$ (both polarities).  Why 
does the motor stop when $\left|V_\text{in}\right|$ is less than a certain 
value?  
\end{enumerate}

\subsection{Task 3 --- Motor Transient Response}

\begin{figure}[htb]
\begin{center}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{MT150F}{MT150F}
\psfrag{ServoAmp}{Servo Amp}
\psfrag{PA150C}{PA150C}
\psfrag{Vt}{$V_t$}
\psfrag{Vin}{$V_\text{in}$}
\psfrag{ChAOutput}{ChA Output}
\psfrag{High}{High}
\psfrag{Low}{Low}
\psfrag{Period}{Period}
\includegraphics[width=.75\textwidth]{figs/servo-4.eps}
\caption{Setup for Task 3.}
\label{fig:servo4}
\end{center}
\end{figure}

For any rotational system, we have a dynamic equation 
\begin{equation}
I \frac{d^2\theta}{dt^2} = \tau,
\end{equation}
where $I$ is the moment of inertia of the system and $\tau$ is the 
torque acting on the system. As the input voltage on the motor is varied, 
the motor accelerates;  consequently, its armature generates an increasing 
EMF $V_e$ (which is proportional to the angular speed $\omega = 
\frac{d\theta}{dt}$) back to the motor. The total torque is proportional 
to the combination of input voltage and back EMF. Thus 
\begin{equation}
\tau = k_1 (V_\text{in} + V_e) = k_1 
\left(V_\text{in} - k_2 \frac{d\theta}{dt}\right)
\end{equation}
where $k_1$ and $k_2$ are constants. We can now rewrite the equation of 
motion as 
\begin{equation}
\frac{d^2\theta}{dt^2} + c\frac{d\theta}{dt} = k_m V_\text{in},
\end{equation}
which has the solution 
\begin{equation}
\frac{d\theta}{dt} = \frac{k_m V_\text{in}}{c} \left(1-e^{-ct}\right)
\end{equation}
and
\begin{equation}
\frac{d^2\theta}{dt^2} = k_m V_\text{in} e^{-ct}
\end{equation}
in response to a step-like voltage of amplitude $V_\text{in}$ applied
at $t=0$, with the motor initially at rest. From the above equations, we
obtain
\begin{displaymath}
V_t = \frac{-k_t k_m V_\text{in}}{30 c} \left(1-e^{-ct}\right).
\end{displaymath}

\begin{enumerate}
\item Set up the circuit in Fig.\ \ref{fig:servo4}. Choose suitable high 
and low voltages, and an appropriate period for the ChA Output such that 
the speed of the servo motor will (nearly) stabilize at each input 
voltage.  

\item Determine $k_m$ and $c$ graphically.  
%\begin{displaymath} 
%-ct = ln(1 - \frac{V_{t}}{V_1}), 
%\end{displaymath} 
%\begin{displaymath} 
%V_1 = \frac{k_t k_m V_{in}}{c}. 
%\end{displaymath} 
\end{enumerate}

\subsection{Task 4 --- Closed Loop Speed Control}

\begin{figure}[htb]
\begin{center}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{MT150F}{MT150F}
\psfrag{ServoAmp}{Servo Amp}
\psfrag{PA150C}{PA150C}
\psfrag{Vt}{$V_t$}
\psfrag{Vin}{$V_\text{in}$}
\psfrag{Vr}{$V_r$}
\psfrag{InputPot}{Input Pot}
\psfrag{+15}{$+$15}
\psfrag{-15}{$-$15}
\psfrag{OU150A}{OU150A}
\psfrag{EddyBrake}{Eddy Brake}
\includegraphics[width=0.9\textwidth]{figs/servo-5.eps}
\caption{Setup for Task 4.}
\label{fig:servo5}
\end{center}
\end{figure}

Set up the circuit shown in Fig.\ \ref{fig:servo5}.

In this task, a signal proportional to the speed is fed back to the OU150A 
and compared with the required speed (reference voltage $V_r$).  This 
produces an error signal to actuate the Servo Amplifier output, allowing 
the motor to maintain a more constant speed in the face of fluctuating 
loads than would be possible without feedback.  

If $V_t < -V_r$, then $V_\text{in} > 0$ and power will be supplied to the
motor to accelerate it, so as to bring $(V_t + V_r)$ closer to zero
(reducing the ``error" signal by negative feedback.)  The speed of the
motor will therefore be controlled by the setting of $V_r$, and will
remain reasonably constant despite changes in the mechanical load the
motor must drive.  

\begin{enumerate}
\item Plot the relation between the reference voltage and motor speed.

\item Slowly apply the eddy brake and observe what happens to the error 
voltage.

\item What happens if you reverse the tacho-generator signal to 
the OU150A (i.e. swap the $V_t$ and ground wires)? What sort of feedback would you call this?
\end{enumerate}

\subsection{Task 5 --- Closed Loop Position Control}

\begin{figure}[htb]
\begin{center}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{MT150F}{MT150F}
\psfrag{ServoAmp}{Servo Amp}
\psfrag{PA150C}{PA150C}
\psfrag{Vin}{$V_\text{in}$}
\psfrag{+15}{$+$15}
\psfrag{-15}{$-$15}
\psfrag{OU150A}{OU150A}
\psfrag{P1}{P$_1$}
\psfrag{r1}{$r_1$}
\psfrag{Vp=kpq}{$V_p = k_p \theta'$}
\psfrag{Vr=kpqr}{$V_r = k_p \theta_r$}
\includegraphics[width=\textwidth]{figs/servo-6.eps}
\caption{Setup for Task 5.  Ratio $r_1$ scales OU150A signal from 0\% to 
100\%.}
\label{fig:servo6}
\end{center}
\end{figure}

Set up the circuit shown in Fig.\ \ref{fig:servo6}.

Here we utilize the error signal output of OU150A to control the position 
of the motor shaft as monitored by the Output Pot.  Potentiometer P$_1$ is 
connected between the OU150A and PA150C to act as a gain control.  The 
input of the PA150C will be 
\begin{equation}
V_\text{in}=-(V_r+V_p)r_1.
\end{equation}

The motor drive signal is given by the voltage difference between these 
two inputs. We can see that $V_\text{in}=0$ if $V_p=-V_r$ or $\theta = 
-\theta_r$.  If $\theta\neq -\theta_r$, the negative feedback will drive 
the motor to reduce $\theta + \theta_r$.

\begin{enumerate}
\item After setting up the circuit as in Fig.\ \ref{fig:servo6} (with the toggle switch on the PA150C in its center position), manually 
adjust the potentiometer P$_1$, so that the Output Pot position is 
controlled by the Input Pot position.  

\item Replace the signal from the Input Pot by a 2V peak-to-peak square 
wave from ChA. The period should be about 3-5 seconds.  Vary the 
potentiometer P$_1$ and observe how the response varies.  

\item Using the linear relationship between the voltage from the Output 
Pot and the angle of the shaft, determine a value for $k_p$.  Find the 
solution to the equation of motion in terms of $c$, $k_m$, $k_p$, $r_1$, 
and $V_A$, the amplitude of ChA's square wave.  
 
\item Solve for the critical $r_1$ value. What type(s) of damping can you 
obtain in this setup? Measure and plot examples of this damping, and 
record the corresponding value $r_1$ from P$_1$. How does the feedback 
parameter affect the steady-state position of the output shaft?  
\end{enumerate}

\subsection{Task 6 --- Closed Loop with Position and Velocity feedback}

\begin{figure}[htb]
\begin{center}
\psfrag{Power}{Power}
\psfrag{Supply}{Supply}
\psfrag{MT150F}{MT150F}
\psfrag{ServoAmp}{Servo Amp}
\psfrag{PA150C}{PA150C}
\psfrag{ChAOutput}{ChA Output}
\psfrag{Vin}{$V_\text{in}$}
\psfrag{+15}{$+$15}
\psfrag{-15}{$-$15}
\psfrag{OU150A}{OU150A}
\psfrag{P3}{P$_3$}
\psfrag{r3}{$r_3$}
\psfrag{P4}{P$_4$}
\psfrag{r4}{$r_4$}
\psfrag{Vp=kpq}{$V_p = k_p \theta'$}
\psfrag{Vt}{$V_t$}
\includegraphics[width=\textwidth]{figs/servo-7.eps}
\caption{Setup for Task 6.  $r_4$ scales the tachometer signal.}
\label{fig:servo7}
\end{center}
\end{figure}

Assemble the system shown in Fig.\ \ref{fig:servo7}.  

In Task 5 we saw that the positional control would be accurate only at 
high gain.  However, the high gain caused overshooting and instability.  
The problem of overshoot is a result of the motor's speed carrying it past 
the point of alignment.  To prevent this overshoot we can feed back a 
proportion of the derivative of the error signal, which we then subtract 
from the error signal, to produce a drive signal to the motor.  

To demonstrate mathematically, the solution to the equation of motion in 
Task 5 contains an undamped transient term. The coefficient of this term 
should satisfy a certain condition to prevent overshoot. Adding a term in 
$\frac{d\theta}{dt}$ to the differential equation obtains this condition, 
which physically corresponds to connecting the tach voltage $V_t$ through 
P$_4$ to the PA150C.  

\begin{enumerate}
\item Choose appropriate high and low voltages for ChA Output, and observe 
how the angular position tracks the square wave applied.  

\item Determine the equation of motion in terms of $k_m, k_p, k_t, c, r_3, 
r_4$, and $V_A$.

\item Using your values of $k_m, k_p, k_t$, and $c$, obtain the relation
between $r_3$ and $r_4$ which produces critical damping.

\item Determine what values of $r_3$ and $r_4$ are associated with 
underdamped, overdamped and critically damped responses.  Measure and plot 
an example of each type of damping.
\end{enumerate}

\section{References}
\begin{enumerate}
\item {\bf ``Control Mechanism"}, H.K.\ Skramstad and G.L.\ Landsman

\item {\bf ``Servomechanisms"}, J.C.\ West

\item {\bf ``Feedback and Control Systems"}, Sydney Davis. 
\end{enumerate}
